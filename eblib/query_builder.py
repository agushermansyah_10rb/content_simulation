import MySQLdb
import re

__author__ = '4740'


class NoChange:
    def __init__(self):
        pass


class QueryBuilder:
    no_change = True

    def __init__(self):
        pass

    def insert(self, data, table_name, ignore=False):
        global sql
        ignore_str = ""
        if ignore: ignore_str = "IGNORE"
        try:
            keys = data.keys()
            fields = ", ".join(keys)

            sql = "INSERT {0} INTO ".format(ignore_str)
            sql += table_name
            sql += " ("+fields+") "
            sql += "VALUES "
            sql += "("+self.value_insert(data, keys)+")"
            print sql
            return sql
        except Exception, e:
            raise

    def bulk_insert(self, datas, table_name, ignore=False):
        global sql
        ignore_str = ""
        if ignore:
            ignore_str = "IGNORE"
        try:
            keys = datas[0].keys()
            fields = ", ".join(keys)

            values = list()
            for data in datas:
                values.append("({})".format(self.value_insert(data, keys)))

            sql = "INSERT {} INTO {} ({}) VALUES {}".format(ignore_str, table_name, fields, ", ".join(values))
            return sql
        except Exception:
            raise

    def value_insert(self, data, keys):
        values = list()
        for key in keys:
            try:
                data[key] = str(data[key].encode('utf-8'))
            except:
                pass

            if isinstance(data[key], dict):
                if "inc" in data[key]:
                    values.append(str(data[key]["inc"]))
                elif "dec" in data[key]:
                    values.append(str(data[key]["dec"]))
            elif data[key] is None or data[key] == "":
                values.append('NULL')
            elif self.is_number(data[key]):
                values.append('{0}'.format(data[key]))
            else:
                values.append('"{0}"'.format(MySQLdb.escape_string(data[key])))
        value = ", ".join(values)
        return value

    def insert_update_clean(self, data, key, table_name):
        return MySQLdb.escape_string(self.insert_update(data, key, table_name))

    def insert_update(self, data, key, table_name, exclude_field=''):
        global sql
        try:
            keys = data.keys()
            fields = "`, `".join(keys)

            sql = "INSERT INTO "
            sql += table_name
            sql += " (`"+fields+"`) "
            sql += "VALUES "
            sql += "("+self.value_insert(data, keys)+") "
            sql += "ON DUPLICATE KEY UPDATE "
            sql += self.value_update(data, key, exclude_field)
            return sql
        except Exception, e:
            raise

    def update_clean(self, data, table, key):
        return MySQLdb.escape_string(self.update(data, table, key))

    def update(self, data, table, key, commit=False):
        global sql
        try:
            sql = "UPDATE {0} SET ".format(table)
            sql += self.value_update(data, key)
            sql += " WHERE {0} = '{1}'".format(key, data[key])
            return sql
        except Exception, e:
            raise

    def value_update(self, data, primary, exclude_field=''):
        try:
            primaries = primary.replace(' ', '').split(',')
            exclude_fields = exclude_field.replace(' ', '').split(',')
            sets = list()
            keys = data.keys()
            for key in keys:
                if key not in primaries or isinstance(key, NoChange) or key not in exclude_fields:
                    if data[key] is not None and data[key] != "":
                        try:
                            data[key] = data[key].encode('utf-8')
                        except Exception:
                            pass

                        if isinstance(data[key], dict):
                            if "inc" in data[key]:
                                sets.append('`{0}` = {0} + {1}'.format(str(key), data[key]["inc"]))
                            elif "dec" in data[key]:
                                sets.append('`{0}` = {0} - {1}'.format(str(key), data[key]["dec"]))
                        elif self.is_number(data[key]):
                            sets.append('`{0}` = {1}'.format(str(key), data[key]))
                        else:
                            sets.append('`{0}` = "{1}"'.format(str(key), MySQLdb.escape_string(data[key])))
                    else:
                        sets.append('`{}` = NULL'.format(str(key)))

            return ", ".join(sets)
        except Exception, e:
            raise

    @staticmethod
    def is_number(s):
        if type(s) is str:
            intstr = ['Infinity', 'infinity', 'nan', 'inf','NAN','INF']
            if intstr.count(s.lower()) or re.match(r'[0-9]+(e|E)[0-9]+', s):
                return False

        try:
            float(s)
            return True
        except Exception:
            pass

        try:
            import unicodedata
            unicodedata.numeric(s)
            return True
        except Exception:
            pass

        return False
