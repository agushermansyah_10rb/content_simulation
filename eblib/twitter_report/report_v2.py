import re
import time

from core import logger
from core.controller import Controller
from eblib.browser.SimpleBrowser import SimpleBrowser, SimpleBrowserWait


class ReportV2(Controller):

    def __init__(self):
        Controller.__init__(self)
        self.browser = None
        self.sb = SimpleBrowser()
        self.sb_wait = SimpleBrowserWait()
        self.login_url = 'https://twitter.com/login'
        self.hashtag_trending_url = 'https://twitter.com/i/trends'

    def set_custom_browser(self, browser):
        result = False
        try:
            if not browser:
                raise Exception('browser not found')

            self.browser = browser
        except Exception as e:
            logger.error(str(e))
        finally:
            return result

    def prepare(self, browser_name='chromium'):
        result = False
        try:
            browser_conf = {
                'headless': True,
                'maximized': True,
                'disable-notif': True,
                'user_agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/79.0.3945.88 Safari/537.36'
            }
            browser = self.sb.get_browser(browser_name=browser_name, browser_config=browser_conf)
            if browser:
                self.browser = browser
                result = True
            else:
                raise Exception('prepare failed, browser not found')
        except Exception as e:
            logger.error(str(e))
        finally:
            return result

    def login(self, screen_name, password):
        result = False
        try:
            if not self.browser:
                raise Exception('browser not found')

            login_css_element = {
                'elem_username': '.signin .js-username-field',
                'elem_password': '.signin .js-password-field',
                'elem_button': '.signin button'
            }

            self.browser.get(self.login_url)

            wait_username = self.sb_wait.wait_until_element_clickable(
                self.browser, login_css_element['elem_username'], 30
            )
            wait_password = self.sb_wait.wait_until_element_clickable(
                self.browser, login_css_element['elem_password'], 30
            )
            wait_button = self.sb_wait.wait_until_element_clickable(
                self.browser, login_css_element['elem_button'], 30
            )

            if wait_username and wait_password and wait_button:
                field_username = self.browser.find_element_by_css_selector(login_css_element['elem_username'])
                field_password = self.browser.find_element_by_css_selector(login_css_element['elem_password'])
                button_login = self.browser.find_element_by_css_selector(login_css_element['elem_button'])

                field_username.send_keys(screen_name)
                time.sleep(1)
                field_password.send_keys(password)
                time.sleep(1)
                button_login.click()
                result = True
            else:
                raise Exception('some login element not found')
        except Exception as e:
            logger.error(str(e))
        finally:
            return result

    def report_abuse(self, model, profile_url, option_report, selected_tweet, reason):
        result = False
        try:
            if not self.browser:
                raise Exception('browser not found')

            report_xpath_element = {
                'more_option': "//main/div[1]/div[1]/div[1]/div[1]/div[1]/"
                               "div[2]/div/div/div[1]/div[1]/div[1]/div/div[1]",
                'button_report': "//a[@role='menuitem' and contains(@href, 'report')]",
                'iframe_report': "//div[@aria-modal='true']//iframe[contains(@src, 'report')]",
                'button_select_report': "//button[@value='{0}']",
                'button_select_report_2': "//input[@value='{0}']",
                'button_next_report': "//div[contains(@class, 'MultiItemSelect-bottom')]/button[@class='submit-btn']",
                'select_tweet': '//div[contains(@class, "QuoteTweet-innerContainer")][@data-tweetid="{}"]'
                                '//input[@class="tweetSelector"]',
                'send_sel_tweet': '//div[@class="MultiItemSelect-bottom"]//button[@id="attach_tweets"]',
                'skip_choose_tweet': '//button[@class="skip-btn"]',
                'fill_reason': '//form[contains(@action, "report_story")]/textarea',
                'button_reason': '//form[contains(@action, "report_story")]/button[@class="submit-btn"]',
                'fill_confirm': '//input[@name="signature_input"]',
                'button_fill_confirm': '//form[contains(@action, "report_story")]/button[@class="submit-btn"]',
                'fill_username': '//textarea[@name="impersonation_victim_name"]',
                'button_fill_username': '//form[contains(@action, "report_story")]/button[@class="submit-btn"]',
                'fill_username_2': '//textarea[@name="impersonation_victim_name"]',
                'fill_email_company': '//input[@name="reporter_email"]',
                'button_fill_username_email': '//form[contains(@action, "report_story")]/button[@class="submit-btn"]',
                'fill_username_3': '//form[contains(@action, "report")]'
                                   '//input[contains(@class, "FormTypeaheadSelect-input")]',
                'choose_username': '//ul[contains(@class, "Dropdown-menuItemList")]/li[1]',
                'button_fill_username_3': '//form[contains(@action, "report_story")]/button[@class="submit-btn"]'
            }

            self.browser.get(profile_url)
            time.sleep(5)

            # CLICK MORE BUTTON
            wait_button_more = self.sb_wait.wait_until_element_clickable_xpath(
                self.browser, report_xpath_element['more_option'], 60
            )

            if not wait_button_more:
                raise Exception('button more not found')

            button_more = self.browser.find_element_by_xpath(report_xpath_element['more_option'])
            button_more.click()

            time.sleep(5)

            # CLICK REPORT BUTTON
            wait_button_report = self.sb_wait.wait_until_element_clickable_xpath(
                self.browser, report_xpath_element['button_report'], 60
            )

            if not wait_button_report:
                raise Exception('button more not found')

            button_report = self.browser.find_element_by_xpath(report_xpath_element['button_report'])
            button_report.click()

            time.sleep(5)

            # GET REPORT URL
            wait_iframe = self.sb_wait.wait_until_element_clickable_xpath(
                self.browser, report_xpath_element['iframe_report'], 60
            )

            if not wait_iframe:
                raise Exception('iframe not found')

            iframe = self.browser.find_element_by_xpath(report_xpath_element['iframe_report'])
            report_url = iframe.get_attribute('src')

            if not report_url:
                raise Exception('report url not found')

            self.browser.get(report_url)
            time.sleep(5)

            # CLICK OPTION REPORT
            option_report = option_report.split(',')
            for opt in option_report:
                opt = str(opt)
                data_raaf = model.get_simple_raaf(opt).fetchone
                if not data_raaf:
                    raise Exception('raaf not found')

                selector = data_raaf['selector']
                if selector != '-' and selector != 'choose_tweet' and selector != 'fill_reason' \
                        and selector != 'fill_reason_username' \
                        and selector != 'fill_reason_username_email' \
                        and selector != 'fill_reason_username_2' and selector != 'confirm':
                    xpath_select = report_xpath_element['button_select_report'].format(selector)
                    # SELECT REPORT
                    wait_button_select = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, xpath_select, 30
                    )

                    if wait_button_select:
                        button_option = self.browser.find_element_by_xpath(xpath_select)
                        button_option.click()

                        time.sleep(5)

                    else:
                        xpath_select_2 = report_xpath_element['button_select_report_2'].format(selector)
                        wait_button_select_2 = self.sb_wait.wait_until_element_clickable_xpath(
                            self.browser, xpath_select_2, 30
                        )
                        # SELECT REPORT V2
                        if wait_button_select_2:
                            button_option = self.browser.find_element_by_xpath(xpath_select_2)
                            button_option.click()

                            time.sleep(5)

                            # CHECK BUTTON NEXT
                            wait_button_next = self.sb_wait.wait_until_element_clickable_xpath(
                                self.browser, report_xpath_element['button_next_report'], 10
                            )

                            if wait_button_next:
                                button_next = self.browser.find_element_by_xpath(report_xpath_element['button_next_report'])
                                if button_next:
                                    button_next.click()

                                    time.sleep(5)
                            else:
                                raise Exception('button next report not found')
                        else:
                            raise Exception('button report not found')

                elif selector == 'choose_tweet':
                    # selected_tweet = selected_tweet.split(',')
                    # for stweet in selected_tweet:
                    #     xpath_select = report_xpath_element['select_tweet'].format(stweet)
                    #     wait_check_select_tweet = self.sb_wait.wait_until_element_clickable_xpath(
                    #         self.browser, xpath_select, 60
                    #     )
                    #
                    #     if not wait_check_select_tweet:
                    #         raise Exception('select check tweet not found')
                    #
                    #     checklist_tweet = self.browser.find_element_by_xpath(xpath_select)
                    #     checklist_tweet.click()
                    #
                    #     time.sleep(2)
                    #
                    # # SUBMIT SELECT TWEET
                    # wait_submit_selected = self.sb_wait.wait_until_element_clickable_xpath(
                    #     self.browser, report_xpath_element['send_sel_tweet'], 60
                    # )
                    #
                    # if not wait_submit_selected:
                    #     raise Exception('submit selected tweet not found')
                    #
                    # submit_selected = self.browser.find_element_by_xpath(
                    #     report_xpath_element['send_sel_tweet']
                    # )
                    # submit_selected.click()
                    #
                    # time.sleep(5)
                    # SKIP SELECT
                    wait_skip_tweet = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, report_xpath_element['skip_choose_tweet'], 60
                    )

                    if not wait_skip_tweet:
                        raise Exception('submit selected tweet not found')

                    skip_tweet = self.browser.find_element_by_xpath(
                        report_xpath_element['skip_choose_tweet']
                    )
                    skip_tweet.click()

                    time.sleep(5)
                elif selector == 'fill_reason':
                    # FILL REASON
                    wait_form_reason = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, report_xpath_element['fill_reason'], 60
                    )

                    if not wait_form_reason:
                        raise Exception('reason form not found')

                    form_reason = self.browser.find_element_by_xpath(
                        report_xpath_element['fill_reason']
                    )
                    form_reason.click()
                    form_reason.send_keys(reason[0])

                    # SEND REASON
                    wait_button_reason = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, report_xpath_element['button_reason'], 60
                    )

                    if not wait_button_reason:
                        raise Exception('reason button not found')

                    button_reason = self.browser.find_element_by_xpath(
                        report_xpath_element['button_reason']
                    )
                    button_reason.click()
                elif selector == 'fill_reason_username':
                    # FILL REASON
                    wait_form_reason = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, report_xpath_element['fill_username'], 60
                    )

                    if not wait_form_reason:
                        raise Exception('reason form not found')

                    form_reason = self.browser.find_element_by_xpath(
                        report_xpath_element['fill_username']
                    )
                    form_reason.click()
                    form_reason.send_keys(reason[0])

                    # SEND REASON
                    wait_button_reason = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, report_xpath_element['button_fill_username'], 60
                    )

                    if not wait_button_reason:
                        raise Exception('reason button not found')

                    button_reason = self.browser.find_element_by_xpath(
                        report_xpath_element['button_fill_username']
                    )
                    button_reason.click()
                elif selector == "fill_reason_username_email":
                    # FILL USERNAME
                    wait_form_reason = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, report_xpath_element['fill_username_2'], 60
                    )

                    if not wait_form_reason:
                        raise Exception('reason form not found')

                    form_reason = self.browser.find_element_by_xpath(
                        report_xpath_element['fill_username_2']
                    )
                    form_reason.click()
                    form_reason.send_keys(reason[0])

                    # FILL EMAIL
                    wait_form_email = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, report_xpath_element['fill_email_company'], 60
                    )

                    if not wait_form_email:
                        raise Exception('email form not found')

                    form_email = self.browser.find_element_by_xpath(
                        report_xpath_element['fill_email_company']
                    )
                    form_email.click()
                    form_email.send_keys(reason[1])

                    # SEND REASON
                    wait_button_reason = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, report_xpath_element['button_fill_username_email'], 60
                    )

                    if not wait_button_reason:
                        raise Exception('reason button not found')

                    button_reason = self.browser.find_element_by_xpath(
                        report_xpath_element['button_fill_username_email']
                    )
                    button_reason.click()
                elif selector == "fill_reason_username_2":
                    # FILL REASON
                    wait_form_reason = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, report_xpath_element['fill_username_3'], 60
                    )

                    if not wait_form_reason:
                        raise Exception('reason form not found')

                    form_reason = self.browser.find_element_by_xpath(
                        report_xpath_element['fill_username_3']
                    )
                    form_reason.click()
                    form_reason.send_keys(reason[0])

                    # CHOOSE USERNAME
                    wait_button_choose = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, report_xpath_element['choose_username'], 60
                    )

                    if not wait_button_choose:
                        raise Exception('reason button not found')

                    button_choose = self.browser.find_element_by_xpath(
                        report_xpath_element['choose_username']
                    )
                    button_choose.click()

                    time.sleep(5)

                    # SEND REASON
                    wait_button_reason = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, report_xpath_element['button_fill_username_3'], 60
                    )

                    if not wait_button_reason:
                        raise Exception('reason button not found')

                    button_reason = self.browser.find_element_by_xpath(
                        report_xpath_element['button_fill_username_3']
                    )
                    button_reason.click()
                elif selector == "confirm":
                    # FILL REASON
                    wait_form_reason = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, report_xpath_element['fill_confirm'], 60
                    )

                    if not wait_form_reason:
                        raise Exception('reason form not found')

                    form_reason = self.browser.find_element_by_xpath(
                        report_xpath_element['fill_confirm']
                    )
                    form_reason.click()
                    form_reason.send_keys('confirm')

                    # SEND REASON
                    wait_button_reason = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, report_xpath_element['button_fill_confirm'], 60
                    )

                    if not wait_button_reason:
                        raise Exception('reason button not found')

                    button_reason = self.browser.find_element_by_xpath(
                        report_xpath_element['button_fill_confirm']
                    )
                    button_reason.click()
                elif selector == '-':
                    pass

            result = True
        except Exception as e:
            logger.error(str(e))
            result = str(e)
        finally:
            return result

    def report_content(self, model, content_url, option_report, selected_tweet, reason):
        result = False
        try:
            if not self.browser:
                raise Exception('browser not found')

            report_css_element = {
                'more_option': 'section div:first div div:first div:first article div:first > '
                               'div:nth-child(2) div[role="button"]',
                'button_report': 'a[role="menuitem"][data-testid="report"]',
                'iframe_report': 'div[aria-modal="true"] iframe[src*="report"]',
                'button_select_report': 'button[class="list-btn"][value="{}"]',
                'select_tweet': 'div[class*="QuoteTweet-innerContainer"][data-tweetid="{}"] '
                                'input[class="tweetSelector"]',
                'button_send_select_tweet': 'div[class="MultiItemSelect-bottom"] button[id="attach_tweets"]',
                'fill_reason': 'form[action*="report_story"] textarea',
                'button_reason': 'form[action*="report_story"] button.submit-btn'
            }

            report_xpath_element = {
                'more_option': '(//section[contains(@aria-labelledby, "accessible-list")])[1]/div[1]/div/div/'
                               'div[contains(., "{}")][1]//article/div/div[2]//div[@role="button"]',
                'reported_button': '(//section[contains(@aria-labelledby, "accessible-list")])[1]/div[1]/div/div/'
                                    'div[contains(., "Anda melaporkan Tweet ini.")]//div[@role="button"]|'
                                    '(//section[contains(@aria-labelledby, "accessible-list")])[1]/div[1]/div/div/'
                                    'div[contains(., "You reported this Tweet.")]//div[@role="button"]',
                'skip_choose_tweet': '//button[@class="skip-btn"]'
            }

            self.browser.get(content_url)
            time.sleep(5)

            # CHECK REPORTED TWEET
            wait_reported_button = self.sb_wait.wait_until_element_located_xpath(
                self.browser, report_xpath_element['reported_button'], 20
            )

            if wait_reported_button:
                reported_button = self.browser.find_element_by_xpath(report_xpath_element['reported_button'])
                reported_button.click()
                time.sleep(5)

            # CLICK BUTTON MORE OPTION
            screen_name = re.sub('.*twitter\.[A-z]+\/([A-z0-9_]+)\/.*', '\g<1>', content_url)
            if re.search('\/', screen_name):
                raise Exception('screen name not found')
            sel_btn_opt = report_xpath_element['more_option'].format(screen_name)
            wait_button_option = self.sb_wait.wait_until_element_located_xpath(
                self.browser, sel_btn_opt, 60
            )

            if not wait_button_option:
                raise Exception('button option not found')

            button_option = self.browser.find_element_by_xpath(sel_btn_opt)
            button_option.click()

            time.sleep(5)

            # CLICK BUTTON REPORT
            wait_button_report = self.sb_wait.wait_until_element_clickable(
                self.browser, report_css_element['button_report'], 60
            )

            if not wait_button_report:
                raise Exception('button report not found')

            button_report = self.browser.find_element_by_css_selector(report_css_element['button_report'])
            button_report.click()

            time.sleep(5)

            # GET REPORT URL
            wait_iframe = self.sb_wait.wait_until_element_clickable(
                self.browser, report_css_element['iframe_report'], 60
            )

            if not wait_iframe:
                raise Exception('iframe not found')

            iframe = self.browser.find_element_by_css_selector(report_css_element['iframe_report'])
            report_url = iframe.get_attribute('src')

            if not report_url:
                raise Exception('report url not found')

            self.browser.get(report_url)
            time.sleep(5)

            # CLICK OPTION REPORT
            option_report = option_report.split(',')
            for opt in option_report:
                opt = str(opt)
                data_raf = model.get_simple_raf(opt).fetchone
                if not data_raf:
                    raise Exception('raf not found')

                selector = data_raf['selector']
                if selector != '-' and selector != 'choose_tweet' and selector != 'fill_reason':
                    css_select = report_css_element['button_select_report'].format(selector)
                    wait_button_select = self.sb_wait.wait_until_element_clickable(
                        self.browser, css_select, 60
                    )

                    if not wait_button_select:
                        raise Exception('button select report not found')

                    button_option = self.browser.find_element_by_css_selector(css_select)
                    button_option.click()

                    time.sleep(5)
                elif selector == 'choose_tweet':
                    # selected_tweet = selected_tweet.split(',')
                    # for stweet in selected_tweet:
                    #     css_select = report_css_element['select_tweet'].format(stweet)
                    #     wait_check_select_tweet = self.sb_wait.wait_until_element_clickable(
                    #         self.browser, css_select, 60
                    #     )
                    #
                    #     if not wait_check_select_tweet:
                    #         raise Exception('select check tweet not found')
                    #
                    #     checklist_tweet = self.browser.find_element_by_css_selector(css_select)
                    #     checklist_tweet.click()
                    #
                    #     time.sleep(2)
                    #
                    # # SUBMIT SELECT TWEET
                    # wait_submit_selected = self.sb_wait.wait_until_element_clickable(
                    #     self.browser, report_css_element['button_send_select_tweet'], 60
                    # )
                    #
                    # if not wait_submit_selected:
                    #     raise Exception('submit selected tweet not found')
                    #
                    # submit_selected = self.browser.find_element_by_css_selector(
                    #     report_css_element['button_send_select_tweet']
                    # )
                    # submit_selected.click()
                    #
                    # time.sleep(5)
                    # SKIP SELECT
                    wait_skip_tweet = self.sb_wait.wait_until_element_clickable_xpath(
                        self.browser, report_xpath_element['skip_choose_tweet'], 60
                    )

                    if not wait_skip_tweet:
                        raise Exception('submit selected tweet not found')

                    skip_tweet = self.browser.find_element_by_xpath(
                        report_xpath_element['skip_choose_tweet']
                    )
                    skip_tweet.click()

                    time.sleep(5)
                elif selector == 'fill_reason':
                    # FILL REASON
                    wait_form_reason = self.sb_wait.wait_until_element_clickable(
                        self.browser, report_css_element['fill_reason'], 60
                    )

                    if not wait_form_reason:
                        raise Exception('reason form not found')

                    form_reason = self.browser.find_element_by_css_selector(
                        report_css_element['fill_reason']
                    )
                    form_reason.click()
                    form_reason.send_keys(reason)

                    # SEND REASON
                    wait_button_reason = self.sb_wait.wait_until_element_clickable(
                        self.browser, report_css_element['button_reason'], 60
                    )

                    if not wait_button_reason:
                        raise Exception('reason button not found')

                    button_reason = self.browser.find_element_by_css_selector(
                        report_css_element['button_reason']
                    )
                    button_reason.click()
                elif selector == '-':
                    pass

            result = True
        except Exception as e:
            logger.error(str(e))
            result = str(e)
        finally:
            return result

    def report_hashtag(self, hashtag, option_report, location='indonesia'):
        result = False
        try:
            if not self.browser:
                raise Exception('browser not found')

            report_options = {
                '1': 'This trend is spam',
                '2': 'This trend is abusive or harmful',
                '3': 'This trend is a duplicate',
                '4': 'This trend is low quality'
            }

            option_report = report_options[str(option_report)]

            report_option_translate = {
                'This trend is spam': 'Tren ini adalah spam',
                'This trend is abusive or harmful': 'Tren ini bersifat menghina atau berbahaya',
                'This trend is a duplicate': 'Tren ini adalah duplikat',
                'This trend is low quality': 'Tren ini berkualitas rendah'
            }

            report_css_element = {
                'elem_setting_button': 'a[href="/settings/trends"]',
                'elem_checklist_trend': 'div[aria-modal="true"] input[type="checkbox"]',
                'elem_change_location': 'a[href="/settings/trends/location"]',
                'elem_location': 'div[aria-modal="true"] input[autocapitalize="sentences"]',
                'elem_location_selected': 'div[aria-modal="true"] > div > div > div > div > div + div',
                'elem_close_button': 'div[aria-labelledby="modal-header"] div[aria-label="Close"], '
                                     'div[aria-labelledby="modal-header"] div[aria-label="Tutup"]'
            }

            report_xpath_element = {
                'elem_select_hashtag': '//div[@role="link"]/div[.//span[contains(text(), "{}")]]//div[@role="button"]',
                'elem_select_report': '//div[@role="menuitem"][.//span[contains(text(), "{}")] '
                                      'or .//span[contains(text(), "{}")]]',
                'elem_close_button': '//div[@aria-labelledby="modal-header"]/div/div[1]/div/div/div/div[1]'
            }

            self.browser.get(self.hashtag_trending_url)
            time.sleep(5)

            wait_button_setting = self.sb_wait.wait_until_element_clickable(
                self.browser, report_css_element['elem_setting_button'], 60
            )

            if not wait_button_setting:
                raise Exception('setting button not found')

            button_setting = self.browser.find_element_by_css_selector(report_css_element['elem_setting_button'])
            button_setting.click()

            time.sleep(5)

            wait_change_location = self.sb_wait.wait_until_element_located(
                self.browser, report_css_element['elem_change_location'], 10
            )

            if not wait_change_location:
                wait_checklist_trend = self.sb_wait.wait_until_element_located(
                    self.browser, report_css_element['elem_checklist_trend'], 60
                )

                if not wait_checklist_trend:
                    raise Exception('checklist trend not found')

                checklist_trend = self.browser.find_element_by_css_selector(report_css_element['elem_checklist_trend'])
                checklist_trend.click()

            time.sleep(5)

            wait_change_location = self.sb_wait.wait_until_element_clickable(
                self.browser, report_css_element['elem_change_location'], 60
            )

            if not wait_change_location:
                raise Exception('change location button not found')

            change_location = self.browser.find_element_by_css_selector(report_css_element['elem_change_location'])
            change_location.click()

            time.sleep(5)

            wait_location_field = self.sb_wait.wait_until_element_clickable(
                self.browser, report_css_element['elem_location'], 60
            )

            if not wait_location_field:
                raise Exception('location field not found')

            location_field = self.browser.find_element_by_css_selector(report_css_element['elem_location'])
            location_field.click()
            location_field.send_keys(location)

            time.sleep(3)

            wait_location_selected = self.sb_wait.wait_until_element_clickable(
                self.browser, report_css_element['elem_location_selected'], 60
            )

            if not wait_location_selected:
                raise Exception('location selected not found')

            location_selected = self.browser.find_element_by_css_selector(report_css_element['elem_location_selected'])
            location_selected.click()

            time.sleep(10)

            wait_close_button = self.sb_wait.wait_until_element_clickable_xpath(
                self.browser, report_xpath_element['elem_close_button'], 60
            )

            if not wait_close_button:
                raise Exception('close button not found')

            close_button = self.browser.find_element_by_xpath(report_xpath_element['elem_close_button'])
            close_button.click()

            time.sleep(3)

            wait_select_hashtag = self.sb_wait.wait_until_element_located_xpath(
                self.browser, report_xpath_element['elem_select_hashtag'].format(hashtag), 60
            )

            if not wait_select_hashtag:
                raise Exception('close button not found')

            select_hashtag = self.browser.find_element_by_xpath(
                report_xpath_element['elem_select_hashtag'].format(hashtag)
            )
            select_hashtag.click()

            time.sleep(5)

            wait_select_report = self.sb_wait.wait_until_element_clickable_xpath(
                self.browser, report_xpath_element['elem_select_report'].format(option_report,
                                                                                report_option_translate[option_report]),
                60
            )

            if not wait_select_report:
                raise Exception('close button not found')

            select_report = self.browser.find_element_by_xpath(
                report_xpath_element['elem_select_report'].format(option_report, report_option_translate[option_report])
            )
            select_report.click()

            result = True
        except Exception as e:
            logger.error(str(e))
            result = str(e)
        finally:
            return result

    def close(self):
        result = False
        try:
            if not self.browser:
                raise Exception('browser not found')

            self.browser.close()
            self.browser.quit()
        except Exception as e:
            logger.error(str(e))
        finally:
            return result
