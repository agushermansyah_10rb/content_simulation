from collections import OrderedDict

from copy import copy

HBD = [
    'HDB',
    'Selamat Ulang Tahun',
    'selamat ulang tahun kawan',
    'Happy Birthdays',
    'Selamat ulang tahun, semoga panjang umur',
    'di tunggu traktirannya yaah :D',
    'semoga panjang umur dan makin baik, selamat yaah',
    'Selamat ulang tahun, pokoknya suksess selalu buat sahabatku ini ya',
    'selamat ulang tahun my best friend, semoga tambah panjang umur dan ditambah kebaikan-kebaikannya, yang paling penting bisa menjadi sahabat terbaikku',
    'Happy brithday, semoga tambah dewasa, tambah rezekinya dan tambah berbakti kepada orangtua',
]

REPORT_QUESTION = {
    '2': {
        'text': 'Report this profile',
        'value': 'account',
        'child': {
            '1': {
                'text': 'This person is annoying me',
                'value': 'annoying',
                'child': {
                    '1': {
                        'text': 'Sending spammy friend requests',
                        'value': 'requests',
                        'child': {
                            '1': {
                                'name': 'action_key',
                                'text': 'Change who can send you friend requests',
                                'value': 'LIMIT_FRIENDING_PRIVACY'
                            },
                            '2': {
                                'name': 'action_key',
                                'text': 'Block',
                                'value': 'BLOCK_ACTOR'
                            },
                            '3': {
                                'name': 'action_key',
                                'text': 'Unfollow',
                                'value': 'UNSUBSCRIBE'
                            },
                            '4': {
                                'name': 'action_key',
                                'text': 'Unfriend',
                                'value': 'UNFRIEND'
                            }
                        }
                    },
                    '2': {
                        'text': 'Sending spam messages',
                        'value': 'messages',
                        'child': {
                            '1': {
                                'name': 'action_key',
                                'text': 'Change who can send you friend requests',
                                'value': 'LIMIT_FRIENDING_PRIVACY'
                            },
                            '2': {
                                'name': 'action_key',
                                'text': 'Block',
                                'value': 'BLOCK_ACTOR'
                            },
                            '3': {
                                'name': 'action_key',
                                'text': 'Unfollow',
                                'value': 'UNSUBSCRIBE'
                            },
                            '4': {
                                'name': 'action_key',
                                'text': 'Unfriend',
                                'value': 'UNFRIEND'
                            }
                        }
                    },
                    '3': {
                        'text': 'Posting annoying things',
                        'value': 'posts',
                        'child': {
                            '1': {
                                'name': 'action_key',
                                'text': 'Block',
                                'value': 'BLOCK_ACTOR'
                            },
                            '2': {
                                'name': 'action_key',
                                'text': 'Unfollow',
                                'value': 'UNSUBSCRIBE'
                            },
                            '3': {
                                'name': 'action_key',
                                'text': 'Unfriend',
                                'value': 'UNFRIEND'
                            },
                            '4': {
                                'name': 'action_key',
                                'text': 'Message to resolve this',
                                'value': 'MESSAGE'
                            }
                        }
                    },
                }
            },
            '2': {
                'text': 'They\'re pretending to be me or someone I know',
                'value': 'impersonating',
                'child': {
                    '1': {
                        'text': 'Me',
                        'value': 'me',
                        'child': {
                            '1': {
                                'name': 'action_key',
                                'text': 'Submitted to Facebook for Review',
                                'value': 'REPORT_CONTENT'
                            },
                            '2': {
                                'name': 'action_key',
                                'text': 'Block',
                                'value': 'BLOCK_ACTOR'
                            },
                            '3': {
                                'name': 'action_key',
                                'text': 'Unfollow',
                                'value': 'UNSUBSCRIBE'
                            },
                            '4': {
                                'name': 'action_key',
                                'text': 'Unfriend',
                                'value': 'UNFRIEND'
                            }
                        }
                    },
                    '2': {
                        'text': 'Someone I know',
                        'value': 'friend',
                        'child': {
                            '1': {
                                'name': 'action_key',
                                'text': 'Submitted to Facebook for Review',
                                'value': 'REPORT_CONTENT'
                            },
                            '2': {
                                'name': 'action_key',
                                'text': 'Block',
                                'value': 'BLOCK_ACTOR'
                            },
                            '3': {
                                'name': 'action_key',
                                'text': 'Unfollow',
                                'value': 'UNSUBSCRIBE'
                            },
                            '4': {
                                'name': 'action_key',
                                'text': 'Unfriend',
                                'value': 'UNFRIEND'
                            },
                            '5': {
                                'name': 'action_key',
                                'text': 'Message to resolve this',
                                'value': 'MESSAGE'
                            }
                        }
                    },
                    '3': {
                        'text': 'A celebrity',
                        'value': 'celeb',
                        'child': {
                            '1': {
                                'name': 'action_key',
                                'text': 'Block',
                                'value': 'BLOCK_ACTOR'
                            },
                            '2': {
                                'name': 'action_key',
                                'text': 'Unfollow',
                                'value': 'UNSUBSCRIBE'
                            },
                            '3': {
                                'name': 'action_key',
                                'text': 'Unfriend',
                                'value': 'UNFRIEND'
                            }
                        }
                    },
                }
            },
            '3': {
                'text': 'They\'re sharing inappropriate or offensive posts',
                'value': 'inappropriate',
                'child': {
                    '1': {
                        'text': 'Nudity and pornography',
                        'value': 'pornography',
                        'child': {
                            '1': {
                                'name': 'action_key',
                                'text': 'Submitted to Facebook for Review',
                                'value': 'REPORT_CONTENT'
                            },
                            '2': {
                                'name': 'action_key',
                                'text': 'Block',
                                'value': 'BLOCK_ACTOR'
                            },
                            '3': {
                                'name': 'action_key',
                                'text': 'Unfollow',
                                'value': 'UNSUBSCRIBE'
                            },
                            '4': {
                                'name': 'action_key',
                                'text': 'Unfriend',
                                'value': 'UNFRIEND'
                            },
                            '5': {
                                'name': 'action_key',
                                'text': 'Message to resolve this',
                                'value': 'MESSAGE'
                            }
                        }
                    },
                    '2': {
                        'text': 'Sexually suggestive',
                        'value': 'suggestive',
                        'child': {
                            '1': {
                                'name': 'action_key',
                                'text': 'Submitted to Facebook for Review',
                                'value': 'REPORT_CONTENT'
                            },
                            '2': {
                                'name': 'action_key',
                                'text': 'Block',
                                'value': 'BLOCK_ACTOR'
                            },
                            '3': {
                                'name': 'action_key',
                                'text': 'Unfollow',
                                'value': 'UNSUBSCRIBE'
                            },
                            '4': {
                                'name': 'action_key',
                                'text': 'Unfriend',
                                'value': 'UNFRIEND'
                            },
                            '5': {
                                'name': 'action_key',
                                'text': 'Message to resolve this',
                                'value': 'MESSAGE'
                            }
                        }
                    },
                    '3': {
                        'text': 'Other',
                        'value': 'other',
                        'child': {
                            '1': {
                                'name': 'action_key',
                                'text': 'Submitted to Facebook for Review',
                                'value': 'REPORT_CONTENT'
                            },
                            '2': {
                                'name': 'action_key',
                                'text': 'Block',
                                'value': 'BLOCK_ACTOR'
                            },
                            '3': {
                                'name': 'action_key',
                                'text': 'Unfollow',
                                'value': 'UNSUBSCRIBE'
                            },
                            '4': {
                                'name': 'action_key',
                                'text': 'Unfriend',
                                'value': 'UNFRIEND'
                            },
                            '5': {
                                'name': 'action_key',
                                'text': 'Message to resolve this',
                                'value': 'MESSAGE'
                            }
                        }
                    },
                }
            },
            '4': {
                'text': 'This is a fake account',
                'value': 'fake',
                'child': {
                    '1': {
                        'name': 'action_key',
                        'text': 'Submitted to Facebook for Review',
                        'value': 'REPORT_CONTENT'
                    },
                    '2': {
                        'name': 'action_key',
                        'text': 'Block',
                        'value': 'BLOCK_ACTOR'
                    },
                    '3': {
                        'name': 'action_key',
                        'text': 'Unfollow',
                        'value': 'UNSUBSCRIBE'
                    },
                    '4': {
                        'name': 'action_key',
                        'text': 'Unfriend',
                        'value': 'UNFRIEND'
                    }
                }
            },
            '5': {
                'text': 'This profile represents a business or organisation',
                'value': 'business',
                'child': {
                    '1': {
                        'name': 'action_key',
                        'text': 'Submitted to Facebook for Review',
                        'value': 'REPORT_CONTENT'
                    },
                    '2': {
                        'name': 'action_key',
                        'text': 'Block',
                        'value': 'BLOCK_ACTOR'
                    },
                    '3': {
                        'name': 'action_key',
                        'text': 'Unfollow',
                        'value': 'UNSUBSCRIBE'
                    },
                    '4': {
                        'name': 'action_key',
                        'text': 'Unfriend',
                        'value': 'UNFRIEND'
                    },
                    '5': {
                        'name': 'action_key',
                        'text': 'Message to resolve this',
                        'value': 'MESSAGE'
                    }
                }
            },
            '6': {
                'text': 'They\'re using a different name than they use in everyday life',
                'value': 'fake_name',
                'child': {
                    '1': {
                        'text': 'This profile doesn\'t represent a real person',
                        'value': 'fake_name_fake_account',
                        'child': {
                            '1': {
                                'name': 'action_key',
                                'text': 'Submitted to Facebook for Review',
                                'value': 'REPORT_CONTENT'
                            },
                            '2': {
                                'name': 'action_key',
                                'text': 'Block',
                                'value': 'BLOCK_ACTOR'
                            },
                            '3': {
                                'name': 'action_key',
                                'text': 'Unfollow',
                                'value': 'UNSUBSCRIBE'
                            },
                            '4': {
                                'name': 'action_key',
                                'text': 'Unfriend',
                                'value': 'UNFRIEND'
                            }
                        }
                    },
                    '2': {
                        'text': 'They\'re using my name or someone else\'s name or photos',
                        'value': 'fake_name_impersonation',
                        'child': {
                            '1': {
                                'text': 'Me',
                                'value': 'me',
                                'child': {
                                    '1': {
                                        'name': 'action_key',
                                        'text': 'Submitted to Facebook for Review',
                                        'value': 'REPORT_CONTENT'
                                    },
                                    '2': {
                                        'name': 'action_key',
                                        'text': 'Block',
                                        'value': 'BLOCK_ACTOR'
                                    },
                                    '3': {
                                        'name': 'action_key',
                                        'text': 'Unfollow',
                                        'value': 'UNSUBSCRIBE'
                                    },
                                    '4': {
                                        'name': 'action_key',
                                        'text': 'Unfriend',
                                        'value': 'UNFRIEND'
                                    }
                                }
                            },
                            '2': {
                                'text': 'Someone I know',
                                'value': 'friend',
                                'child': {
                                    '1': {
                                        'name': 'action_key',
                                        'text': 'Submitted to Facebook for Review',
                                        'value': 'REPORT_CONTENT_WITH_TYPEAHEAD'
                                    },
                                    '2': {
                                        'name': 'action_key',
                                        'text': 'Block',
                                        'value': 'BLOCK_ACTOR'
                                    },
                                    '3': {
                                        'name': 'action_key',
                                        'text': 'Unfollow',
                                        'value': 'UNSUBSCRIBE'
                                    },
                                    '4': {
                                        'name': 'action_key',
                                        'text': 'Unfriend',
                                        'value': 'UNFRIEND'
                                    },
                                    '5': {
                                        'name': 'action_key',
                                        'text': 'Message your friend',
                                        'value': 'MESSAGE'
                                    }
                                }
                            },
                            '3': {
                                'text': 'A celebrity',
                                'value': 'celeb',
                                'child': {
                                    '1': {
                                        'name': 'action_key',
                                        'text': 'Submitted to Facebook for Review',
                                        'value': 'REPORT_CONTENT'
                                    },
                                    '2': {
                                        'name': 'action_key',
                                        'text': 'Block',
                                        'value': 'BLOCK_ACTOR'
                                    },
                                    '3': {
                                        'name': 'action_key',
                                        'text': 'Unfollow',
                                        'value': 'UNSUBSCRIBE'
                                    },
                                    '4': {
                                        'name': 'action_key',
                                        'text': 'Unfriend',
                                        'value': 'UNFRIEND'
                                    }
                                }
                            },
                        }
                    },
                    '3': {
                        'text': 'They\'re using a name that they don\'t go by in everyday life',
                        'value': 'fake_name_not_real',
                        'child': {
                            '1': {
                                'name': 'action_key',
                                'text': 'Ask us to review this profile',
                                'value': 'REPORT_CONTENT'
                            },
                            '2': {
                                'name': 'action_key',
                                'text': 'Block',
                                'value': 'BLOCK_ACTOR'
                            },
                            '3': {
                                'name': 'action_key',
                                'text': 'Unfollow',
                                'value': 'UNSUBSCRIBE'
                            },
                            '4': {
                                'name': 'action_key',
                                'text': 'Unfriend',
                                'value': 'UNFRIEND'
                            },
                            '5': {
                                'name': 'action_key',
                                'text': 'Message to resolve this',
                                'value': 'MESSAGE'
                            }
                        }
                    },
                    '4': {
                        'text': 'Other',
                        'value': 'fake_name_other',
                        'child': {
                            '1': {
                                'name': 'action_key',
                                'text': 'Ask us to review this profile',
                                'value': 'REPORT_CONTENT'
                            },
                            '2': {
                                'name': 'action_key',
                                'text': 'Block',
                                'value': 'BLOCK_ACTOR'
                            },
                            '3': {
                                'name': 'action_key',
                                'text': 'Unfollow',
                                'value': 'UNSUBSCRIBE'
                            },
                            '4': {
                                'name': 'action_key',
                                'text': 'Unfriend',
                                'value': 'UNFRIEND'
                            },
                            '5': {
                                'name': 'action_key',
                                'text': 'Message to resolve this',
                                'value': 'MESSAGE'
                            }
                        }
                    },
                }
            }
        }
    },
    '3': {
        'text': 'Recover or close this account',
        'value': 'recover',
        'child': {
            '1': {
                'name': 'action_key',
                'text': 'Recover this account',
                'value': 'RECOVER_PROFILE'
            },
            '2': {
                'name': 'action_key',
                'text': 'Close this account',
                'value': 'CLOSE_OLD_PROFILE'
            }
        }
    }
}


def report_question_map(question=REPORT_QUESTION, prekey=None):
    result = {}
    for k, v in question.iteritems():
        key = '%s.%s' % (prekey, k) if prekey else k
        result[key] = copy(v)
        result[key]['key'] = key
        if v.get('child'):
            result[key]['final'] = False
            result.update(report_question_map(v['child'], prekey=key))
            del result[key]['child']
        else:
            result[key]['final'] = True
            if not result[key].get('name'):
                result[key]['name'] = 'action_key'
    return OrderedDict(sorted(result.items()))


def report_question_print():
    qmap = report_question_map()
    for k, v in qmap.iteritems():
        print k, v['text'], '[%s]' % v['value']


def report_query_builder(query):
    assert query
    query = query.split('.')

    def get(query, question=REPORT_QUESTION):
        for key, value in question.iteritems():
            if value['value'] == query:
                return key, copy(value.get('child', None))

    result = []
    question = REPORT_QUESTION
    for q in query:
        if question:
            r, question = get(q, question)
            result.append(r)
    return '.'.join(result)


def report_question_step(query):
    assert query
    result = []
    qmap = report_question_map()
    query = query.split('.')
    for i in range(1, len(query)+1):
        key = '.'.join(query[:i])
        result.append(qmap[key])
    return result

# report_query_builder('account.annoying.requests.LIMIT_FRIENDING_PRIVACY')
