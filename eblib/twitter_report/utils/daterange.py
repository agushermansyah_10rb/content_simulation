import sys
from datetime import datetime, timedelta

reload(sys)
__author__ = "Dev. ISR"


def daterange(start_date, end_date):
    start_date = datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.strptime(end_date, "%Y-%m-%d")
    for n in range(int((end_date - start_date).days) + 1):
        yield start_date + timedelta(n)

def modify_date(start_date, end_date):
    start_date = datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.strptime(end_date, "%Y-%m-%d")
    for n in range(int((end_date - start_date).days) + 2):
        yield start_date + timedelta(n)

def next_week_date(d):
    today = datetime.now().strftime("%Y-%m-%d")
    day_date = datetime.strptime(today, "%Y-%m-%d")
    week_date = []
    for i in range(0, (8-d)):
        dt = day_date + timedelta(days=i)
        week_date.append(dt.strftime("%Y-%m-%d"))
    return week_date[1], week_date[-1]
