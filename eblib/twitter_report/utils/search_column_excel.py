__author__ = "Dev. ISR"

def search_column(num):
    alphabet = None
    if num == 1:
        alphabet = "A"
    elif num == 2:
        alphabet = "B"
    elif num == 3:
        alphabet = "C"
    elif num == 4:
        alphabet = "D"
    elif num == 5:
        alphabet = "E"
    elif num == 6:
        alphabet = "F"
    elif num == 7:
        alphabet = "G"
    elif num == 8:
        alphabet = "H"
    elif num == 9:
        alphabet = "I"
    elif num == 10:
        alphabet = "J"
    elif num == 11:
        alphabet = "K"
    elif num == 12:
        alphabet = "L"
    elif num == 13:
        alphabet = "M"
    elif num == 14:
        alphabet = "N"
    elif num == 15:
        alphabet = "O"
    elif num == 16:
        alphabet = "P"
    elif num == 17:
        alphabet = "Q"
    elif num == 18:
        alphabet = "R"
    elif num == 19:
        alphabet = "S"
    elif num == 20:
        alphabet = "T"
    elif num == 21:
        alphabet = "U"
    elif num == 22:
        alphabet = "V"
    elif num == 23:
        alphabet = "W"
    elif num == 24:
        alphabet = "X"
    elif num == 25:
        alphabet = "Y"
    elif num == 26:
        alphabet = "Z"
    elif num == 27:
        alphabet = "AA"
    elif num == 28:
        alphabet = "AB"
    elif num == 29:
        alphabet = "AC"
    elif num == 30:
        alphabet = "AD"
    elif num == 31:
        alphabet = "AE"
    elif num == 32:
        alphabet = "AF"
    elif num == 33:
        alphabet = "AG"
    elif num == 34:
        alphabet = "AH"
    elif num == 35:
        alphabet = "AI"
    elif num == 36:
        alphabet = "AJ"
    elif num == 37:
        alphabet = "AK"
    elif num == 38:
        alphabet = "AL"
    elif num == 39:
        alphabet = "AM"
    elif num == 40:
        alphabet = "AN"
    return alphabet
