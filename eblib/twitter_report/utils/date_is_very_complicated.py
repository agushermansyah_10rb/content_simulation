from datetime import datetime, date, timedelta
from dateutil.parser import parse
import timelib
import time
from thank_god_its_regex import *

__author__ = 'JP'


def convert(raw_date, format_date='%Y-%m-%dT%H:%M:%S+0000', gmt=0):
    assert isinstance(gmt, int)
    result = ''
    try:
        result = parse(raw_date)
        if DAY_OF_WEEK_PATTERN.search(raw_date):
            result = result - timedelta(days=7)
        result = result - timedelta(hours=gmt)
        result = result.strftime(format_date)
    except Exception as e:
        if str(e).lower() == 'unknown string format':
            try:
                pattern1 = DATE_PATTERN_1.search(raw_date)
                if pattern1:
                    raw_date = '{} mins ago'.format(pattern1.group(1))
                result = datetime.fromtimestamp(timelib.strtotime(raw_date))
                result = result.strftime(format_date)
            except Exception as e:
                pattern2 = DATE_PATTERN_2.search(raw_date)
                pattern3 = DATE_PATTERN_3.search(raw_date)
                pattern4 = DATE_PATTERN_4.search(raw_date)
                if pattern2:
                    result = datetime.now()
                    result = result.strftime(format_date)
                elif pattern3:
                    result = convert('{} hour ago'.format(pattern3.group(1)), format_date=format_date, gmt=gmt)
                elif pattern4:
                    result = date.today() - timedelta(1)
                    result = '{} at {}'.format(result, pattern4.group(1))
                    result = convert(result, format_date=format_date, gmt=gmt)
                else:
                    print 'Pattern not match: {} ({})'.format(raw_date, str(e))
    finally:
        return result


def to_unix_time(date_time):
    return time.mktime(datetime.strptime(date_time, "%Y-%m-%d %H:%M:%S").timetuple())


def current_time():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S")


def current_year():
    return datetime.now().strftime("%Y")


if __name__ == '__main__':
    times = [
        '11 hrs',
        'just now',
        '1 min',
        '31 mins',
        '6 minutes ago',
        '1 min ago',
        '1 hr',
        '2 hrs',
        'today',
        'yesterday',
        'Yesterday at 1:41pm',
        'Yesterday at 08:28',
        '28 August at 13:43',
        '5 April 2014 at 14:44',
        'Saturday at 7:24am',
        'Friday at 7:24am',
        'Sunday at 7:24am',
        '1 October',
        'October 25, 2016 3:10 PM ET'
    ]
    print "{:<25}{}".format("Raw Date", "Result")
    print "-" * 50
    for dt in times:
        print '{:<25}{:>2}'.format(dt, convert(dt))
        print

    to_unix_time('2016-09-25 02:19:34')
