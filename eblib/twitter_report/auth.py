import json
import pickle
import urlparse

from selenium.common.exceptions import WebDriverException, NoSuchElementException
from selenium.webdriver.common.keys import Keys
from splinter.exceptions import ElementDoesNotExist
from browser import Browser
from selenium.webdriver.support.ui import WebDriverWait

BASE_URL = 'https://www.twitter.com/'
BASIC_BASE_URL = 'https://twitter.com/'
MOBILE_BASE_URL = "https://mobile.twitter.com/login"
ERROR_LOGIN =["https://mobile.twitter.com/login", "https://mobile.twitter.com/account/locked"]
SIGN_LOGIN = [
    'https://mobile.twitter.com/home'
]
RETYPE = [
    'challenge_type=RetypePhoneNumber',
    'challenge_type=RetypeEmail',
    'challenge_type=RetypeScreenName'
]
COOKIES_FORMAT = "document.cookie = '{name}={value}; path={path}; domain={domain}; expiry={expiry}; expires={expires}';"
APPS_URL = 'https://apps.twitter.com/app'

class Auth(Browser):

    cookies = None
    login_status = False

    def __init__(self, driver_name=None, executable_path=None, port=None):
        Browser.__init__(self, driver_name=driver_name, executable_path=executable_path, port=None)

    def login(self, cookies=None, username=None, password=None, cookies_only=True, robot_id=None, phone_no=None,
              screen_name=None, login=True):
        self.login_status = False
        self.browser.driver.get(MOBILE_BASE_URL)
        if cookies:
            self.log('login using cookies')
            self.set_cookies(cookies)
        elif screen_name and password:
            self.log('login using screen_name and password')
            self.log('screen_name : %s' % screen_name)
            self.log('password : %s' % password)
            # self.browser.fill('session[username_or_email]', screen_name)
            WebDriverWait(self.browser, 10).until(
                lambda browser: self.browser.driver.find_element_by_name('session[username_or_email]'))
            a = self.browser.driver.find_element_by_name('session[username_or_email]')
            a.send_keys(screen_name)
            b = self.browser.driver.find_element_by_name('session[password]')
            b.send_keys(password)
            b.send_keys(Keys.RETURN)
        else:
            self.log_error('login failed')

        if login:
            if self.is_retype():
                self.set_retype(username, phone_no)
            elif not self.is_login():
                raise Exception('failed initiate robot %s : %s' % (screen_name, self.browser.url))
            else:
                pass

            self.login_status = True
            self.log('robot is ready to use')
        else:
            self.log("use without login")

    def is_login(self):
        check = None
        for Error in ERROR_LOGIN:
            if Error in self.browser.url:
                check = False
                break
            check = True
        return check

    def is_retype(self):
        for sl in RETYPE:
            if sl in self.browser.url:
                return True
        return False

    def set_cookies(self, cookies=None):
        try:
            if cookies:
                self.cookies = cookies

            if isinstance(self.cookies, str) or isinstance(self.cookies, unicode):
                try:
                    self.cookies = pickle.loads(self.cookies)
                except Exception, e:
                    self.log(e)
                    self.cookies = json.loads(self.cookies)
            self.verify_cookies()
            self.browser.driver.delete_all_cookies()
            if isinstance(self.cookies, list):
                for c in self.cookies:
                    cookie = COOKIES_FORMAT.format(**c)
                    self.browser.driver.execute_script(cookie)
                return True
            elif isinstance(self.cookies, dict):
                cookie = COOKIES_FORMAT.format(**self.cookies)
                self.browser.driver.execute_script(cookie)
                return True
            else:
                return False
        except Exception, e:
            self.log_error(e)
            return False

    def create_cookie(self):
        try:
            cookies = self.browser.driver.get_cookies()
            cookie = json.dumps(cookies)
            return cookie
        except Exception, e:
            self.log_error(e)
            raise


    def verify_cookies(self):
        if not self.cookies:
            raise Exception('cookies not defined')

        if isinstance(self.cookies, dict):
            self.cookies = [self.cookies]
        cookies = list()
        for cookie in self.cookies:
            if 'name' not in cookie:
                cookie['name'] = None
            if 'value' not in cookie:
                cookie['value'] = None
            if 'path' not in cookie:
                cookie['path'] = None
            if 'domain' not in cookie:
                cookie['domain'] = None
            if 'expiry' not in cookie:
                cookie['expiry'] = None
            if 'expires' not in cookie:
                cookie['expires'] = None
            cookies.append(cookie)
        self.cookies = cookies

    def set_language(self, lang='en_US'):
        try:
            if urlparse.urljoin(MOBILE_BASE_URL, 'home') not in self.browser.url:
                self.browser.visit(MOBILE_BASE_URL)
            flag = self.browser.find_by_xpath("//a[contains(@href, '#header')]").first
            if not flag.value.lower().startswith('back to top'):
                self.browser.visit(urlparse.urljoin(MOBILE_BASE_URL, '/language.php'))
                self.browser.find_by_xpath("//a[contains(@href, '{}')]".format(lang)).first.click()
            return True
        except (WebDriverException, NoSuchElementException, ElementDoesNotExist) as e:
            self.log(e)
            return False

    def set_retype(self, username=None, phone_no=None):
        if "challenge_type=RetypePhoneNumber" in self.browser.url:
            self.log("RetypePhoneNumber {}".format(phone_no))
            phonenumber = self.browser.driver.find_element_by_name('challenge_response')
            if phone_no:
                phonenumber.send_keys(phone_no, Keys.RETURN)
                if "incorrect_solution=true" in self.browser.url:
                    re_message = "Incorrect phone {}".format(phone_no)
                    raise Exception (re_message)
            else:
                re_message = "No phone number {}".format(username)
                raise Exception (re_message)
        elif "challenge_type=RetypeEmail" in self.browser.url:
            self.log("RetypeEmail {}".format(username))
            user_email = self.browser.driver.find_element_by_name('challenge_response')
            if username:
                user_email.send_keys(username, Keys.RETURN)
                if "incorrect_solution=true" in self.browser.url:
                    re_message = "Incorrect email {}".format(username)
                    raise Exception (re_message)
            else:
                re_message = "No email {}".format(username)
                raise Exception(re_message)
        else:
            raise Exception("Need Verification")