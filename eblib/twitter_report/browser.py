from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from splinter import Browser as WebBrowser
from logger import Logger
import base64


class Browser(Logger):

    driver_name = 'phantomjs'
    browser = None
    driver = None

    def __init__(self, driver_name=None, executable_path=None, port=None):
        Logger.__init__(self, self.__class__.__name__)
        if driver_name:
            self.driver_name = driver_name
        self.port = port
        self.executable_path = executable_path

    def start(self, driver_name, proxy=None):
        kwargs = {
        }
        if self.port:
            kwargs['port'] = self.port
        if self.executable_path:
            kwargs['executable_path'] = self.executable_path
        if driver_name == 'phantomjs':
            __dcap = DesiredCapabilities.PHANTOMJS
            __dcap['browserName'] = 'phantom'
            if proxy:
                kwargs['service_args'] = [
                    '--proxy={}:{}'.format(proxy['proxy_host'], proxy['proxy_port']),
                    '--proxy-type={}'.format(proxy['proxy_protocol'])
                ]
                if proxy['proxy_username'] and proxy['proxy_password']:
                    __dcap['phantomjs.page.customHeaders.Proxy-Authorization'] = \
                        "Basic " + base64.b64encode(b'{}:{}'.format(proxy['proxy_username'],
                                                                    proxy['proxy_password']))
            kwargs['desired_capabilities'] = __dcap
        if driver_name == 'chrome':
            if proxy:
                __dcap = dict(DesiredCapabilities.CHROME)
                __dcap['proxy'] = {
                    'proxyType': 'MANUAL',
                    'httpProxy': "{}:{}".format(proxy['proxy_host'], proxy['proxy_port']),
                    'ftpProxy': "{}:{}".format(proxy['proxy_host'], proxy['proxy_port']),
                    'sslProxy': "{}:{}".format(proxy['proxy_host'], proxy['proxy_port']),
                    'noProxy': '',
                    'class': "org.openqa.selenium.Proxy",
                    'autodetect': False
                }

                if proxy['proxy_username'] and proxy['proxy_password']:
                    __dcap['proxy']['socksUsername'] = proxy['proxy_username']
                    __dcap['proxy']['socksPassword'] = proxy['proxy_password']
            options = webdriver.ChromeOptions()
            options.add_argument('headless')
            options.add_argument('disable-gpu')
            options.add_argument('no-sandbox')
            options.add_argument('disable-notifications')
            kwargs['options'] = options
        self.browser = WebBrowser(driver_name=driver_name, **kwargs)
        self.browser.driver.set_window_size(1280, 720)
        self.driver = self.browser.driver

    def quit(self):
        self.browser.quit()
