import time
import re
from urlparse import urlparse
from selenium.webdriver.support.ui import WebDriverWait
from . import BaseObject
from ..auth import BASIC_BASE_URL, APPS_URL
from ..extractor.activity import Activity as ActivityExtractor
from time import sleep


class Activity(BaseObject):

    def __init__(self, browser, driver_name=None, default_wait=None, login=None):
        BaseObject.__init__(self, browser, driver_name, default_wait)
        self.activity_extractor = ActivityExtractor()
        self.login_status = login

    def report_abuse(self, target, options):
        try:
            self.visit("{}{}".format(BASIC_BASE_URL, target))
            time.sleep(10)
            try:
                self.browser.driver.find_element_by_css_selector(
                    "button[data-original-title='More user actions']").click()
            except:
                try:
                    self.browser.driver.find_element_by_css_selector(
                        "button[data-original-title='Tindakan pengguna lainnya']").click()
                except:
                    try:
                        self.browser.driver.find_element_by_css_selector(
                            "button[title='More user actions']").click()
                    except:
                        try:
                            self.browser.driver.find_element_by_css_selector(
                                "button[title='Tindakan pengguna lainnya']").click()
                        except:
                            try:
                                self.browser.driver.find_element_by_css_selector(
                                    "[role='button'][data-testid='userActions']").click()
                                time.sleep(2)
                            except:
                                current_url = self.browser.driver.current_url
                                if re.search("account/access", str(current_url)):
                                    raise Exception("Account error.")
                                else:
                                    raise Exception("Button more user not found")
            try:
                self.browser.driver.find_element_by_class_name("report-text").click()
            except:
                self.browser.driver.find_element_by_css_selector("a[href^='/i/report/user']").click()
                time.sleep(2)

            option = options.split('.')
            value = None
            value2 = None
            value3 = None
            if options == '1':
                value = 'annoying'
            elif options == '2':
                value = 'spam'
                value2 = 'spam_fake_account'
            elif options == '3':
                value = 'compromised'
            elif option[0] == '4':
                value = 'abuse'
                if option[1] == '1':
                    value2 = 'disrespectful_or_offensive'
                elif option[1] == '2':
                    value2 = 'impersonation'
                    if option[2] == '1':
                        value3 = 'Someone_else'
            try:
                self.browser.driver.switch_to.frame(
                    self.browser.driver.find_element_by_id("new-report-flow-frame"))
                WebDriverWait(self.browser, 15).until(
                    lambda browser: self.browser.driver.find_element_by_css_selector(
                        "input[type='radio'][value='{}']".format(value)))
                self.browser.driver.find_element_by_css_selector(
                    "input[type='radio'][value='{}']".format(value)).click()
                self.browser.driver.switch_to_default_content()
                self.browser.driver.find_element_by_id("report-flow-button-next").click()
                time.sleep(2)
                if value2:
                    self.browser.driver.switch_to.frame(
                        self.browser.driver.find_element_by_id("new-report-flow-frame"))
                    self.browser.driver.find_element_by_css_selector(
                        "input[type='radio'][value='{}']".format(value2)).click()
                    self.browser.driver.switch_to_default_content()
                    self.browser.driver.find_element_by_id(
                        "report-flow-button-next").click()
                    time.sleep(2)
                if value3:
                    self.browser.driver.switch_to.frame(
                        self.browser.driver.find_element_by_id("new-report-flow-frame"))
                    self.browser.driver.find_element_by_css_selector(
                        "input[type='radio'][value='{}']".format(value3)).click()
                    self.browser.driver.switch_to_default_content()
                    self.browser.driver.find_element_by_id(
                        "report-flow-button-next").click()
                return 'success'
            except:
                iframe = self.browser.driver.find_element_by_css_selector("iframe")
                link_report = iframe.get_attribute("src")
                self.visit(link_report, 5)
                self.browser.driver.find_element_by_css_selector(
                    "button[value='{}']".format(value)).click()
                time.sleep(2)
                if value2:
                    self.browser.driver.find_element_by_css_selector(
                        "button[value='{}']".format(value2)).click()
                    time.sleep(2)
                if value3:
                    self.browser.driver.find_element_by_css_selector(
                        "button[value='{}']".format(value3)).click()
                return 'success'
        except Exception, e:
            raise e

    def report_abuse_content(self, url, options):
        try:
            parse_url = None
            if 'https://twitter.com/' in url:
                parse_url = url
            elif 'https://' not in url:
                parse_url = "https://{}".format(url)
            else:
                result = urlparse(url)
                parse_url = 'https://twitter.com/{}'.format(result.path)
            self.visit(parse_url)
            time.sleep(10)
            try:
                self.browser.driver.find_element_by_class_name("ProfileTweet-actionButton").click()
            except:
                try:
                    self.browser.driver.find_element_by_css_selector("div[role='button'][data-testid='caret']").click()
                    time.sleep(2)
                except:
                    current_url = self.browser.driver.current_url
                    if re.search("account/access", str(current_url)):
                        raise Exception("Account error.")
                    else:
                        raise Exception("Content in the account is not available.")
            try:
                self.browser.driver.find_element_by_class_name("report-link").click()
            except:
                self.browser.driver.find_element_by_css_selector(
                    "a[href^='/i/report/status/']").click()
                time.sleep(2)

            option = options.split('.')
            value = None
            value2 = None
            value3 = None
            if options == '1':
                value = 'annoying'
            elif options == '2':
                value = 'spam'
                value2 = 'spam_fake_account'
            elif options == '3':
                value = 'sensitive_info'
            elif option[0] == '4':
                value = 'abuse'
                if option[1] == '1':
                    value2 = 'disrespectful_or_offensive'
                elif option[1] == '2':
                    value2 = 'private_info'
                    if option[2] == '1':
                        value3 = 'Someone_else'
            try:
                self.browser.driver.switch_to.frame(
                    self.browser.driver.find_element_by_id("new-report-flow-frame"))
                WebDriverWait(self.browser, 15).until(
                    lambda browser: self.browser.driver.find_element_by_css_selector(
                        "input[type='radio'][value='{}']".format(value)))
                self.browser.driver.find_element_by_css_selector(
                    "input[type='radio'][value='{}']".format(value)).click()
                self.browser.driver.switch_to_default_content()
                self.browser.driver.find_element_by_id("report-flow-button-next").click()
                time.sleep(2)
                if value2:
                    self.browser.driver.switch_to.frame(self.browser.driver.find_element_by_id("new-report-flow-frame"))
                    self.browser.driver.find_element_by_css_selector(
                        "input[type='radio'][value='{}']".format(value2)).click()
                    self.browser.driver.switch_to_default_content()
                    self.browser.driver.find_element_by_id("report-flow-button-next").click()
                    time.sleep(2)
                if value3:
                    self.browser.driver.switch_to.frame(self.browser.driver.find_element_by_id("new-report-flow-frame"))
                    self.browser.driver.find_element_by_css_selector(
                        "input[type='radio'][value='{}']".format(value3)).click()
                    self.browser.driver.switch_to_default_content()
                    self.browser.driver.find_element_by_id("report-flow-button-next").click()
                return "success"
            except:
                iframe = self.browser.driver.find_element_by_css_selector("iframe")
                link_report = iframe.get_attribute("src")
                self.visit(link_report, 5)
                self.browser.driver.find_element_by_css_selector(
                    "button[value='{}']".format(value)).click()
                time.sleep(2)
                if value2:
                    self.browser.driver.find_element_by_css_selector(
                        "button[value='{}']".format(value2)).click()
                    time.sleep(2)
                if value3:
                    self.browser.driver.find_element_by_css_selector(
                        "button[value='{}']".format(value3)).click()
                return 'success'
        except Exception, e:
            raise e

    def create_apps(self, apps_name, apps_desc, apps_url):
        try:
            self.browser.driver.get("https://apps.twitter.com/app/new")
            WebDriverWait(self.browser, 10).until(
                lambda browser: self.browser.driver.find_element_by_id("edit-name"))
            while True:
                try:
                    self.browser.driver.find_element_by_id("edit-name").send_keys(apps_name)
                    self.browser.driver.find_element_by_id("edit-description").send_keys(apps_desc)
                    self.browser.driver.find_element_by_id("edit-url").send_keys(apps_url)
                    self.browser.driver.find_element_by_id("edit-tos-agreement").click()
                    self.browser.driver.find_element_by_css_selector("[id='edit-submit']").click()
                    WebDriverWait(self.browser, 10).until(lambda browser: self.browser.driver.find_element_by_class_name("alert"))
                    eror = self.browser.driver.find_element_by_class_name("alert")
                    if 'The form has become outdated. Copy any unsaved work in the form below and then reload this page.' in eror.text:
                        self.browser.driver.get('https://apps.twitter.com/app/new')
                    else:
                        break
                except:
                    raise
            eror = self.browser.driver.find_element_by_class_name("alert")
            if 'Error' in eror.text:
                error = {
                    'status' : eror.text
                }
                return error
            parse = self.browser.driver.current_url
            url_parse = parse.split('#')
            new_url_recreate = "{}/recreate_keys".format(url_parse[0])
            new_url_token = "{}/recreate_token".format(url_parse[0])
            self.browser.driver.get(new_url_recreate)
            prev = self.browser.driver.current_url
            self.browser.driver.find_element_by_id("edit-submit-api-keys").click()
            WebDriverWait(self.browser, 10).until(
                lambda browser: self.browser.driver.current_url != prev)
            self.browser.driver.get(new_url_token)
            prev2 = self.browser.driver.current_url
            self.browser.driver.find_element_by_id("edit-submit-owner-token").click()
            WebDriverWait(self.browser, 10).until(
                lambda browser: self.browser.driver.current_url != prev2)
            apps = self.browser.driver.find_element_by_class_name("app-settings")
            app_type = apps.find_elements_by_tag_name("span")
            title = self.browser.driver.find_element_by_id("page-title").text
            url = self.browser.driver.current_url
            app_id = url.split('/')[4]
            if not app_id.isdigit():
                app_id = url.split('/'[5])
            token_type = self.browser.driver.find_element_by_class_name("access")
            tok = token_type.find_elements_by_tag_name("span")
            app = {
                'app_id': app_id,
                'app_name': title,
                'consumer_key': app_type[1].text,
                'consumer_secret': app_type[3].text,
                'access_token': tok[1].text,
                'access_token_secret': tok[3].text
            }
            return app
        except Exception, e:
            print "exeption e = {}".format(e)
            raise Exception(e)

    def get_poll_items(self, tweet_id, poll_select=None):
        try:
            blacklist = ['Sign up', 'Log in', 'Daftar', 'Masuk']
            if "/" in tweet_id:
                tweet_id = tweet_id.split("/")
                tweet_id = tweet_id[-1]

            self.browser.driver.get("https://mobile.twitter.com/a/status/{}".format(tweet_id))
            WebDriverWait(self.browser, 10).until(
                lambda browser: self.browser.driver.find_element_by_css_selector("article[role=article] > "
                                                                                 "div > div[dir=auto]"))
            elements = self.browser.driver.find_elements_by_css_selector("article[role=article] "
                                                                         "> div [role=button] > [dir=auto]")
            self.log('Get Poll List')
            poll_list = []
            status = 'failed'
            message = 'Poll Option Not Found'

            if not elements:
                message = 'This account has voted in this poll'

            tweet = self.browser.driver.find_element_by_css_selector("article[role=article] > div > div[dir=auto]")
            tweet_text = tweet.text

            remaining_time = self.browser.driver.find_element_by_css_selector("article[role=article] > div > div > div "
                                                                              "> div > div > div > [dir='auto']")
            remaining_time = remaining_time.text
            remaining_time = remaining_time.encode("ascii", errors="ignore") if remaining_time else ""
            remaining_time = re.sub("^[^ ]+\s(vote(s)?)\s?", "", str(remaining_time))

            for element in elements:
                poll_opt = element.text

                if not poll_opt:
                    poll_opt = element.find_element_by_css_selector("img")
                    poll_opt = poll_opt.get_attribute("src")

                if poll_opt and poll_opt not in blacklist:
                    poll_list.append(poll_opt)
                    if not poll_select:
                        status = 'success'
                        message = 'Success'
                    else:
                        message = 'Selected Poll Option Not Found'
                    if poll_select and poll_select == poll_opt:
                        if not self.login_status:
                            status = 'failed'
                            message = "Need login to poll"
                        else:
                            element.click()
                            self.log('Clicked Selected Poll')
                            sleep(self.default_wait)
                            status = 'success'
                            message = 'Success'
                        break

            result = {
                "status": status,
                "message": message,
                "data": {
                    "option": poll_list,
                    "tweet": tweet_text,
                    "remaining_time": remaining_time
                }
            }
            return result
        except Exception:
            raise

    def execute_poll_items(self, tweet_id, poll_select=None):
        try:
            blacklist = ['Sign up', 'Log in', 'Daftar', 'Masuk']
            if "/" in tweet_id:
                tweet_id = tweet_id.split("/")
                tweet_id = tweet_id[-1]

            self.browser.driver.get("https://mobile.twitter.com/a/status/{}".format(tweet_id))
            # OLD SELECTOR: [data-testid='tweetDetail'] div > div > div > div > div[role=button] > [dir=auto]
            WebDriverWait(self.browser, 10).until(
                lambda browser: self.browser.driver.find_element_by_css_selector("article[role='article'] > div > div:nth-child(4) div[role='button'] [dir='auto']"))
            elements = self.browser.driver.find_elements_by_css_selector("article[role='article'] > div > div:nth-child(4) div[role='button'] [dir='auto']")
            self.log('Get Poll List')
            poll_list = []
            status = 'failed'
            message = 'Poll Option Not Found'

            if not elements:
                message = 'This account has voted in this poll'

            for element in elements:
                poll_opt = element.text

                if poll_opt and poll_opt not in blacklist:
                    poll_list.append(poll_opt)
                    if not poll_select:
                        status = 'success'
                        message = 'Success'
                    else:
                        message = 'Selected Poll Option Not Found'
                    if poll_select and poll_select == poll_opt:
                        if not self.login_status:
                            status = 'failed'
                            message = "Need login to poll"
                        else:
                            element.click()
                            self.log('Clicked Selected Poll')
                            sleep(self.default_wait)
                            status = 'success'
                            message = 'Success'
                        break

            result = {
                "status": status,
                "message": message
            }
            return result
        except Exception:
            raise

    def validate_apps(self, _id):
        try:
            assert _id
            url_show = "{}/{}/show".format(APPS_URL, _id)
            self.browser.driver.get(url_show)
            app_abuse = self.browser.driver.find_elements_by_class_name('app-abuse')
            if app_abuse:
                if app_abuse[0].text == 'Suspended':
                    return {'code': 401, 'message': app_abuse[0].text}
                else:
                    return {'code': 403, 'message': app_abuse[0].text}
            url_keys = "{}/{}/keys".format(APPS_URL, _id)
            self.browser.driver.get(url_keys)
            app_settings = self.browser.driver.find_element_by_class_name('app-settings')
            row_app = app_settings.find_elements_by_class_name('row')
            data = {}
            for a in row_app:
                span = a.find_elements_by_tag_name('span')
                if span[0].text == 'Consumer Key (API Key)':
                        data['consumer_key'] = span[1].text
                if span[0].text == 'Consumer Secret (API Secret)':
                        data['consumer_secret'] = span[1].text
            no_token = self.browser.driver.find_elements_by_id('edit-submit-owner-token')
            if no_token:
                self.browser.driver.find_element_by_id("edit-submit-owner-token").click()
                WebDriverWait(self.browser, 10).until(
                    lambda browser: self.browser.driver.find_element_by_class_name('alert'))
                alert = self.browser.driver.find_elements_by_class_name('alert')
                if alert:
                    generate = alert[0].text
                    data['status_token'] = generate
                else:
                    data['status_token'] = 'generated'
            else:
                data['status_token'] = 'ready'
            app_settings = self.browser.driver.find_element_by_class_name('access')
            row_token = app_settings.find_elements_by_class_name('row')
            for b in row_token:
                span = b.find_elements_by_tag_name('span')
                if span[0].text == 'Access Token':
                        data['access_token'] = span[1].text
                if span[0].text == 'Access Token Secret':
                        data['access_token_secret'] = span[1].text
            return {"code": 200, "message": data}
        except Exception:
            raise
