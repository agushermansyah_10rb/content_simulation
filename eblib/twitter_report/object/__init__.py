import splinter
from time import sleep
from datetime import datetime

from ..logger import Logger


class BaseObject(Logger):

    start = datetime.now()
    default_wait = 3
    visited_url = list()

    def __init__(self, browser, driver_name=None, default_wait=None):
        Logger.__init__(self)
        if isinstance(default_wait, int):
            self.default_wait = default_wait
        if not driver_name:
            driver_name = 'phantomjs'
        if driver_name == 'firefox':
            assert isinstance(browser, splinter.driver.webdriver.firefox.WebDriver)
        elif driver_name == 'chrome':
            assert isinstance(browser, splinter.driver.webdriver.chrome.WebDriver)
        elif driver_name == 'phantomjs':
            assert isinstance(browser, splinter.driver.webdriver.phantomjs.WebDriver)
        else:
            assert isinstance(browser, splinter.driver.webdriver.phantomjs.WebDriver)
        self.browser = browser

    def visit(self, url, wait=None):
        self.log('visit {}'.format(url))
        self.browser.visit(url)
        self.visited_url.append(url)
        if wait is None and not isinstance(wait, int):
            wait = self.default_wait
        sleep(wait)

    def flush_visited_url(self):
        self.visited_url = list()

    def scroll_to_bottom(self):
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        sleep(self.default_wait)

    def set_start(self):
        self.start = datetime.now()
        return self.start

    def build_result(self, data, start=None):
        if not isinstance(start, datetime):
            start = self.start
        result = {
            'crawling_date': start.strftime('%Y-%m-%d %H:%M:%S'),
            'crawling_time': (datetime.now() - start).seconds,
            'visited_url': self.visited_url,
            'result': data
        }
        return result
