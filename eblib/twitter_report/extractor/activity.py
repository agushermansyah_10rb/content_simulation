import re
from . import BaseExtractor


class Activity:

    def __init__(self, parser=None):
        self.parser = parser

    def friend_request(self, html):
        friend_request = FriendRequest(html, self.parser)
        return {
            'data': friend_request.result,
            'next_url': friend_request.next_url
        }


class FriendRequest(BaseExtractor):

    def __init__(self, html, parser=None):
        BaseExtractor.__init__(self, parser=parser)
        self.bs = self.init_bs(html)
        self.result = list()
        self.next_url = None
        self.get()
        self.result = self.verify_data(self.result)

    def get(self):
        peoples = self.bs.select('#friends_center_main > div > div > table > tbody > tr')
        for people in peoples:
            result = {
                'id': '',
                'name': '',
                'accept_url': ''
            }
            name_and_id = people.select_one('td > a')
            if name_and_id:
                result['name'] = name_and_id.text
                match_fbid = re.search(r'(?<=\?uid\=)\d+', name_and_id.get('href', ''))
                if match_fbid:
                    result['id'] = match_fbid.group(0)

            accept_url = people.select('td > div > a')
            if accept_url:
                for url in accept_url:
                    if 'notifications.php?confirm=' in url.get('href', ''):
                        result['accept_url'] = self.verify_url(url['href'])
                        break
            self.result.append(result)

        next_url = self.bs.find('a', href=re.compile(r'\/friends\/center\/requests\/\?'))
        if next_url:
            self.next_url = self.verify_url(next_url['href'])
