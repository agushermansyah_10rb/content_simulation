import urlparse

from pyquery import PyQuery
from bs4 import BeautifulSoup
from ..auth import BASIC_BASE_URL
from ..utils.thank_god_its_regex import *


class BaseExtractor:

    def __init__(self, parser=None):
        self.parser = parser

    def init_pq(self, html):
        pq = PyQuery(html, parser=self.parser)
        pq.make_links_absolute(BASIC_BASE_URL)
        return pq

    def init_bs(self, html):
        pq = self.init_pq(html)
        bs = BeautifulSoup(pq.html(), "lxml")
        return bs

    def verify_data(self, data):
        result = data
        if isinstance(data, dict):
            for k, v in data.iteritems():
                result[k] = self.verify_data(v)
        elif isinstance(data, list) or isinstance(data, set):
            if isinstance(data, set):
                data = list(data)
            data = self.list_values(data)
            for i in range(0, len(data)):
                result[i] = self.verify_data(data[i])
        elif isinstance(data, unicode):
            result = data.encode('ascii', 'xmlcharrefreplace')
        else:
            result = data
        return result

    @staticmethod
    def extract_value(html, remove_new_line=True):
        result = ''
        try:
            html = html[-1] if isinstance(html, list) else html
            if not remove_new_line:
                html = unicode(html.div).strip()
                html = re.sub(r'<.?(div|p|img)[^>]*>', '', html)
            else:
                html = unicode(html.get_text()).strip()
            html = REMOVE_WHITE_SPACE_3.sub(' ', html).strip()
            html = REMOVE_WHITE_SPACE_2.sub(' ', html).strip()
            result = html.encode('ascii', 'xmlcharrefreplace')
        except Exception as e:
            print e
        finally:
            return result

    @staticmethod
    def list_values(lists):
        return [l for l in lists if l]

    @staticmethod
    def verify_url(url, prefix=BASIC_BASE_URL, replace=None):
        if replace is None:
            replace = prefix
        if url is not None and not url.startswith(prefix):
            url = urlparse.urljoin(replace, url)
        return url

    @staticmethod
    def re_extract_username_id(url):
        rusername = None
        rid = None
        match_username = re.search(r'(?<=facebook\.com\/).*(?=\?)', url)
        if match_username:
            username = match_username.group(0)
            if username != 'profile.php':
                rusername = username
            else:
                match_id = re.search('(?<=\?id=)\d+', url)
                if match_id:
                    rid = match_id.group(0)
        return rusername, rid

    @staticmethod
    def extract_username_id(url):
        rusername = None
        rid = None
        parse = urlparse.urlparse(url)
        if parse.path:
            username = str(parse.path).replace('/', '')
            if username:
                if username != 'profile.php':
                    rusername = username
                else:
                    _id = urlparse.parse_qs(parse.query)
                    if _id and _id.get('id'):
                        rid = _id['id'][0]
        return rusername, rid
