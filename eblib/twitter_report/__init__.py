from auth import Auth
from object.activity import Activity
from utils import at_exit
import signal


class Twitter_report(Auth):

    profile = None
    search = None
    post = None
    activity = None
    default_wait = 3

    def __init__(self, cookies=None, username=None, password=None, driver_name=None, screen_name=None,
                 default_wait=None, executable_path=None, port=None):
        Auth.__init__(self, driver_name=driver_name, executable_path=executable_path, port=port)
        if cookies or (username and password):
            self.prepare(cookies, username, password, screen_name=screen_name)
        if isinstance(default_wait, int):
            self.default_wait = default_wait
        at_exit.register(self.atexit)

    def get_browser(self):
        try:
            return self.browser.driver
        except Exception as e:
            self.log(e)
            self.atexit()
            raise

    def prepare(self, cookies, username=None, password=None, cookies_only=True, robot_id=None, phone_no=None,
                screen_name=None, login=True, proxy=None):
        try:
            self.start(self.driver_name, proxy)
            self.login(cookies, username, password, cookies_only=cookies_only, phone_no=phone_no,
                       screen_name=screen_name, login=login)
            self.activity = Activity(self.browser, driver_name=self.driver_name, default_wait=self.default_wait,
                                     login=login)
        except Exception, e:
            self.log(e)
            self.atexit()
            raise


    def create_cookies(self):
        try:
            cookie = self.create_cookie()
            return cookie
        except Exception, e:
            self.log(e)
            self.atexit()
            raise

    def atexit(self):
        try:
            if self.driver_name == 'phantomjs':
                self.browser.driver.service.process.send_signal(signal.SIGTERM)
            self.browser.quit()
        except:
            pass

