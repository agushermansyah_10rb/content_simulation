import requests
import urlparse

__author__ = 'root'


class Producer:
    def __init__(self, host, port, topic):
        self.base_url = "http://{}:{}".format(host, port)
        self.topic = topic

    def put_message(self, msg):
        params = {"message": msg}
        url = "{}?topic={}".format(urlparse.urljoin(self.base_url, "pub"), self.topic)
        requests.post(url, params)


class Consumer:
    def __init__(self, host, port, topic):
        self.base_url = "http://{}:{}".format(host, port)
        self.topic = topic

    def get_message(self):
        url = "{}?topic={}".format(urlparse.urljoin(self.base_url, "pub"), self.topic)

