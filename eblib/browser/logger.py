__author__ = 'Sopan Muhammad'

import logging

class Logger:

    def __init__(self, name=None):
        self.__name = self.__class__.__name__
        if name:
            self.__name = name
        self.__setup_logging()

    def log(self, *strings, **kwargs):
        level = kwargs.pop('level', 'info').upper()
        self.__logger.log(getattr(logging, level), u' '.join(str(s) for s in strings), exc_info=(level=='ERROR'))

    def __setup_logging(self, level='INFO'):
        level = level.upper()
        self.__logger = logging.getLogger(self.__name)
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(getattr(logging, level))

        # create formatter and add it to the handlers
        formatter = logging.Formatter('[%(asctime)s][%(name)s] - %(levelname)s - %(message)s', '%Y-%m-%d %H:%M:%S')
        ch.setFormatter(formatter)

        self.__logger.addHandler(ch)
        self.__logger.setLevel(level)