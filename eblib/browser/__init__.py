import sys
import time
import random
import atexit

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from logger import Logger


class Browser(Logger):
    __DRIVER = {"path": "phantomjs", "name": "phantomjs"}
    __SERVICE_LOG_PATH = "/tmp/ghostdriver.log"
    __UAGENT = [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',
        'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/55.0.2883.87 Chrome/55.0.2883.87 Safari/537.36'
    ]

    def __init__(self, **kwargs):
        Logger.__init__(self)

        self.WAIT = 1

        self.driver = kwargs.pop('driver', '')
        self.driver_path = kwargs.pop('driver_path', '')
        self.__UAGENT = kwargs.pop('uagent', self.__UAGENT)

        if self.driver:
            self.__DRIVER["name"] = self.driver
        if self.driver_path:
            self.__DRIVER["path"] = self.driver_path

        if self.__DRIVER['name'] == 'phantomjs':
            random.shuffle(self.__UAGENT)
            dcap = DesiredCapabilities.PHANTOMJS
            dcap['phantomjs.page.settings.userAgent'] = random.choice(self.__UAGENT)
            self.browser = webdriver.PhantomJS(desired_capabilities=dcap, executable_path=self.__DRIVER['path'], service_log_path=self.__SERVICE_LOG_PATH)
        elif self.__DRIVER['name'] == 'firefox':
            self.browser = webdriver.Firefox()
        elif self.__DRIVER['name'] == 'chrome':
            self.browser = webdriver.Chrome(executable_path=self.__DRIVER['path'], service_log_path=self.__SERVICE_LOG_PATH)
        else:
            raise Exception('no driver found')

        w_width, w_height = 1024, 800
        atexit.register(self.quit)
        self.browser.set_window_size(width=w_width, height=w_height)
        # self.browser.implicitly_wait(1)

        self.log('at {}'.format(sys._getframe().f_code.co_name), '-', 'initialize ... ')
        self.log('at {}'.format(sys._getframe().f_code.co_name), '-', 'using driver {}'.format(self.__DRIVER['name']))
        self.log('at {}'.format(sys._getframe().f_code.co_name), '-', 'set window size {} x {}'.format(w_width, w_height))

    def load_page(self, page):
        try:
            self.log('at {}'.format(sys._getframe().f_code.co_name), '-', 'opening page {}'.format(page))
            self.browser.get(page)
            return True
        except:
            return False

    def submit_form(self, element):
        try:
            element.submit()
            return True
        except TimeoutException:
            return False

    def wait_show_element(self, selector, wait=0, _type='css', action=None):
        if wait: self.WAIT = wait

        try:
            wait = WebDriverWait(self.browser, self.WAIT)
            if _type == 'css':
                element = wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))
            else:
                element = wait.until(EC.visibility_of_element_located((By.XPATH, selector)))

            if action == 'click':
                self.click_selector(selector, _type)
            else:
                return element
        except:
            return None

    def wait_hide_element(self, selector, wait):
        try:
            wait = WebDriverWait(self.browser, wait)
            element = wait.until(EC.invisibility_of_element_located((By.CSS_SELECTOR, selector)))
            return element
        except:
            return None

    def get_element_from(self, object, selector, _type='css'):
        try:
            if _type == 'css':
                return object.find_element_by_css_selector(selector)
            else:
                return object.find_element_by_xpath(selector)
        except NoSuchElementException:
            return None

    def get_elements_from(self, object, selector):
        try:
            return object.find_elements_by_css_selector(selector)
        except NoSuchElementException:
            return []

    def get_element(self, selector, _type='css'):
        return self.get_element_from(self.browser, selector, _type)

    def get_elements(self, selector):
        return self.get_elements_from(self.browser, selector)

    def get_element_from_value(self, object, selector):
        element = self.get_element_from(object, selector)
        return self.get_value(element)

    def get_element_value(self, selector):
        element = self.get_element(selector)
        return self.get_value(element)

    def get_value(self, element):
        if element:
            return element.text
        return None

    def get_attribute(self, element, attribute):
        if element:
            return element.get_attribute(attribute)
        return None

    def get_element_from_attribute(self, fromObject, selector, attribute):
        element = self.get_element_from(fromObject, selector)
        return self.get_attribute(element, attribute)

    def get_element_attribute(self, selector, attribute):
        element = self.get_element(selector)
        return self.get_attribute(element, attribute)

    def get_parent_levels(self, node, levels):
        path = '..'
        if levels > 1:
            for i in range(1, levels):
                path = path + '/..'
        return node.find_element_by_xpath(path)

    def get_parent_node(self, node):
        return node.find_element_by_xpath('..')

    def get_child_nodes(self, node):
        return node.find_elements_by_xpath('./*')

    def select_and_write(self, field, value):
        fieldObject = self.get_element(field)
        fieldObject.send_keys(value)
        return fieldObject

    def wait_and_write(self, field, value):
        object = self.wait_show_element(field, self.WAIT)
        object.send_keys(value)
        return object

    def press_enter(self, object):
        object.send_keys(Keys.RETURN)
        return object

    def click_selector(self, selector, _type='css'):
        element = self.get_element(selector, _type)
        if element:
            try:
                self.click(element)
                return True
            except:
                return False
        return False

    def click_multiple(self, selector):
        exit = False
        elements = self.get_elements(selector)
        if elements:
            for element in elements:
                try:
                    self.click(element)
                    exit = True
                except:
                    pass
        return exit

    def click_next(self, selector, limit=0, wait=0):
        exit = False
        if not wait: wait = self.WAIT
        wdw = WebDriverWait(self.browser, wait)

        n = 0
        while True:
            try:
                if limit and n >= limit: break

                element = wdw.until(EC.element_to_be_clickable((By.CSS_SELECTOR, selector)))
                self.click(element)
                exit = True

                n +=1
            except TimeoutException:
                break  # cannot click the button anymore
        time.sleep(0.5)

        return exit

    def click(self, element):
        self.log('at {}'.format(sys._getframe().f_code.co_name), '-', 'click element {} {}'.format(element.tag_name, element))

        actions = webdriver.ActionChains(self.browser)
        actions.move_to_element(element)
        actions.click(element)
        actions.perform()

    def move_to_element(self, element):
        self.browser.execute_script("return arguments[0].scrollIntoView();", element)
        actions = webdriver.ActionChains(self.browser)
        actions.move_to_element(element)
        actions.perform()

    def scroll_down(self, wait=0.5):
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(wait)

    def scrolling_down(self, times, wait=0.5):
        for i in range(1, times):
            self.scroll_down()
            time.sleep(wait)

    def scrolling_down_until(self, selector):
        times = 1
        last_try_to_scroll = 0
        try_to_scroll_limit = 3
        while not self.is_element_present_by_css_selector(what=selector, wait=1):
            last_height = int(self.browser.execute_script("return document.body.scrollHeight;"))
            self.log('at {}'.format(sys._getframe().f_code.co_name), '-', 'scroll times {}'.format(times))
            self.scroll_down()
            time.sleep(0.5)

            current_height = int(self.browser.execute_script("return document.body.scrollHeight;"))
            if last_height == current_height:
                if last_try_to_scroll < try_to_scroll_limit:
                    self.log('at {}'.format(sys._getframe().f_code.co_name), '-', 'increase try to scroll')
                    last_try_to_scroll += 1
                else:
                    break
            else:
                last_try_to_scroll = 0
            times += 1

    def slow_scroll(self, interval=None, wait=1):
        if interval is None:
            window = self.browser.get_window_size()
            interval = window['height']*0.8

        height = int(self.browser.execute_script("return document.body.scrollHeight;"))
        scroll = interval
        while height > interval and interval:
            self.browser.execute_script("window.scrollTo(0, {});".format(scroll))
            time.sleep(wait)
            scroll += interval
            height -= interval

    def get_field_value(self, record, parent=None):
        exit = None
        if parent:
            if record['type'] == 'attr':
                if record['selector']:
                    exit = self.get_element_from_attribute(parent, record['selector'], record['attr'])
                else:
                    exit = self.get_attribute(parent, record['attr'])
            elif record['type'] == 'text':
                if record['selector']:
                    exit = self.get_element_from_value(parent, record['selector'])
                else:
                    exit = self.get_value(parent)
            elif record['type'] == 'style':
                exit = record['attr']
        else:
            if record['type'] == 'attr':
                exit = self.get_element_attribute(record['selector'], record['attr'])
            elif record['type'] == 'text':
                exit = self.get_element_value(record['selector'])
            elif record['type'] == 'style':
                exit = record['attr']

        return exit

    def load_and_wait(self, url, selector, wait=0):
        if not wait: wait = self.WAIT
        self.load_page(url)
        return self.wait_show_element(selector, wait)

    def is_element_present(self, how, what, wait=10):
        if not wait: wait = self.WAIT
        try:
            WebDriverWait(self.browser, wait).until(
                EC.presence_of_element_located((how, what))
            )
            return True
        except:
            return False

    def is_element_present_by_css_selector(self, what, wait=None):
        return self.is_element_present(By.CSS_SELECTOR, what, wait)

    def is_element_present_by_xpath(self, what, wait=None):
        return self.is_element_present(By.XPATH, what, wait)

    def is_element_present_by_tag(self, what, wait=None):
        return self.is_element_present(By.TAG_NAME, what, wait)

    def is_element_present_by_name(self, what, wait=None):
        return self.is_element_present(By.NAME, what, wait)

    def is_element_present_by_link_text(self, what, wait=None):
        return self.is_element_present(By.LINK_TEXT, what, wait)

    def is_element_present_by_partial_link_text(self, what, wait=None):
        return self.is_element_present(By.PARTIAL_LINK_TEXT, what, wait)

    def is_element_present_by_id(self, what, wait=None):
        return self.is_element_present(By.ID, what, wait)

    def is_element_present_by_class_name(self, what, wait=None):
        return self.is_element_present(By.CLASS_NAME, what, wait)

    def quit(self):
        try:
            self.browser.quit()
        except:
            print "Something Wrong when exit browser"