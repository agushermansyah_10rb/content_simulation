import re
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.webdriver import FirefoxProfile

__author__ = 'GANI'


class SimpleBrowser:

    def __init__(self):
        self.app = None

    @staticmethod
    def parse_proxy(url):
        url = url.strip()
        protocol = re.sub('^(https?).*', '\g<1>', url)
        if re.search('\@', url):
            username = re.sub('^.*\/\/([A-z0-9]+)\:.*', '\g<1>', url)
            password = re.sub('^.*\:([A-z0-9\!]+)\@.*', '\g<1>', url)
        else:
            username = None
            password = None

        ip = re.sub('.*(\@|\/\/)(.*)\:\d+\/?$', '\g<2>', url)
        port = re.sub('.*\:(\d+)\/?$', '\g<1>', url)
        return protocol, username, password, ip, port

    def get_browser(self, browser_name='chrome', executable_path=None, browser_config=None, sentry=None):
        result = None
        try:
            if browser_name == 'chrome':

                kwargs = dict()
                options = webdriver.ChromeOptions()
                options.add_argument("--disable-infobars")

                if browser_config:
                    if 'headless' in browser_config:
                        options.add_argument('headless')
                        options.add_argument('disable-gpu')
                        options.add_argument('no-sandbox')
                    if 'disable-notif' in browser_config:
                        options.add_argument("--disable-notifications")
                    if 'disable-infobars' in browser_config:
                        options.add_argument("--disable-infobars")
                    if 'user_agent' in browser_config:
                        options.add_argument("user-agent={}".format(browser_config['user_agent']))
                    if 'maximized' in browser_config:
                        options.add_argument("--start-maximized")
                    if 'lite_app' in browser_config:
                        options.add_argument("--app=http://www.google.com")
                    if 'slite_app' in browser_config:
                        options.add_argument("--kiosk")
                    if 'user_data' in browser_config:
                        options.add_argument("--user-data-dir={}".format(browser_config['user_data']))
                    if 'proxy' in browser_config:
                        options.add_argument("--proxy-server={}".format(browser_config['proxy']))

                kwargs['chrome_options'] = options

                if executable_path:
                    browser = webdriver.Chrome(executable_path=executable_path, **kwargs)
                else:
                    browser = webdriver.Chrome(**kwargs)
                if browser:
                    # logger.info("browser started", print_log=True)
                    result = browser
            elif browser_name == 'chromium':
                kwargs = dict()
                options = webdriver.ChromeOptions()
                options.add_argument("--disable-infobars")

                if browser_config:
                    if 'headless' in browser_config:
                        options.add_argument('headless')
                        options.add_argument('disable-gpu')
                        options.add_argument('no-sandbox')
                    if 'disable-notif' in browser_config:
                        options.add_argument("--disable-notifications")
                    if 'disable-infobars' in browser_config:
                        options.add_argument("--disable-infobars")
                    if 'user_agent' in browser_config:
                        options.add_argument("user-agent={}".format(browser_config['user_agent']))
                    if 'maximized' in browser_config:
                        options.add_argument("--start-maximized")
                    if 'lite_app' in browser_config:
                        options.add_argument("--app=http://www.google.com")
                    if 'superlite_app' in browser_config:
                        options.add_argument("--kiosk")
                    if 'proxy' in browser_config:
                        options.add_argument("--proxy-server={}".format(browser_config['proxy']))

                kwargs['chrome_options'] = options

                if executable_path:
                    options.binary_location = executable_path

                browser = webdriver.Chrome(**kwargs)

                if browser:
                    result = browser
            elif browser_name == 'firefox':
                options = webdriver.FirefoxOptions()
                profile = FirefoxProfile()

                if browser_config:
                    if 'headless' in browser_config:
                        options.add_argument('-headless')
                    if 'user_agent' in browser_config:
                        profile.set_preference("general.useragent.override", browser_config['user_agent'])
                    if 'proxy' in browser_config:
                        protocol, username, password, proxy_host, proxy_port = self.parse_proxy(browser_config['proxy'])
                        profile.set_preference('network.proxy.type', 1)
                        profile.set_preference('network.proxy.http', proxy_host)
                        profile.set_preference('network.proxy.http_port', int(proxy_port))
                        profile.set_preference('network.proxy.ssl', proxy_host)
                        profile.set_preference('network.proxy.ssl_port', int(proxy_port))
                        profile.set_preference('network.proxy.socks', proxy_host)
                        profile.set_preference('network.proxy.socks_port', int(proxy_port))
                        profile.set_preference('network.proxy.ftp', proxy_host)
                        profile.set_preference('network.proxy.ftp_port', int(proxy_port))

                if executable_path:
                    browser = webdriver.Firefox(executable_path=executable_path,
                                                firefox_options=options,
                                                firefox_profile=profile)
                else:
                    browser = webdriver.Firefox(firefox_options=options,
                                                firefox_profile=profile)

                if browser:
                    if browser_config:
                        if 'maximized' in browser_config:
                            browser.maximize_window()
                    result = browser
            else:
                raise Exception('Undefined browser {}'.format(type))
        except Exception as e:
            print str(e)
            if sentry:
                sentry.captureException()
        finally:
            return result

class SimpleBrowserWait:

    def __init__(self):
        pass

    def wait_until_element_located(self, browser, element, timeout):
        result = False
        try:
            WebDriverWait(browser, timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, element)))
            result = True
        except Exception as e:
            print "[ERROR] SimpleBrowserWait: {}".format(e)
        finally:
            return result

    def wait_until_element_located_xpath(self, browser, element, timeout):
        result = False
        try:
            WebDriverWait(browser, timeout).until(EC.presence_of_element_located((By.XPATH, element)))
            result = True
        except Exception as e:
            print "[ERROR] SimpleBrowserWait: {}".format(e)
        finally:
            return result

    def wait_until_element_clickable(self, browser, element, timeout):
        result = False
        try:
            WebDriverWait(browser, timeout).until(EC.element_to_be_clickable((By.CSS_SELECTOR, element)))
            result = True
        except Exception as e:
            print "[ERROR] SimpleBrowserWait: {}".format(e)
        finally:
            return result

    def wait_until_element_clickable_xpath(self, browser, element, timeout):
        result = False
        try:
            WebDriverWait(browser, timeout).until(EC.element_to_be_clickable((By.XPATH, element)))
            result = True
        except Exception as e:
            print "[ERROR] SimpleBrowserWait: {}".format(e)
        finally:
            return result

class SimpleBrowserUtils:

    def __init__(self):
        pass

    def scroll_into_element(self, browser, element):
        browser.execute_script("arguments[0].scrollIntoView();", element)

