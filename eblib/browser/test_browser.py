import unittest

from SimpleBrowser import SimpleBrowser


class TestBrowser(unittest.TestCase):

    URL_TEST = 'https://www.google.com/'

    def test_browser_chrome(self):
        self.assertTrue(self.run_browser('chrome'))

    def test_browser_firefox(self):
        self.assertTrue(self.run_browser('firefox'))

    def run_browser(self, browser_name):
        verify = False
        try:
            browser = SimpleBrowser()
            if browser_name == 'chrome':
                browser = browser.get_browser('chrome')
            elif browser_name == 'firefox':
                browser = browser.get_browser('firefox')
            browser.get(self.URL_TEST)
            browser.close()
            browser.quit()
            verify = True
        except Exception as e:
            print "ERROR: {}".format(str(e))
        finally:
            return verify

if __name__ == '__main__':
    unittest.main()
