import json
import pickle


class FacebookLib:
    def __init__(self):
        self.cookies = None

    @staticmethod
    def get_cookies(driver, _type='json'):
        try:
            if _type == 'json':
                cookies = json.dumps(driver.get_cookies())
            else:
                cookies = pickle.dumps(driver.get_cookies())
            return cookies
        except:
            raise

    def set_cookies(self, driver, cookies=None, _type='json'):
        try:
            if cookies:
                self.cookies = cookies

            if isinstance(self.cookies, str) or isinstance(self.cookies, unicode):
                if _type == 'json':
                    self.cookies = json.loads(self.cookies)
                else:
                    self.cookies = pickle.loads(self.cookies)
            self.verify_cookie()
            driver.delete_all_cookies()
            if isinstance(self.cookies, list):
                for cookie in self.cookies:
                    if 'expiry' in cookie:
                        driver.add_cookie({k: cookie[k] for k in ('name', 'value', 'domain', 'path', 'expiry')})
                return True
            elif isinstance(self.cookies, dict):
                cookie = "document.cookie = '{name}={value}; path={path}; domain={domain};" \
                         "expiry={expiry}; expires={expires}';".format(**self.cookies)
                driver.execute_script(cookie)
                return True
            else:
                return False
        except:
            raise

    def verify_cookie(self):
        try:
            if isinstance(self.cookies, dict):
                self.cookies = [self.cookies]

            cookies = list()
            for cookie in self.cookies:
                if 'name' not in cookie:
                    cookie['name'] = None
                if 'value' not in cookie:
                    cookie['value'] = None
                if 'path' not in cookie:
                    cookie['path'] = None
                if 'domain' not in cookie:
                    cookie['domain'] = None
                if 'expiry' not in cookie:
                    cookie['expiry'] = None
                if 'expires' not in cookie:
                    cookie['expires'] = None
                cookies.append(cookie)
            self.cookies = cookies
        except:
            raise

if __name__ == '__main__':
    pass
