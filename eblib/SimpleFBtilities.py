import time
import json
import random
import hashlib
import urlparse
import requests

from collections import OrderedDict

from eblib.browser.SimpleBrowser import SimpleBrowser, SimpleBrowserWait


class FBTokenUtils:

    def __init__(self):
        self.browser = SimpleBrowser()
        self.browser_wait = SimpleBrowserWait()
        self.fb_url = 'https://www.facebook.com/'
        self.default_app_id = '202022896916565'
        self.default_redirect_url = 'http://balakutak89.eu5.org/'
        self.default_app_url = 'https://www.facebook.com/v2.10/dialog/oauth?response_type=token' \
                               '&client_id={}&scope={}&redirect_uri={}'
        self.default_scope = 'email,user_hometown,user_religion_politics,publish_actions,user_likes,user_status,' \
                             'user_about_me,user_location,user_tagged_places,user_birthday,user_photos,user_videos,' \
                             'user_education_history,user_posts,user_website,user_friends,user_relationship_details,' \
                             'user_work_history,user_games_activity,user_relationships,ads_management,' \
                             'pages_manage_instant_articles,publish_pages,ads_read,pages_messaging,' \
                             'read_page_mailboxes,business_management,pages_messaging_phone_number,rsvp_event,' \
                             'manage_pages,pages_messaging_subscriptions,user_events,pages_manage_cta,' \
                             'pages_show_list,user_managed_groups,user_actions.books,user_actions.music,' \
                             'user_actions.video,user_actions.fitness,user_actions.news,' \
                             'read_audience_network_insights,read_custom_friendlists,read_insights'
        self.browser_config = {
            # 'headless': True,
            'disable-notif': True,
            'disable-infobars': True,
            'maximized': True
        }

        self.selectors = {
            'email_field': 'input[name="email"]',
            'password_field': 'input[name="pass"]',
            'login_button': 'form[id="login_form"] input[type="submit"]',
            'verify_login': '[id="pagelet_bluebar"]',
            'next_button': 'button[data-testid="nextBtn"]'
        }

    def generate_token(self, email, password, browser='chrome', app_id=None, redirect_url=None):
        result = None
        try:
            if not app_id:
                app_id = self.default_app_id

            if not redirect_url:
                redirect_url = self.default_redirect_url

            app_url = self.default_app_url.format(app_id, self.default_scope, redirect_url)

            if browser == 'chrome':
                browser = self.browser.get_browser(browser_config=self.browser_config)
                if not browser:
                    raise Exception('get browser failed')

                browser.get(self.fb_url)

                email_field = self.browser_wait.wait_until_element_clickable(browser, self.selectors['email_field'], 20)
                if not email_field:
                    raise Exception('email field not found')

                password_field = self.browser_wait.wait_until_element_clickable(browser,
                                                                                self.selectors['password_field'], 20)
                if not password_field:
                    raise Exception('password field not found')

                login_button = self.browser_wait.wait_until_element_clickable(browser,
                                                                              self.selectors['login_button'], 20)
                if not login_button:
                    raise Exception('logi button not found')

                email_field = browser.find_element_by_css_selector(self.selectors['email_field'])
                email_field.send_keys(email)

                time.sleep(random.randint(2, 3))

                password_field = browser.find_element_by_css_selector(self.selectors['password_field'])
                password_field.send_keys(password)

                time.sleep(random.randint(2, 3))

                login_button = browser.find_element_by_css_selector(self.selectors['login_button'])
                login_button.click()

                time.sleep(random.randint(2, 3))

                verify_login = self.browser_wait.wait_until_element_located(browser,
                                                                            self.selectors['verify_login'], 20)
                if not verify_login:
                    raise Exception('login failed')

                browser.get(app_url)

                i = 0
                while i < 4:
                    next_button = self.browser_wait.wait_until_element_clickable(browser,
                                                                                 self.selectors['next_button'], 10)
                    if not next_button:
                        raise Exception('next button not found')

                    btn_next = browser.find_element_by_css_selector(self.selectors['next_button'])
                    btn_next.click()

                    time.sleep(random.randint(2, 3))

                    i += 1

                time.sleep(random.randint(5, 10))

                fat_url = browser.current_url

                parsed_url = urlparse.urlparse(fat_url)
                access_token = parsed_url.fragment
                access_token = access_token.split('&')
                access_token = access_token[0].replace("access_token=", "")

                if not access_token:
                    raise Exception('access token not found')

                result = access_token
        except Exception as e:
            print '[GENERATE TOKEN][ERROR] - {}'.format(str(e))
        finally:
            return result

    def generate_extended_token(self, email, password, device='android'):
        result = None
        try:
            params = OrderedDict()
            params["api_key"] = ""
            params["email"] = email
            params["format"] = "JSON"
            params["locale"] = "en_us"
            params["method"] = "auth.login"
            params["password"] = password
            params["return_ssl_resources"] = "0"
            params["v"] = "1.0"

            if device == 'android':
                params["api_key"] = '882a8490361da98702bf97a021ddc14d'
                device_signature = '62f8ce9f74b12f84c123cc23437a4a32'
            elif device == 'ios':
                params["api_key"] = '3e7c78e35a76a9299309885393b02d97'
                device_signature = 'c1e620fa708a1d5696fb991c1bde5662'
            else:
                raise Exception('device not allowed')

            sig = ""
            for key in params:
                sig += '{}={}'.format(key, params[key])
            sig = "{}{}".format(sig, device_signature)

            m = hashlib.md5()
            m.update(sig)
            sig = m.hexdigest()

            params['sig'] = sig

            response = requests.get('https://api.facebook.com/restserver.php', params=params)
            data_token = json.loads(response.text)
            if data_token:
                result = data_token
        except Exception as e:
            print '[GENERATE EXTENDED TOKEN][ERROR] - {}'.format(str(e))
        finally:
            return result
