import random
import itertools
from datetime import datetime

__author__ = "Dev. ISR"


class MessageGenerator:
    def generate_message(self):
        if int(datetime.now().hour) >= 1 and int(datetime.now().hour) <= 11:
            greet = "Good Morning"
        elif int(datetime.now().hour) > 11 and int(datetime.now().hour) < 14:
            greet = "Good day"
        elif int(datetime.now().hour) >= 14 and int(datetime.now().hour) <= 18:
            greet = "Good Afternoon"
        else:
            greet = "Good evening"

        c_list = list(itertools.product(
            ["Hi,", "Hallo,", "Assalamualaikum,", "{},".format(greet),
             "Hi..", "Hallo..", "Assalamualaikum..", "{}..".format(greet)],
            ["salam kenal... follow back aku ya",
             "salam persahabatan, follow back aku sahabat",
             "salam pertemanan, follow back aku teman",
             "salam saudara, follback aku ya",
             "salam kenal ya temanku, semoga silaturahmi tetap terjaga, follback ya",
             "apa kabar? follback aku ya",
             "apa kabar? jangan lupa follback ya",
             "aku ingin jadi temanmu, follback aku ya",
             "aku ingin jadi sahabatmu, follback aku ya",
             "jangan lupa follback aku ya teman",
             "jangan lupa follback aku ya sahabat",
             "jangan lupa follback aku ya my friend",
             "follback aku ya, kamu kan baik",
             "follback aku ya, jangan lupa hehe"
             "follback aku ya, aku sudah follow kamu teman hehe",
             ],
            [".", "..", "...", "!", "!!", "!!!"]))

        repl = [("  ", " "), (" .", "."), (" !", "!")]
        res = " ".join(random.choice(c_list))
        for s, r in repl:
            res = res.replace(s, r)
        return res.capitalize()


if __name__ == "__main__":
    app = MessageGenerator()
    app.generate_message()