import re
import tweepy
import HTMLParser
import json
from TwitterAPI import TwitterAPI


class MyTwitter:
    def __init__(self, consumer_key, consumer_secret, access_token, access_token_secret):
        self.__auth = tweepy.OAuthHandler(consumer_key=consumer_key, consumer_secret=consumer_secret)
        self.__auth.set_access_token(key=access_token, secret=access_token_secret)

    def twitter_tweet(self, text, tweet_id=None, proxy_url=None):
        # """ This function to post the post """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, proxy=proxy_url, parser=tweepy.parsers.JSONParser())
            if tweet_id:
                result = api.update_status(text, tweet_id, auto_populate_reply_metadata=True)
            else:
                result = api.update_status(text)
            status = True
            message = "Your tweet has been sent."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_retweet(self, tweet_id, proxy_url=None):
        # """ This function to re-post the post """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, proxy=proxy_url, parser=tweepy.parsers.JSONParser())
            result = api.retweet(tweet_id)
            status = True
            message = "Re-tweet has been sent."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_favorite(self, tweet_id, proxy_url=None):
        # """ This function to favorite the post """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, proxy=proxy_url, parser=tweepy.parsers.JSONParser())
            result = api.create_favorite(tweet_id)
            status = True
            message = "Create favorite the tweet successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_unfavorite(self, tweet_id, proxy_url=None):
        # """ This function to unfavorite the post """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, proxy=proxy_url, parser=tweepy.parsers.JSONParser())
            result = api.destroy_favorite(tweet_id)
            status = True
            message = "Create favorite the tweet successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_follow(self, user_id=None, screen_name=None, proxy_url=None):
        # """ This function to follow other """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, proxy=proxy_url, parser=tweepy.parsers.JSONParser())
            if user_id:
                result = api.create_friendship(user_id=user_id)
            elif screen_name:
                result = api.create_friendship(screen_name=screen_name)
            status = True
            message = "Create friendship successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_unfollow(self, user_id=None, screen_name=None, proxy_url=None):
        # """ This function to un-follow your friend """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, proxy=proxy_url, parser=tweepy.parsers.JSONParser())
            if user_id:
                result = api.destroy_friendship(user_id=user_id)
            elif screen_name:
                result = api.destroy_friendship(screen_name=screen_name)
            status = True
            message = "Unfollow your friend successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_search(self, keyword, count=100, max_id=None, since=None, until=None, proxy_url=None, parser=False):
        # """ This function to search by keyword """"
        status = False
        code = None
        result = None
        try:
            if parser:
                api = tweepy.API(self.__auth, proxy=proxy_url, parser=tweepy.parsers.JSONParser())
            else:
                api = tweepy.API(self.__auth, proxy=proxy_url)
            result = api.search(q=keyword, count=count, max_id=max_id, since=since, until=until, tweet_mode='extended')
            status = True
            message = "Search by keyword successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_get_user(self, user_id=None, screen_name=None, proxy_url=None):
        # """ This function to get detail user """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, proxy=proxy_url, parser=tweepy.parsers.JSONParser())
            if user_id:
                result = api.get_user(user_id=user_id)
            elif screen_name:
                result = api.get_user(screen_name=screen_name)
            status = True
            message = "Get user successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_follower_ids(self, user_id=None, screen_name=None, count=None, cursor=None, proxy_url=None):
        # """ This function to get follower ids """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, proxy=proxy_url, parser=tweepy.parsers.JSONParser())
            if user_id:
                result = api.followers_ids(user_id=user_id, cursor=cursor, count=count)
            elif screen_name:
                result = api.followers_ids(screen_name=screen_name, cursor=cursor, count=count)
            status = True
            message = "Get status follower IDS successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_following_ids(self, user_id=None, screen_name=None, count=None, cursor=None, proxy_url=None):
        # """ This function to get following ids """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, proxy=proxy_url, parser=tweepy.parsers.JSONParser())
            if user_id:
                result = api.friends_ids(user_id=user_id, cursor=cursor, count=count)
            elif screen_name:
                result = api.friends_ids(screen_name=screen_name, cursor=cursor, count=count)
            status = True
            message = "Get status following IDS successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_follower(self, user_id=None, screen_name=None, count=None, cursor=None, proxy_url=None):
        # """ This function to get follower account """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, proxy=proxy_url, parser=tweepy.parsers.JSONParser())
            if user_id:
                result = api.followers(user_id=user_id, cursor=cursor, count=count)
            elif screen_name:
                result = api.followers(screen_name=screen_name, cursor=cursor, count=count)
            status = True
            message = "Get status follower successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_following(self, user_id=None, screen_name=None, count=None, cursor=None, proxy_url=None):
        # """ This function to get following account """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, proxy=proxy_url, parser=tweepy.parsers.JSONParser())
            if user_id:
                result = api.friends(user_id=user_id, cursor=cursor, count=count)
            elif screen_name:
                result = api.friends(screen_name=screen_name, cursor=cursor, count=count)
            status = True
            message = "Get following successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_search_user(self, keyword, count=100, page=None, proxy_url=None):
        # """ This function to search by account """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, proxy=proxy_url, parser=tweepy.parsers.JSONParser())
            result = api.search_users(q=keyword, count=count, page=page)
            status = True
            message = "Search by user successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def status_following(self, source, target, proxy_url=None):
        # """ This function to check friendship """"
        status = False
        code = None
        result = None
        message = None
        try:
            api = tweepy.API(self.__auth, parser=tweepy.parsers.JSONParser(), proxy=proxy_url)
            result = api.show_friendship(source_id=source, target_id=target)
            if result:
                if "source" in result["relationship"]:
                    if result["relationship"]["source"]["following"] == True:
                        status = True
                        message = "Get status following successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def status_follower(self, source, target, proxy_url=None):
        # """ This function to check friendship """"
        status = False
        code = None
        result = None
        message = None
        try:
            api = tweepy.API(self.__auth, parser=tweepy.parsers.JSONParser(), proxy=proxy_url)
            result = api.show_friendship(source_id=source, target_id=target)
            if result:
                if "target" in result["relationship"]:
                    if result["relationship"]["target"]["following"] == True:
                        status = True
                        message = "Get status follower successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_user_timeline(self, user_id=None, screen_name=None, since_id=None,
                              max_id=None, count=None, include_rts=None, proxy_url=None,
                              parser=True, since=None, until=None):
        # """ This function to get user timeline """"
        status = False
        code = None
        result = None
        try:
            if parser:
                api = tweepy.API(self.__auth, parser=tweepy.parsers.JSONParser(),
                                 proxy=proxy_url)
            else:
                api = tweepy.API(self.__auth, proxy=proxy_url)

            if user_id:
                result = api.user_timeline(
                    user_id=user_id, since_id=since_id, max_id=max_id, count=count,
                    include_rts=include_rts, since=since, until=until
                )
            elif screen_name:
                result = api.user_timeline(
                    screen_name=screen_name, since_id=since_id, max_id=max_id,
                    count=count, include_rts=include_rts, since=since, until=until
                )
            status = True
            message = "Get user timeline successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_lookup_users(self, user_ids=None, screen_names=None, proxy_url=None):
        # """ This function to lookup users """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, parser=tweepy.parsers.JSONParser(), proxy=proxy_url)
            if user_ids:
                result = api.lookup_users(user_ids=user_ids)
            elif screen_names:
                result = api.lookup_users(screen_names=screen_names)
            status = True
            message = "Lookup users successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_tweet_image(self, text, filename, proxy_url=None):
        # """ This function to tweet with media """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, parser=tweepy.parsers.JSONParser(), proxy=proxy_url)
            media_ids = api.media_upload(filename=filename)
            result = api.update_status(status=text, media_ids=[media_ids["media_id"]])
            status = True
            message = "Send tweet with image successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                elif re.search("The system cannot find the file specified", str(e)):
                    message = "The file image is not found."
                else:
                    message = "Undefined error."
        return status, result, code, message

    def twitter_get_status(self, tweet_id, proxy_url=None):
        # """ This function to get user timeline """"
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(self.__auth, parser=tweepy.parsers.JSONParser(), proxy=proxy_url)
            result = api.get_status(id=tweet_id, tweet_mode='extended')
            status = True
            message = "Get status successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                if re.search("Cannot connect to proxy", str(e)):
                    code = 443
                    message = "Cannot connect to proxy"
                else:
                    message = "Undefined error."
        return status, result, code, message

    def get_home_timeline(self, since_id=None, max_id=None, count=None, proxy_url=None,
                          parser=True):
        status = False
        code = None
        result = None
        try:
            if parser:
                api = tweepy.API(self.__auth, parser=tweepy.parsers.JSONParser(),
                                 proxy=proxy_url)
            else:
                api = tweepy.API(self.__auth, proxy=proxy_url)

            result = api.home_timeline(since_id=since_id, max_id=max_id, count=count)
            status = True
            message = "Get home timeline successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                message = "Undefined error."
        return status, result, code, message

    def trending_topic(self, proxy_url=None):
        status = False
        code = None
        result = None
        try:
            api = tweepy.API(auth_handler=self.__auth, parser=tweepy.parsers.JSONParser(),
                             proxy=proxy_url)
            response = api.trends_place(id="23424846")
            result = response[0]["trends"]
            status = True
            message = "Get trending topic successfully."
        except (tweepy.TweepError, BaseException) as e:
            try:
                message = e[0][0]["message"]
                code = e[0][0]["code"]
            except:
                message = "Undefined error."
        return status, result, code, message


class MyTwitterNew:
    def __init__(self, consumer_key, consumer_secret, access_token, access_token_secret):
        self.api = TwitterAPI(consumer_key=consumer_key, consumer_secret=consumer_secret,
                              access_token_key=access_token, access_token_secret=access_token_secret)

    def send_direct_message(self, text, recipient_id, url=None):
        status = False
        code = None
        result = None
        try:
            event = {
                "event": {
                    "type": "message_create",
                    "message_create": {
                        "target": {
                            "recipient_id": recipient_id
                        },
                        "message_data": {
                            "text": HTMLParser.HTMLParser().unescape(text)
                        }
                    }
                }
            }
            if url:
                event["event"]["message_create"]["message_data"].update({
                    "ctas": [{
                        "type": "web_url",
                        "label": "Lihat Lampiran",
                        "url": url
                    }]
                })
            response = self.api.request("direct_messages/events/new", params=json.dumps(event))
            if response.status_code == 200:
                result = response.json()
                if "event" in result:
                    status = True
                    message = "Send direct message successfully."
                else:
                    if "errors" in result:
                        message = result["errors"][0]["message"]
                        code = result["errors"][0]["code"]
                    else:
                        raise Exception("Failed send message.")
            else:
                result = response.json()
                try:
                    message = result["errors"][0]["message"]
                    code = result["errors"][0]["code"]
                except:
                    raise Exception("undefined error.")
        except Exception, e:
            message = str(e)
        return status, result, code, message


# if __name__ == "__main__":
    # app = MyTwitterNew("cvhEcJGnSeqEj8pJQEHZ4JFdE","aNxfUpgbP5ZVkjwan2exPHLJy3EoD9BVlYdm0kaoFO1FsomUca",
    #                 "1017622815908876288-Wuv5vHEk7jrN8cFde1EaGVQTXk7XSb","HdbcCKkdFUFFjdRoNtfQmxYJHQmqzQXg9v9pdqJQT8InA")
    # a, b, c, d = app.send_direct_message(text="Tes", recipient_id="1173785567248568321")


