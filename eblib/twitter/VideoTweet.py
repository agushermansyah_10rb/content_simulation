import os
import sys
import time

import requests
from requests_oauthlib import OAuth1
from core import logger

MEDIA_ENDPOINT_URL = 'https://upload.twitter.com/1.1/media/upload.json'
POST_TWEET_URL = 'https://api.twitter.com/1.1/statuses/update.json'

CONSUMER_KEY = 'NYsN3r10kAzjAA3TByrozGGTq'
CONSUMER_SECRET = 'fZicrcTX7RcOx39Sf74Q9wuPuxT8G5kVyUpAJEa539gBkokrgF'
ACCESS_TOKEN = '893015173480316928-6CRoyU4bPoiFUOrF2JM9LRZXsGAkl0b'
ACCESS_TOKEN_SECRET = 'iY4oHR8qAwJyunZ1sJVO06ipwfZMjCgbJaNCCSeM5nqI6'


class VideoTweet(object):
    def __init__(self, file_name, **kwargs):
        """
            Defines video tweet properties
        """

        self.oauth = OAuth1(
            client_key=kwargs.get('consumer_key', CONSUMER_KEY),
            client_secret=kwargs.get('consumer_secret', CONSUMER_SECRET),
            resource_owner_key=kwargs.get('access_token', ACCESS_TOKEN),
            resource_owner_secret=kwargs.get('access_token_secret', ACCESS_TOKEN_SECRET)
        )
        self.video_filename = file_name
        self.total_bytes = os.path.getsize(self.video_filename)
        self.media_id = None
        self.processing_info = None
        self.sentry = kwargs.get('sentry', None)
        self.log_info = kwargs.get('log_info', True)
        self.proxy = kwargs.get('proxy', None)

    def upload_init(self):
        """
            Initializes Upload
        """
        logger.info("INIT", self.log_info)

        request_data = {
            'command': 'INIT',
            'media_type': 'video/mp4',
            'total_bytes': self.total_bytes,
            'media_category': 'tweet_video'
        }

        if self.proxy:
            req = requests.post(url=MEDIA_ENDPOINT_URL, data=request_data, auth=self.oauth, proxies={
                "http": self.proxy,
                "https": self.proxy
            })
        else:
            req = requests.post(url=MEDIA_ENDPOINT_URL, data=request_data, auth=self.oauth)
        media_id = req.json()['media_id']

        self.media_id = media_id

        logger.info('Media ID: %s' % str(media_id), self.log_info)

    def upload_append(self):
        """
            Uploads media in chunks and appends to chunks uploaded
        """

        segment_id = 0
        bytes_sent = 0
        _file = open(self.video_filename, 'rb')

        while bytes_sent < self.total_bytes:
            chunk = _file.read(4 * 1024 * 1024)

            logger.info("APPEND", True)

            request_data = {
                'command': 'APPEND',
                'media_id': self.media_id,
                'segment_index': segment_id
            }

            files = {
                'media': chunk
            }

            if self.proxy:
                req = requests.post(url=MEDIA_ENDPOINT_URL, data=request_data, files=files, auth=self.oauth, proxies={
                    "http": self.proxy,
                    "https": self.proxy
                })
            else:
                req = requests.post(url=MEDIA_ENDPOINT_URL, data=request_data, files=files, auth=self.oauth)

            if req.status_code < 200 or req.status_code > 299:
                logger.error("{} - {}".format(req.status_code, req.text), sentry=self.sentry)
                sys.exit(0)

            segment_id += 1
            bytes_sent = _file.tell()

            logger.info('%s of %s bytes uploaded' % (str(bytes_sent), str(self.total_bytes)), self.log_info)

        logger.info('Upload chunks complete.', self.log_info)

    def upload_finalize(self):
        """
            Finalizes uploads and starts video processing
        """
        logger.info('FINALIZE', self.log_info)

        request_data = {
            'command': 'FINALIZE',
            'media_id': self.media_id
        }

        if self.proxy:
            req = requests.post(url=MEDIA_ENDPOINT_URL, data=request_data, auth=self.oauth, proxies={
                "http": self.proxy,
                "https": self.proxy
            })
        else:
            req = requests.post(url=MEDIA_ENDPOINT_URL, data=request_data, auth=self.oauth)
        logger.info(req.json(), self.log_info)

        self.processing_info = req.json().get('processing_info', None)
        self.check_status()

    def check_status(self):
        """
            Checks video processing status
        """
        if self.processing_info is None:
            return

        state = self.processing_info['state']

        logger.info('Media processing status is %s ' % state, self.log_info)

        if state == u'succeeded':
            return

        if state == u'failed':
            logger.error('Media processing status is %s ' % state, sentry=self.sentry)
            raise Exception('Media processing status is %s ' % state)

        check_after_secs = self.processing_info['check_after_secs']

        logger.info('Checking after %s seconds' % str(check_after_secs), self.log_info)
        time.sleep(check_after_secs)

        logger.info('STATUS', self.log_info)

        request_params = {
            'command': 'STATUS',
            'media_id': self.media_id
        }

        if self.proxy:
            req = requests.get(url=MEDIA_ENDPOINT_URL, params=request_params, auth=self.oauth, proxies={
                "http": self.proxy,
                "https": self.proxy
            })
        else:
            req = requests.get(url=MEDIA_ENDPOINT_URL, params=request_params, auth=self.oauth)

        self.processing_info = req.json().get('processing_info', None)
        self.check_status()

    def tweet(self, text):
        """
            Publishes Tweet with attached video
        """
        request_data = {
            'status': text,
            'media_ids': self.media_id
        }

        if self.proxy:
            req = requests.post(url=POST_TWEET_URL, data=request_data, auth=self.oauth, proxies={
                "http": self.proxy,
                "https": self.proxy
            })
        else:
            req = requests.post(url=POST_TWEET_URL, data=request_data, auth=self.oauth)
        return req.json()


if __name__ == '__main__':
    videoTweet = VideoTweet('/home/yuzar/Downloads/mov4.mp4')
    videoTweet.upload_init()
    videoTweet.upload_append()
    videoTweet.upload_finalize()
    videoTweet.tweet("Test Post video")
