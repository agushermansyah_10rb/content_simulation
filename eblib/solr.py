import json
import re
import requests

__author__ = 'root'


class SolrException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class SolrReturn:
    error_msg = None

    def __init__(self, response):
        self.docs = response.documents
        self.num_found = response.total_results
        self.facets = response.facets
        if response.facets:
            self.facets = response.facets["facet_fields"]

        if "error" in response.raw_content:
            raise SolrException(response.raw_content["error"]["msg"])


class SolrLib:
    cursor = None

    def __init__(self):
        self.q = list()
        self.fq = list()

    def get_query(self):
        return " AND ".join(self.q)

    def get_f_query(self):
        return " AND ".join(self.fq)

    def set_solr_cursor(self, solr_cursor):
        self.cursor = solr_cursor

    def set_query(self, query, bracket=True):
        if bracket:
            self.q.append("({})".format(query))
        else:
            self.q.append("{}".format(query))

    def set_f_query(self, query, bracket=True):
        if bracket:
            self.fq.append("({})".format(query))
        else:
            self.fq.append("{}".format(query))

    def keywords(self, keywords):
        self.q.append(query_builder(keywords))

    def reset_query(self):
        self.q = list()
        self.fq = list()

    def solr_search(self, sort=None, start=0, rows=0, fields=None, facet_field=None, pivot=None, facet_limit=-1,
                    facet_mincount=1, facet_offset = None):
        if isinstance(fields, list):
            fields = ",".join(fields)

        query = "*:*"
        if self.q:
            query = " AND ".join(self.q)

        args = {'q': query, "sort": sort, "start": start, "rows": rows, "fl": fields}

        if self.fq:
            args["fq"] = " AND ".join(self.fq)

        if facet_field:
            args["facet"] = "true"

            if isinstance(facet_field, list):
                facet_field = ",".join(facet_field)
            args["facet.field"] = facet_field
            args["facet.mincount"] = facet_mincount
            args["facet.limit"] = facet_limit
            if facet_offset:
                args["facet.offset"] = facet_offset

        if pivot:
            args["facet"] = "true"
            args["facet.pivot"] = pivot
            args["facet.pivot.mincount"] = facet_mincount

            return args

        response = self.cursor.search(**args)
        return SolrReturn(response)

    def get_documents(self, sort=None, start=0, rows=0, fields=None):
        return self.solr_search(sort, start, rows, fields)

    def get_facet(self, facet_field, facet_limit=-1, facet_mincount=1, facet_offset = None):
        return self.solr_search(facet_field=facet_field, facet_limit=facet_limit, facet_mincount=facet_mincount, 
                                facet_offset = facet_offset)

    def get_pivot(self, pivot_field, pivot_mincount=1, pivot_limit=-1, wt='json'):
        args = self.solr_search(pivot=pivot_field, facet_mincount=pivot_mincount)
        args['wt'] = wt
        response = requests.get(self.cursor.base_url + "select", args)
        result = dict()
        if response:
            tmp = json.loads(response.content)
            if "facet_counts" in tmp:
                for facet_key, value in tmp["facet_counts"].iteritems():
                    if facet_key == "facet_pivot" and value:
                        pivot = value[pivot_field]
                        for ent in pivot:
                            result[ent["value"]] = dict(total=ent["count"], pivot=dict())
                            if "pivot" in ent:
                                count = 0
                                for elm in ent["pivot"]:
                                    result[ent["value"]]["pivot"].update({elm["value"]: elm["count"]})
                                    count += 1
                                    if pivot_limit != -1 and count == pivot_limit:
                                        break
        return result

    @staticmethod
    def language_query(language):
        q_temp = None
        if language is not None and language != "":
            langs = language.split(';')
            if len(langs) > 0:
                lang = langs[0]
                q_temp = "language:%s" % lang
                for lang in langs[1:]:
                    q_temp = "%s OR language:%s" % (q_temp, lang)
        return q_temp


def grouping(query):
    # print query
    if query.find(" OR ") < 0:
        return query

    arr_q = query.split(" OR ")
    arr_query = []
    for a in arr_q:
        arr_query.append("({0})".format(a))
    query = " OR ".join(arr_query)
    # print query

    return query


def query_builder(keywords, field=''):
    # print keywords
    if keywords is None:
        return ""

    keywords = keywords.strip()
    if keywords.startswith('LQ:'):
        query = keywords[3:]
        # lower everything between quote
        query = re.sub(r'(["\'])(?:(?=(\\?))\2.)*?\1', lambda pat: ' {} '.format(pat.group().lower()), query)
        query = re.sub(r' (or|and) (?=(?:[^"]*"[^"]*")*[^"]*$)',
                       lambda pat: ' {} '.format(pat.group(1).upper()),
                       query, flags=re.I)
        query = re.sub(r'((^| |\()not) (?=(?:[^"]*"[^"]*")*[^"]*$)',
                       lambda pat: ' {} '.format(pat.group(1).upper()),
                       query, flags=re.I)
    else:
        if field != '':
            field += ':'
        if keywords.startswith('&'):
            keywords = keywords[1:]
        query = ""
        query_not = []
        for keyword in keywords.split(';'):
            # print keyword
            if keyword.startswith("-"):
                # NOT
                query_not.append("-{0}\"{1}\"".format(field, keyword[1:]))
            elif keyword.startswith("&"):
                # AND
                query += " {0} {1}\"{2}\"".format("AND", field, keyword[1:])
            else:
                # OR
                if query == "":
                    query += "{0}\"{1}\"".format(field, keyword)
                else:
                    query += " {0} {1}\"{2}\"".format("OR", field, keyword)

        query = grouping(query)
        if len(query_not) > 0:
            query = "{0} AND ({1})".format(" ".join(set(query_not)), query)

    return "(%s)" % query

    # def query_checker(keyword, query=None, temp=None, field=''):
    #     key = keyword[1:]
    #     if not (key.endswith("*") and key.count(" ") == 0): key = "\"%s\"" % key
    #     if keyword[0] == '-':
    #         if query is None:
    #             query = "NOT %s%s" % (field, key)
    #         else:
    #             query = "%s AND NOT %s%s" % (query, field, key)
    #     elif keyword[0] == '&':
    #         if query is None:
    #             query = "%s%s" % (field, key)
    #         else:
    #             query = "%s AND %s%s" % (query, field, key)
    #     else:
    #         key = keyword
    #         if not key.endswith("*"):
    #             key = "\"%s\"" % key
    #         if query is None:
    #             if temp is None:
    #                 query = "%s%s" % (field, key)
    #             else:
    #                 query = "(%s)" % temp
    #         else:
    #             if temp is None:
    #                 query = "%s OR %s%s" % (query, field, key)
    #             else:
    #                 query = "%s OR (%s)" % (query, temp)
    #                 # print query
    #     return query
    #
    #
    # def query_builder(keywords, field=''):
    #     # print keywords
    #     if keywords.startswith('LQ:'):
    #         query = keywords[3:]
    #         query = re.sub(r' (or|and) ', lambda pat: ' {} '.format(pat.group(1).upper()), query, flags=re.I)
    #         query = re.sub(r'((^| |\()not) ', lambda pat: ' {} '.format(pat.group(1).upper()), query, flags=re.I)
    #         return query
    #     else:
    #         query = None
    #         if field != '':
    #             field += ':'
    #         if keywords.startswith('&'):
    #             keywords = keywords[1:]
    #         # keywords = keywords.replace(";&", " & ")
    #         # keywords = keywords.replace(";-", " - ")
    #         # print keywords
    #         if len(keywords.split(';')) > 1:
    #             # print keywords.split(';')
    #             for keyword in keywords.split(';'):
    #                 keyword = keyword.strip()
    #                 if len(keyword) == 0:
    #                     continue
    #                 if len(keyword.split('&')) > 1:
    #                     tempQuery = None
    #                     for key in keyword.split('&'):
    #                         key = key.strip()
    #                         if len(key) == 0:
    #                             continue
    #                         if len(key.split('-')) > 1:
    #                             idx = 0
    #                             for word in key.split('-'):
    #                                 word = word.strip()
    #                                 if len(word) == 0:
    #                                     continue
    #                                 if idx == 0:
    #                                     tempQuery = "%s" % query_checker('&' + word, tempQuery, field=field)
    #                                 else:
    #                                     tempQuery = "%s" % query_checker('-' + word, tempQuery, field=field)
    #                                 idx += 1
    #                         else:
    #                             tempQuery = "%s" % query_checker('&' + key, tempQuery, field=field)
    #                     query = query_checker(keyword, query, tempQuery, field=field)
    #                 elif len(keyword.split(' -')) > 1:
    #                     tempQuery = None
    #                     idx = 0
    #                     for key in keyword.split(' -'):
    #                         key = key.strip()
    #                         if len(key) == 0:
    #                             continue
    #                         if idx == 0:
    #                             tempQuery = "%s" % query_checker(key, tempQuery, field=field)
    #                         else:
    #                             tempQuery = "%s" % query_checker('-' + key, tempQuery, field=field)
    #                         idx += 1
    #                     query = query_checker(keyword, query, tempQuery, field=field)
    #                 else:
    #                     query = query_checker(keyword, query, field=field)
    #         else:
    #             keyword = keywords.strip()
    #             if len(keyword.split('&')) > 1:
    #                 tempQuery = None
    #                 for key in keyword.split('&'):
    #                     key = key.strip()
    #                     if len(key) == 0:
    #                         continue
    #                     if len(keyword.split('&')) > 1:
    #                         tempQuery = None
    #                         for key in keyword.split('&'):
    #                             key = key.strip()
    #                             if len(key) == 0:
    #                                 continue
    #                             if len(key.split('-')) > 1:
    #                                 idx = 0
    #                                 for word in key.split('-'):
    #                                     word = word.strip()
    #                                     if len(word) == 0:
    #                                         continue
    #                                     if idx == 0:
    #                                         tempQuery = "%s" % query_checker('&' + word, tempQuery, field=field)
    #                                     else:
    #                                         tempQuery = "%s" % query_checker('-' + word, tempQuery, field=field)
    #                                     idx += 1
    #                             else:
    #                                 tempQuery = "%s" % query_checker('&' + key, tempQuery, field=field)
    #                 # print tempQuery
    #                 query = query_checker(keyword, query, tempQuery, field=field)
    #             elif len(keyword.split(' -')) > 1:
    #                 print keyword
    #                 tempQuery = None
    #                 idx = 0
    #                 for key in keyword.split(' -'):
    #                     key = key.strip()
    #                     if len(key) == 0:
    #                         continue
    #                     if idx == 0:
    #                         tempQuery = "%s" % query_checker(key, tempQuery, field=field)
    #                         # print tempQuery
    #                     else:
    #                         tempQuery = "%s" % query_checker('-' + key, tempQuery, field=field)
    #                         # print tempQuery
    #                     idx += 1
    #                 # print tempQuery
    #                 query = query_checker(keyword, query, tempQuery, field=field)
    #                 query = tempQuery
    #             else:
    #                 query = query_checker(keyword, query, field=field)
    #         # print query
    #         return "(%s)" % query
