import re
import time
import random
import tweepy
import oauth2
import urlparse
import mechanize
import cookielib

from pyquery import PyQuery as pq

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

__editor__ = 'Agan Yudistira'

class MyTwitterUtils:
    def __init__(self, consumer_key=None, consumer_key_secret=None, access_token=None, access_token_secret=None, proxy=None):
        self.default_consumer_key = 'NgWdvaOJEnDv3pg1L8WicwCCh'
        self.default_consumer_key_secret = 'JfJM3eKKn3JUEL21tE4tMWusi0xJyajxK6ve9MRYf6SvqsqzaQ'
        self.default_access_token = '730435916825690112-e37bXVJ0MDxcy7EAeszMHipNdUWL6oI'
        self.default_access_token_secret = 'TBkqTDwhpKKTJIP8Mjh2ZobFYVSN3VpgajHy5a19vMR9p'

        self.consumer_key = consumer_key
        self.consumer_key_secret = consumer_key_secret
        self.access_token = access_token
        self.access_token_secret = access_token_secret
        self.proxy = None

    def get_api(self, tweepy_parser=False):
        result = None
        try:
            if self.consumer_key and self.consumer_key_secret and self.access_token and self.access_token_secret:
                auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_key_secret)
                auth.set_access_token(self.access_token, self.access_token_secret)
                if tweepy_parser:
                    api = tweepy.API(auth, proxy=self.proxy, parser=tweepy.parsers.JSONParser())
                else:
                    api = tweepy.API(auth, proxy=self.proxy)
                result = api
            else:
                raise Exception("consumer or access token is empty")
        except (tweepy.TweepError, BaseException) as e:
            try:
                print '[GET API][ERROR] - {}'.format(str(e[0][0]["message"]))
            except:
                print '[GET API][ERROR] - {}'.format(str(e))
        finally:
            return result

    def get_user_data(self, username):
        result = None
        try:
            if self.consumer_key and self.consumer_key_secret and self.access_token and self.access_token_secret:
                auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_key_secret)
                auth.set_access_token(self.access_token, self.access_token_secret)
            else:
                auth = tweepy.OAuthHandler(self.default_consumer_key, self.default_consumer_key_secret)
                auth.set_access_token(self.default_access_token, self.default_access_token_secret)

            api = tweepy.API(auth)

            raw_data = api.get_user(username)._json
            if raw_data:
                result = raw_data
        except (tweepy.TweepError, BaseException) as e:
            try:
                print '[GET USER DATA][ERROR] - {}'.format(str(e[0][0]["message"]))
            except:
                print '[GET USER DATA][ERROR] - {}'.format(str(e))
        finally:
            return result


class MyTwitterTokenUtils:
    def __init__(self, auth_data):
        self.request_token_url = 'https://api.twitter.com/oauth/request_token'
        self.access_token_url = 'https://api.twitter.com/oauth/access_token'
        self.authorize_url = 'https://api.twitter.com/oauth/authorize'

        self.auth_data = auth_data
        self.official_app_name = self.auth_data['official_app_name']
        self.official_consumer_key = self.auth_data['official_consumer_key']
        self.official_consumer_secret = self.auth_data['official_consumer_secret']
        self.screen_name = self.auth_data['screen_name']
        self.password = self.auth_data['password']
        self.proxy = self.auth_data['proxy'] if 'proxy' in self.auth_data and self.auth_data['proxy'] else None
        self.get_ua = ['Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
                       'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
                       'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36']

    def get_browser(self):
        result = None
        try:
            debug_browser = False
            browser = mechanize.Browser()

            # set cookies
            cookies = cookielib.LWPCookieJar()
            browser.set_cookiejar(cookies)

            u_agent = random.choice(self.get_ua)

            if self.proxy:
                # set proxy
                raw_proxy = {
                    'http': self.proxy
                }
                browser.set_proxies(raw_proxy)

            # browser option
            browser.set_handle_equiv(True)
            browser.set_handle_redirect(True)
            browser.set_handle_referer(True)
            browser.set_handle_robots(False)

            # debugging message
            browser.set_debug_http(debug_browser)
            browser.set_debug_redirects(debug_browser)
            browser.set_debug_responses(debug_browser)

            # refresh avoid hangs
            browser.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)
            # set user agent
            browser.addheaders = [
                ('User-Agent', '{}'.format(u_agent))
            ]

            result = browser
        except Exception as e:
            print '[GET BROWSER][ERROR] - {}'.format(str(e))
        finally:
            return result

    def get_browser_automation(self, browser_name='chrome', executable_path=None, browser_config=None):
        result = None
        try:
            if browser_name == 'chrome':

                kwargs = dict()
                options = webdriver.ChromeOptions()
                options.add_argument("--disable-infobars")
                capabilities = webdriver.DesiredCapabilities.CHROME

                if browser_config:
                    if 'headless' in browser_config:
                        options.add_argument('headless')
                        options.add_argument('disable-gpu')
                        options.add_argument('no-sandbox')
                    if 'disable-notif' in browser_config:
                        options.add_argument("--disable-notifications")
                    if 'disable-infobars' in browser_config:
                        options.add_argument("--disable-infobars")
                    if 'user_agent' in browser_config:
                        options.add_argument("user-agent={}".format(browser_config['user_agent']))
                    if 'maximized' in browser_config:
                        options.add_argument("--start-maximized")
                    if 'lite_app' in browser_config:
                        options.add_argument("--app=http://www.google.com")
                    if 'slite_app' in browser_config:
                        options.add_argument("--kiosk")
                    if 'user_data' in browser_config:
                        options.add_argument("--user-data-dir={}".format(browser_config['user_data']))
                    if 'proxy' in browser_config:
                        options.add_argument("--proxy-server={}".format(browser_config['proxy']))

                kwargs['chrome_options'] = options

                if executable_path:
                    browser = webdriver.Chrome(desired_capabilities=capabilities, executable_path=executable_path, **kwargs)
                else:
                    browser = webdriver.Chrome(**kwargs)
                if browser:
                    # logger.info("browser started", print_log=True)
                    result = browser

            else:
                raise Exception('Undefined browser {}'.format(type))
        except Exception as e:
            # logger.error(str(e), print_log=True)
            print str(e)
        finally:
            return result

    def get_request_url(self):
        result = None
        try:
            consumer = oauth2.Consumer(key=self.official_consumer_key, secret=self.official_consumer_secret)
            client = oauth2.Client(consumer)

            response, content = client.request(self.request_token_url, "GET")
            if response['status'] != '200':
                raise Exception("Invalid response {}.".format(response['status']))

            request_token = dict(urlparse.parse_qsl(content))
            self.auth_data['access_token'] = request_token['oauth_token']
            self.auth_data['access_token_secret'] = request_token['oauth_token_secret']

            result = "{}?oauth_token={}".format(self.authorize_url, request_token['oauth_token'])
        except Exception as e:
            print '[GET REQUEST URL][ERROR] - {}'.format(str(e))
        finally:
            return result

    def get_verifier_code(self, request_url, browser_name='mechanize'):
        result = None
        browser = None
        try:
            if browser_name =='mechanize':
                browser = self.get_browser()
                browser.open(request_url)

                # begin automation
                browser.select_form(nr=0)
                browser.form['session[username_or_email]'] = self.screen_name
                browser.form['session[password]'] = self.password
                response = browser.submit()

                # getting result
                html = response.read()
            else:
                b_config = {
                    'user_agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
                    'disable-notif': True,
                    'disable-infobars': True,
                    'maximized': True,
                    'headless': True
                }

                if self.proxy:
                    b_config['proxy'] = self.proxy

                browser = self.get_browser_automation(browser_name='chrome', browser_config=b_config)
                browser.get(request_url)

                WebDriverWait(browser, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'input[id="username_or_email"]')))
                WebDriverWait(browser, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'input[id="password"]')))
                WebDriverWait(browser, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'input[id="allow"]')))

                user_field = browser.find_element_by_css_selector('input[id="username_or_email"]')
                pass_field = browser.find_element_by_css_selector('input[id="password"]')
                send_btn = browser.find_element_by_css_selector('input[id="allow"]')

                user_field.send_keys(self.screen_name)
                time.sleep(2)
                pass_field.send_keys(self.password)
                time.sleep(2)
                send_btn.click()

                html = browser.page_source
                WebDriverWait(browser, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'code')))

            soup = BeautifulSoup(html, 'lxml')
            verifier_code = soup.find('code')
            verifier_code = re.sub(r'<[^>]*>', '', str(verifier_code))

            if verifier_code:
                result = verifier_code
        except Exception as e:
            print '[GET VERIFIER CODE][ERROR] - {}'.format(str(e))
        finally:
            if browser_name != 'mechanize' and browser:
                browser.close()
                browser.quit()
            return result

    # def get_verifier_code(self, request_url):
    #     result = None
    #     try:
    #         browser = self.get_browser()
    #         browser.open(request_url)
    #
    #         # begin automation
    #         browser.select_form(nr=0)
    #         browser.form['session[username_or_email]'] = self.screen_name
    #         browser.form['session[password]'] = self.password
    #         response = browser.submit()
    #
    #         # getting result
    #         html = response.read()
    #         element = pq(html)
    #         verifier_code = element("code")
    #         verifier_code = re.sub(r'<[^>]*>', '', str(verifier_code))
    #
    #         if verifier_code:
    #             result = verifier_code
    #     except Exception as e:
    #         print '[GET VERIFIER CODE][ERROR] - {}'.format(str(e))
    #     finally:
    #         return result

    def generate_token(self):
        result = None
        try:
            request_url = self.get_request_url()
            if request_url:
                verifier_code = self.get_verifier_code(request_url)

                if verifier_code:
                    authorize_consumer = oauth2.Consumer(key=self.official_consumer_key, secret=self.official_consumer_secret)
                    authorize_token = oauth2.Token(key=self.auth_data['access_token'], secret=self.auth_data['access_token_secret'])
                    authorize_token.set_verifier(verifier_code)
                    client = oauth2.Client(authorize_consumer, authorize_token)
                    response, content = client.request(self.access_token_url, "POST")
                    access_token = dict(urlparse.parse_qsl(content))
                    access_token['access_token'] = access_token['oauth_token']
                    access_token['access_token_secret'] = access_token['oauth_token_secret']
                    access_token['consumer_key'] = self.official_consumer_key
                    access_token['consumer_secret'] = self.official_consumer_secret

                    del access_token['oauth_token']
                    del access_token['oauth_token_secret']
                    result = access_token
        except Exception as e:
            print '[GENERATE TOKEN][ERROR] - {}'.format(str(e))
        finally:
            return result

    def generate_token_chrome(self):
        result = None
        try:
            request_url = self.get_request_url()
            if request_url:
                verifier_code = self.get_verifier_code(request_url, 'chrome')

                if verifier_code:
                    authorize_consumer = oauth2.Consumer(key=self.official_consumer_key, secret=self.official_consumer_secret)
                    authorize_token = oauth2.Token(key=self.auth_data['access_token'], secret=self.auth_data['access_token_secret'])
                    authorize_token.set_verifier(verifier_code)
                    client = oauth2.Client(authorize_consumer, authorize_token)
                    response, content = client.request(self.access_token_url, "POST")
                    access_token = dict(urlparse.parse_qsl(content))
                    access_token['access_token'] = access_token['oauth_token']
                    access_token['access_token_secret'] = access_token['oauth_token_secret']
                    access_token['consumer_key'] = self.official_consumer_key
                    access_token['consumer_secret'] = self.official_consumer_secret

                    del access_token['oauth_token']
                    del access_token['oauth_token_secret']
                    result = access_token
        except Exception as e:
            print '[GENERATE TOKEN][ERROR] - {}'.format(str(e))
        finally:
            return result
