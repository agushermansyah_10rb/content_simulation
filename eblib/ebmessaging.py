import requests

DEFAULT_HOST = '0.0.0.0'
DEFAULT_PORT = 1810


class eBMessaging:
    def __init__(self, host=DEFAULT_HOST, port=DEFAULT_PORT):
        self.host = host
        self.port = port

    def send(self, client_event, message, room='broadcast'):
        param = {'client_event': client_event, 'message': message, 'room': room}

        r = requests.post('http://{}:{}/set_message'.format(self.host, self.port), data=param)
        return r

     #isr_livetweet_location