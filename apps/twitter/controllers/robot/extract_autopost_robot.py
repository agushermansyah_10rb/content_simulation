import datetime
import json
import time
import os
import random
import re
import requests
from shutil import copyfile

import MySQLdb

from DBUtils.PersistentDB import PersistentDB
from datetime import timedelta
from ssdb import SSDB

from apps.twitter.models.autopost_robot import AutopostRobot
from apps.twitter.models.main.additional_robot_psychography import AdditionalRobotPsychography
from apps.twitter.models.main.personality_psychography import PersonalityPsychography
from apps.twitter.models.main.psychography_catalog import PsychographyCatalog
from apps.twitter.models.main.robot_schedule import RobotSchedule
from apps.twitter.models.mongo_db import MongoDb
from apps.twitter.models.autopost_catalog import AutopostCatalog
from apps.twitter.models.robot import Robot
# from content_management.utils import Utils
from core import logger
from core.controller import Controller
from helper.general import init_pusher_beanstalk, init_worker_beanstalk, get_age_from_mapping, modify_tweet
from difflib import SequenceMatcher
from mysolr.mysolr import Solr
from eblib.solr import SolrLib, SolrException


class ExtractTweet(Controller):
    def __init__(self):
        Controller.__init__(self)
        self.timeout = self.config.get('beanstalk', 'timeout')
        self.ssdb = SSDB(host=self.config.get("ssdb", "server"), port=int(self.config.get("ssdb", "port")))

    def get_extract_tweet(self, robot_id=None, schedule_type='private', total_tweet=None, types=None, single=False,
                          utilobj=None):
        content_type = ["campaign", "private", "trend", "lifetime"]

        conn = self.pool_content_management.connection()
        cursor = conn.cursor(MySQLdb.cursors.DictCursor)

        robot_model = Robot(conn, cursor)
        ac_model = AutopostCatalog(conn, cursor)
        psycho_catalog_model = PsychographyCatalog(conn, cursor)
        add_psycho_model = AdditionalRobotPsychography(conn, cursor)
        person_psycho_model = PersonalityPsychography(conn, cursor)

        mongo_model = MongoDb()
        list_id, list_cluster = [], []
        try:
            solr_lib = SolrLib()
            solr_lib.set_solr_cursor(self.solr_robot_tweet)
            ip_address = self.get_ip_address()

            if types == 'all':
                schedule_type = types

            robot_ids = robot_id
            if robot_ids and robot_ids != '':
                if not utilobj:
                    psychography_catalog = psycho_catalog_model.get_psychographycal_catalog().fetchall
                    utilobj = Utils(dbname="mongo_db", configObj=self.config, get_ugc=False,
                                    psychography_catalog=psychography_catalog, disable_logger=True)

                robots = robot_model.get_robot(robot_id=robot_id).fetchall

                dict_demography = {}
                dict_location = {}

                path = "{}/soldier".format(self.config.get('path', 'log_path'))

                if not os.path.exists(path):
                    os.makedirs(path)

                date_only = datetime.datetime.now().strftime("%Y-%m-%d")
                robot_list = [str(r['screen_name']) for r in robots]

                for robot in robots:
                    rec_count = int(self.config.get("etc", "rec_count"))
                    rec_count_ori = int(self.config.get("etc", "rec_count"))

                    if total_tweet:
                        rec_count = total_tweet
                        rec_count_ori = total_tweet

                    path_soldier = "{}/{}".format(path, robot_id)

                    if not os.path.exists(path_soldier):
                        os.makedirs(path_soldier)

                    filename_soldier = "{}".format(date_only)
                    file_soldier = open("{}/{}.txt".format(path_soldier, filename_soldier), "a")
                    file_soldier.write("======================= Start ==========================\n")

                    # list_sac = soldier_autopost_catalog.get_notjoin(robot_id=soldier['robot_id']).fetchall
                    list_sac = None
                    tweets_text = []

                    logger.info("Process Robot {}".format(robot_id), True)

                    clone_data = {}

                    dict_demography['age'] = get_age_from_mapping(robot['age'])
                    dict_demography['gender'] = robot['gender']
                    dict_demography['religion'] = robot['religion']

                    if robot['city'] is not None:
                        l_city = robot['city'].split("|")
                        if len(l_city) > 2:
                            dict_location['city'] = l_city[2]
                        else:
                            dict_location['city'] = None
                    else:
                        dict_location['city'] = None
                    dict_location['province'] = (robot['province'].split("|")[1]) \
                        if robot['province'] is not None else None
                    dict_location['country'] = (robot['country'].split("|")[0]) \
                        if robot['country'] is not None else None
                    dict_demography['location'] = dict_location

                    psychography_clone = None
                    if clone_data:
                        psychography_clone = clone_data['psychography']

                    list_recs = []
                    if schedule_type == 'all' or schedule_type == 'campaign':
                        list_scene = ss_model.get_scene(soldier_id=soldier['soldier_id'], campaign_status=1).fetchall
                        logger.info("Total scene {}".format(len(list_scene)), True)
                        for scene in list_scene:
                            if datetime.datetime.now() >= scene['start_date'] and datetime.datetime.now() <= scene['end_date']:
                                scene_id = scene['scene_id']
                                campaign_id = scene['campaign_id']
                                campaign_psychography = ss_model.get_scene_psycho(soldier_id=soldier['soldier_id'],
                                                                                  campaign_status=1,
                                                                                  scene_id=scene_id).fetchall

                                file_soldier.write("Demography >> {}\n".format(dict_demography))
                                file_soldier.write("Campaign >> {}\n".format(campaign_psychography))

                                rec_tweets = utilobj.get_robot_campaign_autopost(dict_demography, campaign_psychography)

                                scene_channel = sc_model.get_sc(scene_id=scene_id).fetchall
                                for v in scene_channel:
                                    contents = cp_model.get_content(channel_id=v['channel_id'],
                                                                    sentiment=str(v['sentiment']),
                                                                    emotion=v['emotions'],
                                                                    order_by='created_at DESC').fetchall
                                    if not contents:
                                        continue

                                    for vv in contents[:rec_count_ori]:
                                        if random.randint(0, 1) == 0:
                                            continue

                                        vv['_id'] = vv['source_id']
                                        vv['lifetime'] = [None, None]
                                        vv['gender'] = None
                                        vv['clean_tweet'] = vv['description']
                                        vv['location'] = [None]
                                        vv['age'] = None
                                        vv['religion'] = None
                                        vv['keywords'] = []

                                        if vv['image_name']:
                                            try:
                                                picture = re.search('([^/]+$)', vv['image_name']).group(0)
                                                copyfile(
                                                    '{}/{}'.format(self.config.get('file_upload', 'content_media_path'),
                                                                   vv['image_name']),
                                                    '{}/{}'.format(
                                                        self.config.get('file_upload', 'image_path_schedule'),
                                                        picture)
                                                )

                                                vv['image_name'] = picture
                                            except Exception, ee:
                                                logger.warning(ee)
                                                vv['image_name'] = None

                                        rec_tweets.append(vv)

                                file_soldier.write("Rec Tweet >> {}\n".format(len(rec_tweets)))

                                if rec_tweets:
                                    index_name = "{}__{}".format(scene_id, campaign_id)
                                    list_recs.append({index_name: rec_tweets})

                    rec_tweets_now = []
                    list_id = []
                    while rec_count > 0 and len(list_recs) > 0:
                        index = 0
                        for lr in list_recs:
                            if random.randint(0, 1) == 1:
                                for sid in lr:
                                    loop = True
                                    index_rec = 0
                                    if len(lr[sid]) > 0:
                                        while loop and len(lr[sid]) > 0:
                                            tweet = lr[sid][index_rec]
                                            tweet_id = tweet['_id']
                                            if tweet_id not in list_id:
                                                list_id.append(tweet_id)
                                                loop = False
                                            else:
                                                del lr[sid][index_rec]

                                        if len(lr[sid]) > 0:
                                            sid_now, cid_now = str(sid).split('__')
                                            tweet_clean = tweet['clean_tweet']

                                            # ====== set mentions
                                            list_mention = scene_mention_model.find_all(scene_id=sid_now, status=1).fetchall
                                            for lm in list_mention:
                                                mention = " @{} ".format(lm['mention'])
                                                check_limit = len(tweet_clean) + len(mention)
                                                if check_limit <= 280:
                                                    tweet_clean = "{}{}".format(mention, tweet_clean)

                                            # === set hashtags
                                            list_hashtag = scene_hashtag_model.find_all(scene_id=sid_now, status=1).fetchall
                                            for lh in list_hashtag:
                                                hashtag = " #{}".format(lh['hashtag'])
                                                check_limit = len(tweet_clean) + len(hashtag)
                                                if check_limit <= 280:
                                                    tweet_clean = "{}{}".format(tweet_clean, hashtag)

                                            tweet['clean_tweet'] = tweet_clean
                                            tweet['campaign_id'] = cid_now
                                            rec_tweets_now.append(tweet)
                                            del lr[sid][index_rec]
                                            rec_count -= 1
                                    else:
                                        del list_recs[index]
                            index += 1

                    logger.info("RTN {}".format(len(rec_tweets_now)), True)
                    if rec_tweets_now:
                        _type = 'campaign'
                        logger.info("Campaign {}".format(len(rec_tweets_now)), True)
                        list_id = self.insert_rec_tweets(conn, content_type, list_sac, mongo_model, rec_tweets_now,
                                                         skc_model, soldier, soldier_autopost_catalog, tweets_text,
                                                         _type, robot_list, file_soldier, rec_count_ori, solr_lib,
                                                         sm_model)
                    if rec_count > 0 and (schedule_type == 'private' or schedule_type == 'all' ):
                        main_psychography = add_psycho_model.get_additional_psychography(soldier['robot_id'])
                        person_psycho = person_psycho_model.get_personality_psychography(soldier['soldier_id']).fetchall

                        merge_psycho = person_psycho
                        for m in main_psychography:
                            if not any(mp.get('psychography_id', None) == m['psychography_id'] for mp in merge_psycho):
                                merge_psycho += (m,)

                        main_psychography = merge_psycho

                        logger.info("Main Psycho : {}".format(len(main_psychography)), True)
                        psychography = {}
                        if psychography_clone or main_psychography:
                            if psychography_clone:
                                psychography = eval(psychography_clone)

                            for lv1 in psychography:
                                rep_lv1 = lv1.replace('_', ' ')
                                psychography[rep_lv1] = psychography.pop(lv1)
                                lv1 = rep_lv1

                                for lv2 in psychography[lv1]:
                                    rep_lv2 = lv2.replace('_', ' ')
                                    psychography[lv1][rep_lv2] = psychography[lv1].pop(lv2)
                                    lv2 = rep_lv2

                                    if 'emotion' in psychography[lv1][lv2]:
                                        psychography[lv1][lv2]['emotions'] = psychography[lv1][lv2].pop('emotion')

                                    if isinstance(psychography[lv1][lv2]['emotions'], str):
                                        psychography[lv1][lv2]['emotions'] = [psychography[lv1][lv2]['emotions']]

                            if psychography or main_psychography:
                                file_soldier.write("Demography >> {}\n".format(dict_demography))
                                file_soldier.write("Private >> {}\n".format(main_psychography))
                                file_soldier.write("Clone >> {}\n".format(psychography))

                                rec_tweets = []
                                try:
                                    rec_tweets = utilobj.get_robot_personality_autopost(dict_demography, main_psychography, psychography)
                                    if rec_tweets['main_autopost_content']:
                                        rec_tweets = rec_tweets['main_autopost_content']
                                    else:
                                        rec_tweets = rec_tweets['additional_autopost_content']
                                except Exception, e:
                                    self.error_logging(e)
                                    file_soldier.write("Error >> {}\n".format(e))

                                file_soldier.write("Rec Tweet >> {}\n".format(len(rec_tweets)))

                                _type = 'private'
                                logger.info("Personality {}".format(len(rec_tweets)), True)
                                list_id = self.insert_rec_tweets(conn, content_type, list_sac, mongo_model, rec_tweets,
                                                                 skc_model, soldier, soldier_autopost_catalog,
                                                                 tweets_text, _type, robot_list, file_soldier, rec_count,
                                                                 solr_lib, sm_model)
                            else:
                                pass

                        robot_channel = rc_model.get_rc(robot_id=soldier['robot_id']).fetchall
                        for v in robot_channel:
                            contents = cp_model.get_content(channel_id=v['channel_id'], sentiment=str(v['sentiment']),
                                                            emotion=v['emotions'], order_by='created_at DESC').fetchall
                            if not contents:
                                continue

                            rec_tweets = []
                            for vv in contents[:rec_count_ori]:
                                if random.randint(0, 1) == 0:
                                    continue

                                vv['_id'] = vv['source_id']
                                vv['lifetime'] = [None, None]
                                vv['gender'] = None
                                vv['clean_tweet'] = vv['description']
                                vv['location'] = [None]
                                vv['age'] = None
                                vv['religion'] = None
                                vv['keywords'] = []

                                if vv['image_name']:
                                    try:
                                        picture = re.search('([^/]+$)', vv['image_name']).group(0)
                                        copyfile(
                                            '{}/{}'.format(self.config.get('file_upload', 'content_media_path'),
                                                           vv['image_name']),
                                            '{}/{}'.format(self.config.get('file_upload', 'image_path_schedule'),
                                                           picture)
                                        )

                                        vv['image_name'] = picture
                                    except Exception, ee:
                                        logger.warning(ee)
                                        vv['image_name'] = None

                                rec_tweets.append(vv)

                            self.insert_rec_tweets(conn, content_type, list_sac, mongo_model, rec_tweets,
                                                   skc_model, soldier, soldier_autopost_catalog,
                                                   tweets_text, 'private', robot_list, file_soldier, rec_count,
                                                   solr_lib, sm_model)

                    file_soldier.close()
                    conn.commit()
                    conn_main.commit()
        except Exception, e:
            conn.rollback()
            conn_main.rollback()
            logger.error(e, sentry=self.sentry)
        finally:
            cursor.close()
            conn.close()
            cursor_main.close()
            conn_main.close()

        return list_id

    def insert_rec_tweets(self, conn, content_type, list_sac, mongo_model, rec_tweets, skc_model, soldier,
                          soldier_autopost_catalog, tweets_text, _type, robot_list, file_soldier, rec_count, solr_lib,
                          sm_model=None):
        week_limit = 0
        week_start = 0
        if content_type:
            pub_date = None
        else:
            pub_date = None

        db_name = "databank"
        table_name = "soldier_post"
        list_id = []

        for tweet in rec_tweets[:rec_count]:
            similiar_status = False

            tweet_id = tweet['_id']
            lifetime_start = tweet['lifetime'][0].strftime("%Y-%m-%d") if tweet['lifetime'][0] else None
            lifetime_end = tweet['lifetime'][1].strftime("%Y-%m-%d") if tweet['lifetime'][1] else None
            sentiment = None
            emotions = None
            keywords = None

            campaign_id = tweet['campaign_id'] if 'campaign_id' in tweet else None

            _class = None
            if 'class' in tweet:
                _class = tweet['class']

            if week_start > week_limit:
                week_start = 0
            date_assign = (datetime.datetime.now() + datetime.timedelta(days=week_start)).strftime(
                "%Y-%m-%d")
            pub_day = (datetime.datetime.now() + datetime.timedelta(days=week_start)).strftime("%Y%m%d")
            pub_month = (datetime.datetime.now() + datetime.timedelta(days=week_start)).strftime("%Y%m")
            pub_year = (datetime.datetime.now() + datetime.timedelta(days=week_start)).strftime("%Y")
            print lifetime_start, lifetime_end, date_assign

            if lifetime_start <= date_assign:
                if not lifetime_end or date_assign <= lifetime_end:
                    pub_date = date_assign
                    week_start += 1
                    logger.info(
                        'Available lifetime, ID {} assign to date {}'.format(tweet_id, pub_date),
                        print_log=True)
                else:
                    pub_date = tweet['lifetime'][1].strftime("%Y-%m-%d")
                    pub_day = tweet['lifetime'][1].strftime("%Y%m%d")
                    pub_month = tweet['lifetime'][1].strftime("%Y%m")
                    pub_year = tweet['lifetime'][1].strftime("%Y")
                    logger.info('Limit lifetime, ID {} assign to date {}'.format(tweet_id, pub_date),
                                print_log=True)
            else:
                logger.info('Expired lifetime, ID {}'.format(tweet_id), print_log=True)

            gender = (str(tweet['gender']).replace("[", "")).replace("]", "") if tweet['gender'] else None
            max_robot_list = len(robot_list) - 1
            robot_mention = "@{}".format(robot_list[random.randint(0, max_robot_list)])
            twit = str(tweet['clean_tweet']).replace("{{ACCOUNT}}", robot_mention, 1)
            twit = twit.replace("{{ACCOUNT}}", "")
            if len(twit) > 280:
                twit = twit.replace(robot_mention, "")
            else:
                conn = self.pool_isr_twitter.connection()
                cursor = conn.cursor(MySQLdb.cursors.DictCursor)
                ta = TwitterAccount(conn=conn, cursor=cursor)
                robot = ta.get_twitter_account(robot_ids=soldier['robot_id'])
                if robot:
                    screen_name = robot[0]["screen_name"]
                    if re.search("\@{}".format(screen_name), str(twit)) and not re.search("^RT\s?", str(twit)):
                        twit = re.sub("\@{}\s?".format(screen_name), "", str(twit))

            file_soldier.write("Tweet ID {} | {}\n".format(tweet_id, twit))

            city = ""
            province = ""
            country = ""
            if tweet['location'][0] is not None:
                if "city" in tweet['location'][0]:
                    city = (str(tweet['location'][0]['city']).replace("[", "")).replace("]", "")
                if "province" in tweet['location'][0]:
                    province = (str(tweet['location'][0]['province']).replace("[", "")).replace("]", "")
                if "country" in tweet['location'][0]:
                    country = (str(tweet['location'][0]['country']).replace("[", "")).replace("]", "")

            age = (str(tweet['age']).replace("[", "")).replace("]", "") if tweet['age'] else None
            religion = (str(tweet['religion']).replace("[", "")).replace("]", "") if tweet['religion'] else None

            toggle_similiar = True

            if self.config.has_option("toggle_function", "similiar_tweet"):
                toggle_similiar = self.config.getboolean("toggle_function", "similiar_tweet")

            if toggle_similiar:
                if tweets_text:
                    for tw in tweets_text:
                        m = SequenceMatcher(None, twit.lower(), tw.lower())
                        ratio = m.ratio()
                        if ratio <= 0.65:
                            similiar_status = False
                        else:
                            similiar_status = True
                            logger.info(
                                "Tweet {} similiar with {} rasio : {}".format(twit, tw.encode('utf-8'),
                                                                              ratio),
                                True)
                            break

                if not similiar_status:
                    start = 0
                    rows = 100
                    while True:
                        logger.info('Start {}'.format(start), True)

                        solr_lib.set_query('{!join from=twitter_account_id  to=user_id fromIndex=robot}robot_id:' + str(soldier['robot_id']))
                        resp = solr_lib.get_documents('pub_day desc', start=start, rows=rows, fields='text')
                        solr_lib.reset_query()

                        start += rows

                        if not resp.docs:
                            break
                        else:
                            for sac in resp.docs:
                                sac['text'] = re.sub("(^RT\s?[^\:]*\:\s?)(.*)", "\g<2>", str(sac['text']))
                                tweets_text.append(sac['text'])
                                m = SequenceMatcher(None, twit.lower(), sac['text'].lower())
                                ratio = m.ratio()
                                if ratio <= 0.65:
                                    similiar_status = False
                                else:
                                    similiar_status = True
                                    logger.info("Tweet {} similiar with {} rasio : {}".format(
                                        twit, sac['text'].encode('utf-8'), ratio), True)
                                    break

                tweets_text.append(twit)

            try:
                if not similiar_status:
                    sac_id = soldier_autopost_catalog.insert(
                        tweet_id=tweet_id, age=age, gender=gender, religion=religion, city=city,
                        province=province, country=country, tweet=twit, robot_id=soldier['robot_id'],
                        pub_date=pub_date, pub_day=pub_day, pub_month=pub_month, pub_year=pub_year,
                        sentiment=sentiment, emotions=emotions, keywords=keywords, _class=_class,
                        _type=_type, campaign_id=campaign_id)

                    if 'image_name' in tweet and tweet['image_name']:
                        sm_model.save_smp({
                            'smp_id': sac_id,
                            'soldier_id': soldier['soldier_id'],
                            'smp_text': twit,
                            'smp_picture': tweet['image_name'],
                            'smp_pub_date': pub_date
                        })

                    logger.info(tweet['keywords'])
                    if sac_id and 'keywords' in tweet:
                        list_keyword = tweet['keywords']
                        for keyword in list_keyword:
                            emotions = ",".join(list_keyword[keyword]['emotions'])
                            sentiment = list_keyword[keyword]['sentiment']
                            if sentiment == 'negative':
                                sentiment = '-1'
                            elif sentiment == 'positive':
                                sentiment = '1'
                            else:
                                sentiment = '0'
                            if emotions == '':
                                emotions = None
                            data_skc = {
                                skc_model.FIELD_SAC_ID: sac_id,
                                skc_model.FIELD_SENTIMENT: sentiment,
                                skc_model.FIELD_EMOTIONS: emotions,
                                skc_model.FIELD_KEYWORD: keyword
                            }
                            skc_model.save_by_data(data_skc)

                    if not sac_id in list_id:
                        list_id.append(sac_id)

                    conn.commit()

                    data_update = {"status": 1}
                    mongo_model.update(tweet_id, data_update, db_name, table_name)

                    logger.info('Done TID {}'.format(tweet_id), print_log=True)
            except Exception, e:
                conn.rollback()
                logger.error(e, sentry=self.sentry)

        file_soldier.write("======================= End ==========================\n")
        return list_id

    def worker_extract_autopost(self, ip_address=None):
        ip_address = ip_address if ip_address else self.get_ip_address()
        worker = init_worker_beanstalk(self.config, "robot_extract_autopost_{}".format(ip_address))
        utilobj = self.get_utils_object()
        while True:
            job = worker.getJob(30)
            if job is None:
                logger.info("Job not found, Sleep {}s".format(self.timeout), True)
                time.sleep(int(self.timeout))
            else:
                print "Found Job"
                process_redis_name = None
                try:
                    start_time = time.time()
                    data = json.loads(job.body)
                    process_redis_name = "robot_extract:robot_id:{}".format(data["robot_id"])
                    self.get_extract_tweet(robot_id=data["robot_id"], types="all", single=True, utilobj=utilobj)
                    exec_time = time.time() - start_time
                    logger.info("Success extract robot {}. Time {}".format(data["robot_id"], exec_time))
                    worker.deleteJob(job)
                except Exception, e:
                    worker.buriedJob(job)
                    logger.error(str(e), sentry=self.sentry)
                finally:
                    if process_redis_name:
                        self.redis_pointer.delete(process_redis_name)

    def pusher_extract_autopost(self):
        conn = self.pool_content_management.connection()
        cursor = conn.cursor(MySQLdb.cursors.DictCursor)
        ip_address = self.get_ip_address()
        pusher = init_pusher_beanstalk(self.config, "robot_extract_autopost_{}".format(ip_address))
        try:
            robot_model = Robot(conn, cursor)
            robots = robot_model.get_robot()
            total = 0
            for robot in robots:
                process_redis_name = "soldier_extract:robot_id:{}".format(robot["robot_id"])
                if self.redis_pointer.get(process_redis_name):
                    logger.info("Robot {} in process".format(robot["robot_id"]))
                else:
                    total += 1
                    job = {"robot_id": robot["robot_id"]}
                    pusher.setJob(json.dumps(job))
                    self.redis_pointer.set(process_redis_name, "1")

            logger.info("Total robots {}, Success push {} robots".format(len(robots), total), True)
        except Exception:
            raise
        finally:
            conn.close()
            cursor.close()
            pusher.close()

    def twitter_catalog_selection(self, hour, schedule, robot_id, time_lookups, ip_address):
        conn = self.pool_content_management.connection()
        cursor = conn.cursor(MySQLdb.cursors.DictCursor)

        ar_model = AutopostRobot(conn, cursor)
        ac_model = AutopostCatalog(conn, cursor)
        rs_model = RobotSchedule(conn, cursor)
        robot_model = Robot(conn, cursor)
        try:
            acs = ac_model.get_ac(robot_id=robot_id, status="0", s_date=time_lookups[1], e_date=time_lookups[0]).fetchall
            try:
                max_tweet_per_hour = int(self.config.get('robot', 'max_retweet_per_hour'))
            except:
                max_tweet_per_hour = 2

            robot = robot_model.get_robot(robot_id=robot_id).fetchone
            twitter_account_id = robot["twitter_account_id"]

            current_hour = datetime.datetime.now().strftime("%Y%m%d%H")
            counter = self.ssdb.hget(
                name="counter_robot_autopost_hourly",
                key="{}_{}".format(current_hour, robot_id)
            )
            if not counter:
                counter = 0
            logger.info("Counter From Robot ID {} is {}".format(robot_id, counter), True)

            if int(counter) <= max_tweet_per_hour:
                if acs:
                    for num_tweet in range(int(schedule[rs_model.FIELD_COUNT])):
                        if len(acs) == num_tweet:
                            break
                        ac = acs[num_tweet]
                        ac_model.update_ac({ac_model.FIELD_STATUS: 1}, ac_id=ac[ac_model.FIELD_ID])
                        ar_model.save_ar({
                            ar_model.FIELD_AUTOPOST_CATALOG_ID: ac[ac_model.FIELD_ID],
                            ar_model.FIELD_DEVICE: "Intelligence Socio Robot",
                            ar_model.FIELD_ROBOT_ID: robot_id
                        })
                        rs_model.decrease_counter(robot_id, hour, schedule[rs_model.FIELD_TYPE], count=1)

                        minute = datetime.datetime.now().minute
                        send_time = random.randint(minute, 59) if minute < 59 else minute
                        delay = (send_time - minute) * 60

                        pusher = init_pusher_beanstalk(self.config, "robot_autopost_tweet_{}".format(ip_address))
                        job = "{}|{}".format(ac[ac_model.FIELD_ID], twitter_account_id)
                        pusher.setJob(job, delay=delay)

                        if counter:
                            counter = int(counter) + 1
                        else:
                            counter = 1

                        self.ssdb.hset(
                            name="counter_robot_autopost_hourly",
                            key="{}_{}".format(current_hour, robot_id),
                            value=str(counter)
                        )
                        logger.info("Catalog ID {}, Robot {}".format(ac[ac_model.FIELD_ID], robot_id), True)
                        conn.commit()

                        if counter >= max_tweet_per_hour:
                            break
        except Exception, e:
            conn.rollback()
            logger.error("Twitter Robot {}, {}".format(robot_id, str(e)))
        finally:
            cursor.close()
            conn.close()

    def worker_push_autopost(self, ip_address=None):
        ip_address = ip_address if ip_address else self.get_ip_address()
        worker = init_worker_beanstalk(self.config, "robot_push_autopost_{}".format(ip_address))
        while True:
            job = worker.getJob(30)
            if job is None:
                logger.info("Job not found, Sleep {}s".format(self.timeout), True)
                time.sleep(int(self.timeout))
            else:
                print "Found Job"
                process_redis_name = None
                try:
                    start_time = time.time()
                    data = json.loads(job.body)
                    process_redis_name = "robot_push_autopost:robot_id:{}".format(data["robot_id"])
                    self.push_soldier_tweet(robot_id=data["robot_id"], ip_address=ip_address)
                    exec_time = time.time() - start_time
                    logger.info("Success push robot {} autopost. Time {}".format(data["robot_id"], exec_time))
                    worker.deleteJob(job)
                except Exception, e:
                    worker.buriedJob(job)
                    logger.error(str(e), sentry=self.sentry)
                finally:
                    if process_redis_name:
                        self.redis_pointer.delete(process_redis_name)

    def push_soldier_tweet(self, robot_id=None, ip_address=None):
        conn = self.pool_content_management.connection()
        cursor = conn.cursor(MySQLdb.cursors.DictCursor)
        rs_model = RobotSchedule(conn, cursor)

        now = datetime.datetime.now()
        hour = now.hour
        today = now.strftime("%Y%m%d")
        yesterday = (now - timedelta(days=1)).strftime("%Y%m%d")
        time_lookups = [today, yesterday]
        try:
            ip_address = ip_address if ip_address else self.get_ip_address()
            schedules = rs_model.get_robot_schedule(robot_id=robot_id, hour=hour).data
            if not schedules:
                logger.info("Robot {} not have schedule on hour {}".format(robot_id, hour), True)

            for schedule in schedules:
                self.twitter_catalog_selection(hour, schedule, robot_id, time_lookups, ip_address)
        except Exception, e:
            logger.error(e, sentry=self.sentry)
        finally:
            cursor.close()
            conn.close()

    def pusher_push_autopost(self):
        conn = self.pool_content_management.connection()
        cursor = conn.cursor(MySQLdb.cursors.DictCursor)

        robot_model = Robot(conn, cursor)
        ip_address = self.get_ip_address()
        pusher = init_pusher_beanstalk(self.config, "robot_push_autopost_{}".format(ip_address))
        try:
            robots = robot_model.get_robot().fetchall
            total = 0
            for robot in robots:
                process_redis_name = "robot_push_autopost:robot_id:{}".format(robot["robot_id"])
                if self.redis_pointer.get(process_redis_name):
                    logger.info("Robot {} in process".format(robot["robot_id"]))
                else:
                    total += 1
                    job = {"robot_id": robot["robot_id"]}
                    pusher.setJob(json.dumps(job))
                    self.redis_pointer.set(process_redis_name, "1")

            logger.info("Total robots {}, Success push {} robots".format(len(robots), total), True)
        except Exception:
            raise
        finally:
            conn.close()
            cursor.close()
            pusher.close()

