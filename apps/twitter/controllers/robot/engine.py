import random
import MySQLdb
import datetime
import json
import operator
import re
import time

from HTMLParser import HTMLParser
from core import logger
from core.controller import Controller
from apps.twitter.models.robot import Robot
from apps.twitter.models.autopost_catalog import AutopostCatalog
from apps.twitter.models.autopost_robot import AutopostRobot
from apps.twitter.models.applications import Applications
from apps.twitter.models.main.template_schedule import TemplateSchedule
from apps.twitter.models.main.robot_schedule import RobotSchedule
from helper.general import init_worker_beanstalk
from eblib.twitter.twitter import MyTwitter


class Engine(Controller):
    def __init__(self):
        Controller.__init__(self)
        self.html_parser = HTMLParser()

    def assign_robot_schedule(self, robot_id=None):
        try:
            robots, template = self.__get_robot_and_template(robot_id=robot_id)
            for robot in robots:
                self.__generate_robot_schedule(robot, template)
        except Exception, e:
            self.error_logging(e)

    def __get_robot_and_template(self, robot_id=None):
        conn = self.pool_content_management.connection()
        cursor = conn.cursor(MySQLdb.cursors.DictCursor)

        ts_model = TemplateSchedule(conn, cursor)
        robot_model = Robot(conn, cursor)

        hari = ["senin", "selasa", "rabu", "kamis", "jumat", "sabtu", "minggu"]
        week_day = datetime.datetime.now().weekday()
        try:
            template = ts_model.get_template_schedule(desc=hari[week_day]).fetchone
            robots = robot_model.get_robot(robot_id=robot_id).fetchall
            conn.commit()
            return robots, template
        except Exception:
            raise
        finally:
            cursor.close()
            conn.close()

    def __generate_robot_schedule(self, robot, template):
        conn = self.pool_content_management.connection()
        cursor = conn.cursor(MySQLdb.cursors.DictCursor)

        s_model = RobotSchedule(conn, cursor)
        try:
            s_model.delete(robot_id=robot["robot_id"])
            limit_per_day = self.get_tweet_per_day(robot)
            schedule = self.calculate_robot_schedule(
                kuota={"private": 100},
                template=template["ts_value"],
                limit=limit_per_day
            )
            for hour, val in enumerate(schedule):
                for _type, value in val.iteritems():
                    if value:
                        data = {
                            s_model.FIELD_ROBOT_ID: robot["robot_id"],
                            s_model.FIELD_HOUR: hour,
                            s_model.FIELD_TYPE: _type,
                            s_model.FIELD_COUNT: int(value),
                            s_model.FIELD_STATUS: "0"
                        }
                        s_model.save(data)
                        logger.info("Robot id {} set on {} with {}".format(robot["robot_id"], str(hour), _type),
                                    True)
            conn.commit()
        except Exception, e:
            self.error_logging(e)
        finally:
            cursor.close()
            conn.close()

    def calculate_robot_schedule(self, kuota, template, limit):
        template_list = json.loads(template)
        sorted_tmpl = sorted(range(len(template_list)), key=lambda k: template[k], reverse=True)
        kuota_soldier = dict()
        for t, k in kuota.iteritems():
            kuota_soldier.update({t: round(limit * (float(k) / 100))})

        kuota_type = kuota.keys()  # ["private"]
        schedule_per_hour = self.calculate_template_schedule(template, limit)

        _break = False
        schedule = list()
        for hour, q_per_hour in schedule_per_hour:
            all_type = [0]
            data = {"private": 0}

            if max(kuota_soldier.iteritems(), key=operator.itemgetter(1))[1] is 0:
                _break = True
            for t in range(int(q_per_hour)):
                if _break:
                    break

                pick = random.randint(0, 0)
                while kuota_soldier[kuota_type[pick]] == 0:
                    if pick in all_type:
                        all_type.remove(pick)
                    pick = random.randint(0, 0)
                    if len(all_type) == 0:
                        break

                data[kuota_type[pick]] += 1

                kuota_soldier[kuota_type[pick]] -= 1

            schedule.append(data)

        for t, value in kuota_soldier.iteritems():
            i = 0
            for s in range(int(value)):
                hour = sorted_tmpl[i]
                schedule[hour][t] += 1
                i += 1

        return schedule

    @staticmethod
    def calculate_template_schedule(template, limit):
        template = json.loads(template)
        coef = sum(template)

        schedule = list()
        i = 0
        for hour_limit in template:
            percentage = float(float(hour_limit) / float(coef))
            q_per_hour = round(limit * percentage)
            schedule.append((i, q_per_hour))
            i += 1
        return schedule

    @staticmethod
    def get_tweet_per_day(robot):
        limit_per_day = 10
        if robot["followers_count"] in range(100, 199):
            limit_per_day = 20
        elif robot["followers_count"] in range(200, 299):
            limit_per_day = 30
        elif robot["followers_count"] in range(300, 399):
            limit_per_day = 40
        elif robot["followers_count"] >= 400:
            limit_per_day = 50

        limit_per_day += random.randint(0, 9)

        return limit_per_day

    def send_twitter_tweet(self, net_intf=None):
        list_net = None
        if net_intf:
            list_net = [net_intf]
        ip_address = self.get_ip_address(list_net)
        worker = init_worker_beanstalk(self.config, "robot_autopost_tweet_{}".format(ip_address))
        while True:
            job = worker.getJob(int(self.config.get("beanstalk", "timeout")))
            if job is None:
                logger.info("Worker sleep 10 seconds", True)
                time.sleep(10)
            else:
                conn = self.pool_content_management.connection()
                cursor = conn.cursor(MySQLdb.cursors.DictCursor)
                robot_model = Robot(conn, cursor)
                ac_model = AutopostCatalog(conn, cursor)
                ar_model = AutopostRobot(conn, cursor)
                app_model = Applications(conn, cursor)
                ac_id = None
                try:
                    ac_id, twitter_account_id = str(job.body).split("|")
                    robot = robot_model.get_robot(twitter_account_id=twitter_account_id).fetchone
                    if not robot:
                        raise Exception("Robot not found")

                    catalog = ac_model.get_ac(catalog_id=ac_id).fetchone
                    if not catalog:
                        raise Exception("Catalog not found")

                    robot_id = robot["robot_id"]
                    tweet = catalog["tweet"]
                    text = self.html_parser.unescape(tweet.decode('string_escape'))
                    text = self.text_management(text=text)

                    # Get apps
                    apps = app_model.get_app_token(twitter_account_id=twitter_account_id).fetchone
                    if apps:
                        twitter_api = MyTwitter(
                            consumer_key=apps["consumer_key"],
                            consumer_secret=apps["consumer_secret"],
                            access_token=apps["access_token"],
                            access_token_secret=apps["access_token_secret"]
                        )
                        status, result, code, message = twitter_api.twitter_tweet(text=text)
                        if status:
                            tweet_id = result["id"]
                            today = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                            # update autopost
                            data_ar = {ar_model.FIELD_TWEET_ID: tweet_id, ar_model.FIELD_STATUS: "success"}
                            ar_model.update_ar(data=data_ar, ac_id=ac_id)
                            # update catalog
                            data_ac = {ac_model.FIELD_TWEET_ID: tweet_id, ac_model.FIELD_TWEETED_AT: today}
                            ac_model.update_ac(data=data_ac, ac_id=ac_id)
                            # update last tweet
                            data_ur = {robot_model.FIELD_LAST_TWEET: today}
                            robot_model.update_robot(data=data_ur, robot_id=robot_id)
                            logger.info("Robot id {} post tweet success".format(robot_id), True)
                        else:
                            data_ar = {ar_model.FIELD_STATUS: "failed", ar_model.FIELD_MESSAGE: message}
                            ar_model.update_ar(data=data_ar, ac_id=ac_id)
                            logger.info("Robot id {} post tweet failed. {}".format(robot_id, message), True)
                            if code:
                                if code == 64:
                                    data_update = {
                                        robot_model.FIELD_BANNED: 1,
                                        robot_model.FIELD_STATUS: 3
                                    }
                                    robot_model.update_robot(data_update, robot_id)
                                elif code == 32:
                                    data_update = {robot_model.FIELD_STATUS: 5}
                                    robot_model.update_robot(data_update, robot_id)
                                elif code == 326:
                                    data_update = {robot_model.FIELD_STATUS: 4}
                                    robot_model.update_robot(data_update, robot_id)
                    else:
                        raise Exception("App not found")

                    conn.commit()
                    worker.deleteJob(job)
                except Exception, e:
                    conn.rollback()
                    if ac_id:
                        data_ar = {
                            ar_model.FIELD_STATUS: "failed",
                            ar_model.FIELD_MESSAGE: str(e)
                        }
                        ar_model.update_ar(data=data_ar, ac_id=ac_id)
                    worker.buriedJob(job)
                    logger.error(str(e), sentry=self.sentry)
                finally:
                    conn.close()
                    cursor.close()
