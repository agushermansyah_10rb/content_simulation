import json
import os
import urlparse
import nsq
from apps.twitter.controllers.robot import AutopostRobot
from core import logger
from helper.general import tweet_date

__author__ = 'root'

sentiments = {"negative": -1, "neutral": 0, "positive": 1}
lang_mapping = {"in": "id", "iw": "he", "ji": "yi"}


class DatabankParser(AutopostRobot):
    def __init__(self):
        AutopostRobot.__init__(self)

    @staticmethod
    def get_user_mentions(tweet):
        mentions = []
        if tweet["entities"]["user_mentions"]:
            user_mentions = tweet["entities"]["user_mentions"]
            for u in user_mentions:
                mentions.append(u["id_str"])
        return mentions

    def databank_parser_handler(self, message):
        tweet_str = message.body
        try:
            tweet = json.loads(tweet_str)
        except ValueError:
            tweet_str = dict(urlparse.parse_qsl(tweet_str))["message"]
            tweet = json.loads(tweet_str)

        tweet["mentions"] = self.get_user_mentions(tweet)
        tweet["created_at"] = tweet_date(tweet["created_at"], int(self.config.get("etc", "timezone")))
        try:
            pid = os.getpid()
            if self.insert_databank_data(tweet):
                logger.info("Tweet id {} inserted to databank, PID {}".format(tweet["id"], pid), print_log=False)
            return True
        except Exception, e:
            logger.error("Tweet id {}, {}".format(tweet["id"], e), sentry=self.sentry, print_log=False)
            return False

    def databank_parser(self):
        pid = os.getpid()
        logger.info("Databank parser Start with PID {}".format(pid), True)

        nsqd_tcp_addresses = "{}:{}".format(self.config.get("nsq", "host"), self.config.get("nsq", "tcp_port"))
        nsq.Reader(message_handler=self.databank_parser_handler, topic=self.config.get("nsq", "databank-topic"),
                   channel="parser", lookupd_poll_interval=15, nsqd_tcp_addresses=[nsqd_tcp_addresses],
                   lookupd_request_timeout=int(self.config.get('nsq', 'lookupd_request_timeout')))
        nsq.run()
