from apps.twitter.models.mongo_db import MongoDb
from core import logger
from core.controller import Controller
from helper.general import to_dict, resize_image, get_age_from_mapping
# from nlp_modules.tw_lang_detector.eb_langdetect import SimpleLangDetector


class AutopostRobot(Controller):
    def __init__(self):
        from content_management.utils import Utils
        Controller.__init__(self)
        self.mongo_model = MongoDb()
        self.lang_detector = SimpleLangDetector()
        self.utilObj = Utils(dbname="mongo_db", psychography_catalog=self.get_psychography_catalog(),
                             configObj=self.config)
        self.whitelisted_lang = self.config.get("etc", "whitelisted_lang").replace(" ", "").split(",")
        self.robot_ids = self.get_robot_list(order_by="robot_id")
        self.last_robot = self.get_last_robot(_type='single')

    def insert_databank_data(self, data, from_streaming=True):
        user = data["user"]
        robot_count = self.get_last_robot(_type='count')
        if len(self.robot_ids) != robot_count:
            diff = robot_count - len(self.robot_ids)
            logger.info("New {} robot detected".format(diff), True)
            self.robot_ids = self.get_robot_list()
        else:
            print "not detect", robot_count

        robot_ids = self.robot_ids

        retweeted = False

        affected_user = [user["id_str"]]

        # Get RT
        if "rt_user_id" in data:
            rt_user_id = data["rt_user_id"]
            if rt_user_id:
                retweeted = True
                affected_user.append(rt_user_id)

        if "retweeted_status" in data:
            retweeted = True

        # Get Mentions
        if "mentions" in data:
            mentions = data["mentions"]
            affected_user.extend(mentions)

        # Get Full Text Tweet
        if "extended_tweet" in data:
            data["text"] = data["extended_tweet"]["full_text"]

        c_date = data["created_at"]

        affected_status = False
        for au in affected_user:
            if au in robot_ids:
                affected_status = True
                break

        if str(data["lang"]).lower() in ["id", "in", "my", "ms", "en"]:
            lang = self.lang_detector.detect_language(data["text"]).lower()
            if lang in self.whitelisted_lang and not affected_status and not retweeted:
                if from_streaming:
                    add_user = self.mongo_model.find(self.config.get("mongo_db", "accountbank_db_name"), "twitter_user",
                                                     {"_id": str(user["id"])}, type='one')
                else:
                    add_user = user

                user.update({
                    "age": get_age_from_mapping(add_user["age"]) if add_user and "age" in add_user else None,
                    "gender": add_user["gender"] if add_user and "gender" in add_user else None,
                    "country": add_user["country"] if add_user and "country" in add_user else None,
                    "religion": add_user["religion"] if add_user and "religion" in add_user else None,
                    "province": add_user["province"] if add_user and "province" in add_user else None,
                    "city": add_user["city"] if add_user and "city" in add_user else None,
                    "subdistrict": add_user[
                        "subdistrict"] if add_user and "subdistrict" in add_user else None,
                    "village": add_user["village"] if add_user and "village" in add_user else None
                })

                location = {"country": None, "province": None, "city": None}
                full_location = {"country": None, "province": None, "city": None}

                if 'country' in user and user['country']:
                    location['country'] = user['country']
                    full_location['country'] = user['country']
                if 'province' in user and user['province']:
                    full_location['province'] = user['province']
                    data_split = user['province'].split("|")
                    if data_split[1]:
                        location['province'] = data_split[1]
                if 'city' in user and user['city']:
                    full_location['city'] = user['city']
                    data_split = user['city'].split("|")
                    if data_split[2]:
                        location['city'] = data_split[2]

                demography = {"religion": user['religion'], "location": location}
                psychography = self.utilObj.get_single_tweet_psychography(demography, data['text'], c_date,
                                                                          lang=str(data["lang"]))
                if psychography:
                    data_mongo = {"_id": data["id_str"], "account_id": user["id"], "tweet": data["text"],
                                  "tweet_date": c_date, "created_at": c_date,
                                  "age": user["age"] if "age" in user else None,
                                  "gender": user["gender"] if "gender" in user else None,
                                  "religion": user["religion"] if "religion" in user else None,
                                  "location": location, "full_location": full_location}
                    data_mongo.update(psychography)
                    check_exist = self.mongo_model.find(self.config.get("mongo_db", "databank_db_name"),
                                                        "soldier_post", {"_id": str(data["id_str"])}, type='one')
                    if check_exist is None:
                        self.mongo_model.insert(data_mongo, self.config.get("mongo_db", "databank_db_name"),
                                                "soldier_post")
                    else:
                        logger.info("Data ID {} Already Exist".format(data["id_str"]))

                    return True
                else:
                    with open('trace_tweet.txt', 'a') as _file:
                        _file.write("%s || \n" % data['text'].encode('utf-8'))

                    logger.warning("Unknown Classification", print_log=False)

        return False
