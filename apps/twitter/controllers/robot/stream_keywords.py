import threading
import MySQLdb
import json
import time
import math
from random import randint
import tweepy

from apps.twitter.models.robot import Robot
from eblib.nsq import Producer

from apps.twitter.models.main.additional_robot_psychography import AdditionalRobotPsychography
from apps.twitter.models.main.personality_psychography import PersonalityPsychography
from core import logger
from core.controller import Controller
from helper.general import to_dict, init_redis


class StreamKeywords(tweepy.StreamListener, Controller, threading.Thread):
    def __init__(self, _id):
        threading.Thread.__init__(self)
        Controller.__init__(self)
        tweepy.StreamListener.__init__(self)
        self._id = _id
        self.worker = None
        self.stream = None
        self.stream_type = None
        self.signal = 0
        self.limit_track = 400.0
        self.track = None
        self.follow = None
        self.keywords = None
        self.full_keywords = None
        self.consumer_key = None
        self.consumer_secret = None
        self.access_token = None
        self.access_token_secret = None
        self.is_running = True
        self.producer = None
        self.idle_time = int(self.config.get("streaming", "idle_time_restart"))

    def set_params(self, keywords=None, access_token=None, access_token_secret=None, consumer_key=None,
                   consumer_secret=None, limit_track=None, full_keywords=None, producer=None, follow=None,
                   idle_time=None):
        self.keywords = keywords
        self.access_token = access_token
        self.access_token_secret = access_token_secret
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.limit_track = limit_track
        self.full_keywords = full_keywords
        self.follow = follow
        self.producer = producer
        if idle_time:
            self.idle_time = idle_time

    def on_status(self, status):
        self.signal = 0
        tweet = to_dict(status)
        self.producer.put_message(json.dumps(tweet))

        if randint(0, 5) == 1:
            logger.info("Insert tweet id {} from stream {}".format(tweet['id'], self._id), True, config_load=self.config)

    def on_error(self, status_code):
        if int(status_code) == 420:
            logger.info("Error status code {}, sleeping for 10 seconds".format(status_code), True)
            time.sleep(10)
        else:
            logger.info("Error status code {}".format(status_code), True)

    def on_timeout(self):
        time.sleep(10)

    def init_worker(self):
        try:
            if len(self.keywords) < 1 and len(self.follow) < 1:
                raise Exception("No keywords or follows assigned")

            if not self.access_token:
                raise Exception("Access token required")

            auth = tweepy.auth.OAuthHandler(self.consumer_key, self.consumer_secret)
            auth.set_access_token(self.access_token, self.access_token_secret)
            self.stream = tweepy.Stream(auth=auth, listener=self, tweet_mode='extended')

            self.stream.filter(track=self.keywords, follow=self.follow, async=True)
        except Exception, e:
            self.error_logging(e, config_load=self.config)

    def release_stream(self):
        if self.stream:
            self.stream.disconnect()
            self.stream = None

    def run(self):
        try:
            self.init_worker()
            logger.info("Starting streaming id {}".format(self._id), True, config_load=self.config)

            while self.is_running:
                if self.signal >= self.idle_time:
                    self.release_stream()
                    self.init_worker()
                    self.signal = 0
                    logger.info("Restart Streaming id {}".format(self._id), True, config_load=self.config)
                elif not self.stream.running:
                    self.init_worker()
                    self.signal = 0
                    logger.info("Restart Streaming id {}".format(self._id), True, config_load=self.config)
                self.signal += 1
                time.sleep(1)
        except Exception, e:
            self.error_logging(e, config_load=self.config)

        self.release_stream()


class StreamListener(tweepy.StreamListener, Controller):
    def __init__(self, worker_id):
        Controller.__init__(self)
        tweepy.StreamListener.__init__(self)
        self._id = worker_id
        self.worker = None
        self.stream = None
        self.stream_type = None
        self.signal = 0
        self.limit_track = 400.0
        self.track = None
        self.follow = None
        self.keywords = None
        self.full_keywords = None
        self.consumer_key = None
        self.consumer_secret = None
        self.access_token = None
        self.access_token_secret = None
        self.is_running = True
        self.producer = None
        self.idle_time = int(self.config.get("streaming", "idle_time_restart"))

    def set_params(self, keywords=None, access_token=None, access_token_secret=None, consumer_key=None,
                   consumer_secret=None, limit_track=None, full_keywords=None, producer=None, follow=None,
                   idle_time=None):
        self.keywords = keywords
        self.access_token = access_token
        self.access_token_secret = access_token_secret
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.limit_track = limit_track
        self.full_keywords = full_keywords
        self.follow = follow
        self.producer = producer
        if idle_time:
            self.idle_time = idle_time

    def on_status(self, status):
        self.signal = 0
        tweet = to_dict(status)
        self.producer.put_message(json.dumps(tweet))

        logger.info("Insert tweet id {}".format(tweet['id']), True)

    def on_error(self, status_code):
        if int(status_code) == 420:
            logger.info("Error status code {}, sleeping for 10 seconds".format(status_code), True)
            time.sleep(10)
        else:
            logger.info("Error status code {}".format(status_code), True)

    def on_timeout(self):
        time.sleep(10)

    def init_worker(self):
        try:
            if len(self.keywords) < 1 and len(self.follow) < 1:
                raise Exception("No keywords or follows assigned")

            if not self.access_token:
                raise Exception("Access token required")

            auth = tweepy.auth.OAuthHandler(self.consumer_key, self.consumer_secret)
            auth.set_access_token(self.access_token, self.access_token_secret)
            self.stream = tweepy.Stream(auth=auth, listener=self, tweet_mode='extended')

            self.stream.filter(track=self.keywords, follow=self.follow, async=True)
        except Exception, e:
            self.error_logging(e)

    def release_stream(self):
        if self.stream:
            self.stream.disconnect()
            self.stream = None

    def run(self):
        try:
            self.init_worker()
            logger.info("Starting streaming id {}".format(self._id), True)

            while self.is_running:
                if self.signal >= self.idle_time:
                    self.release_stream()
                    self.init_worker()
                    self.signal = 0
                    logger.info("Restart Streaming id {}".format(self._id), True)
                elif not self.stream.running:
                    self.init_worker()
                    self.signal = 0
                    logger.info("Restart Streaming id {}".format(self._id), True)
                self.signal += 1
                time.sleep(1)
        except Exception, e:
            self.error_logging(e)

        self.release_stream()


class DatabankStream(Controller):
    def __init__(self):
        Controller.__init__(self)
        self.limit_track = float(self.config.get("streaming", "limit_track"))
        self.limit_follow = float(self.config.get("streaming", "limit_follow"))

        self.twitter_regular_consumer_key = self.config.get(
            "streaming", "twitter_regular_consumer_key")
        self.twitter_regular_consumer_secret = self.config.get(
            "streaming", "twitter_regular_consumer_secret")
        self.twitter_regular_access_token = self.config.get(
            "streaming", "twitter_regular_access_token")
        self.twitter_regular_access_token_secret = self.config.get(
            "streaming", "twitter_regular_access_token_secret")

        self.streams = dict()
        self.redis_pointer = init_redis()
        self.robot_token_now = []
        self.producer_databank = Producer(
            self.config.get("nsq", "host"), self.config.get("nsq", "http_port"),
            self.config.get("nsq", "databank-topic")
        )

    def stop_streams(self):
        for _id, stream in self.streams.iteritems():
            stream.is_running = False
            stream.join()
            logger.info("Stream with id {} stopped".format(_id))
            self.redis_pointer.hdel("tat:used", _id)

        self.streams = dict()

    def start(self, access_token=False):
        keywords = self.get_keyword_filter()
        self.init_stream(keywords, access_token)
        try:
            check_time = 60
            while True:
                time.sleep(check_time)
                check_keywords = self.get_keyword_filter()
                if set(keywords) != set(check_keywords):
                    keywords = check_keywords
                    self.stop_streams()
                    self.init_stream(check_keywords, access_token)

                    logger.info("Restart Streaming, Diff keywords detected", config_load=self.config)
                else:
                    logger.info("Diff keywords not found", config_load=self.config)
        except KeyboardInterrupt:
            logger.info("Shutting down streams...", True, config_load=self.config)
        except Exception, e:
            self.error_logging(e, config_load=self.config)

        self.stop_streams()

    def init_stream(self, keywords, access_token=False):
        part = int(math.ceil(len(keywords) / self.limit_track))
        split_keywords = self.split_list(keywords, wanted_parts=part)

        logger.info("{} there is {} keyword(s) and part {}".format(split_keywords, len(keywords), part), True,
                    config_load=self.config)
        for keyword in split_keywords:
            if access_token:
                access_token = self.twitter_regular_access_token
                access_token_secret = self.twitter_regular_access_token_secret
                robot_id = randint(1, 10)
            else:
                access_token, access_token_secret, robot_id = self.get_access_token(
                    self.twitter_regular_consumer_key,
                    self.twitter_regular_consumer_secret
                )
            sk = StreamKeywords(robot_id)
            sk.set_params(
                keywords=keyword, consumer_key=self.twitter_regular_consumer_key,
                consumer_secret=self.twitter_regular_consumer_secret,
                access_token=access_token, access_token_secret=access_token_secret,
                limit_track=self.limit_track, full_keywords=keywords,
                producer=self.producer_databank, idle_time=10
            )
            sk.start()
            self.streams[robot_id] = sk

    @staticmethod
    def join_list(list_a, list_b):
        if len(list_a) < len(list_b):
            diff_len = len(list_b) - len(list_a)
            for x in xrange(diff_len):
                list_a.append([])
        elif len(list_b) < len(list_a):
            diff_len = len(list_a) - len(list_b)
            for x in xrange(diff_len):
                list_b.append([])
        list_c = zip(list_a, list_b)

        return list_c

    def get_access_token(self, consumer_key, consumer_secret):
        access_token = None
        access_token_secret = None
        robot_id = None
        try:
            start = 0
            rows = 20
            robots = self.get_robots(start, rows)
            found = False
            while not found and robots:
                for robot in robots:
                    if not self.redis_pointer.hget("tat:used", robot["robot_id"]):
                        access_token, access_token_secret, message = self.auto_authorized_selenium(
                            consumer_key, consumer_secret, robot, update_status=False
                        )
                        if access_token:
                            self.redis_pointer.hset("tat:used", robot["robot_id"], 1)
                            self.robot_token_now.append(robot['robot_id'])
                            robot_id = robot["robot_id"]
                            found = True
                            break

                start += rows
                robots = self.get_robots(start, rows)

        except Exception, e:
            self.error_logging(e)
            raise

        return access_token, access_token_secret, robot_id

    def get_keyword_filter(self):
        conn = self.pool_isr_main.connection()
        cursor = conn.cursor(MySQLdb.cursors.DictCursor)

        arp_model = AdditionalRobotPsychography(conn, cursor)
        pp_model = PersonalityPsychography(conn, cursor)
        track = []
        try:
            column = "distinct(p_alias) as p_alias, p_desc"
            arp_data = arp_model.get_additional_psychography(column=column)
            for arp in arp_data:
                desc = arp['p_desc']
                if desc not in track:
                    track.append(desc)

                alias = arp['p_alias']
                if alias and alias != '':
                    keywords = arp['p_alias'].split(',')
                    for k in keywords:
                        if k not in track:
                            track.append(k)

            pp_data = pp_model.get_data_personality_psychography(column_custom=column, p_status=1).fetchall
            for pp in pp_data:
                desc = pp['p_desc']
                if desc not in track:
                    track.append(desc)

                alias = pp['p_alias']
                if alias and alias != '':
                    keywords = pp['p_alias'].split(',')
                    for k in keywords:
                        if k not in track:
                            track.append(k)
            conn.commit()
        except Exception:
            raise
        finally:
            cursor.close()
            conn.close()
        return track

    def get_robots(self, start=0, rows=20):
        conn = self.pool_isr_twitter.connection()
        cursor = conn.cursor(MySQLdb.cursors.DictCursor)

        robot_model = Robot(conn, cursor)
        try:
            robots = robot_model.get_robot(status=1, start=start, rows=rows, order_by="RAND()").fetchall
            conn.commit()
        except Exception:
            raise
        finally:
            cursor.close()
            conn.close()
        return robots

    @staticmethod
    def split_list(alist, wanted_parts=1):
        length = len(alist)
        return [alist[i * length // wanted_parts: (i + 1) * length // wanted_parts]
                for i in range(wanted_parts)]

