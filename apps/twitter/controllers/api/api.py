import MySQLdb
from core import logger
from core.controller import Controller
from apps.twitter.models.autopost_catalog import AutopostCatalog


class API(Controller):
    def __init__(self):
        Controller.__init__(self)

    def get_tweets(self):
        conn = self.pool_content_management.connection()
        cursor = conn.cursor(MySQLdb.cursors.DictCursor)
        result = dict()
        try:
            print "ok"
        except Exception, e:
            conn.rollback()
            logger.error(str(e), sentry=self.sentry)
        finally:
            conn.close()
            cursor.close()
            return result

