import pymongo
from core.model import Model
from core import config
from bson.son import SON


__author__ = 'Linuxz'


class MongoDb(Model):

    def __init__(self, conn=None, cursor=None, host=None, port=None):
        Model.__init__(self, conn, cursor)
        self.config = config.load()
        if host is None:
            host = self.config.get("mongo_db", "host")
        if port is None:
            port = self.config.get("mongo_db", "port")

        self.client = pymongo.MongoClient(
            "mongodb://{}:{}".format(host, port))
        self.db = None

    def find(self, db_name, table_name, filter_data=None, limit=None, type='many', skip=0):
        try:
            self.db = self.client[db_name]
            if limit:
                if (type == 'many'):
                    return self.db[table_name].find(filter_data).skip(int(skip)).limit(int(limit))
                else:
                    return self.db[table_name].find_one(filter_data)
            else:
                if (type == 'many'):
                    return self.db[table_name].find(filter_data)
                else:
                    return self.db[table_name].find_one(filter_data)
        except:
            raise

    def aggregate(self, db_name, table_name, group_by, filter_data=None, order_by=None):
        try:
            self.db = self.client[db_name]
            pipeline = [
                {"$match": filter_data},
                {"$group": {"_id": "${}".format(group_by), "count": {"$sum": 1}}},
            ]

            if order_by:
                order_by = order_by.split(' ')
                if len(order_by) > 0:
                    if order_by[1].upper() == 'DESC':
                        order_ = -1
                    else:
                        order_ = 1

                    pipeline.append({"$sort": SON([(order_by[0], order_)])})

            return self.db[table_name].aggregate(pipeline)
        except:
            raise

    def insert(self, data, db_name, table_name):
        try:
            self.db = self.client[db_name]
            result = self.db[table_name].save(data)

            return result
        except:
            raise

    def update(self, _id ,data, db_name, table_name):
        try:
            self.db = self.client[db_name]
            result = self.db[table_name].update_one(
                {"_id": _id},
                {
                    "$set": data,
                    "$currentDate": {"lastModified": True}
                }
            )

            return result.modified_count
        except:
            raise

    def delete(self, filter_data, db_name, table_name):
        try:
            self.db = self.client[db_name]
            self.db[table_name].remove(filter_data)
        except:
            raise

    def count(self, db_name, table_name, filter_data=None):
        try:
            self.db = self.client[db_name]
            return self.db[table_name].find(filter_data).count()
        except:
            raise

    def set_ascending(self):
        return pymongo.ASCENDING

    def set_descending(self):
        return pymongo.DESCENDING