from core.model import Model


class AccessToken(Model):
    TABLE = "access_token"
    FIELD_ID = "access_token_id"
    FIELD_ACCESS_TOKEN = "access_token"
    FIELD_ACCESS_TOKEN_SECRET = "access_token_secret"
    FIELD_TWITTER_ACCOUNT_ID = "twitter_account_id"
    FIELD_APPLICATION_ID = "app_id"
    FIELD_STATUS = "status"

    def __init__(self, conn, cursor):
        Model.__init__(self, conn, cursor)

    def save_access_token(self, data):
        try:
            self.db.insert(self.TABLE, data)
            return self.db.execute(commit=False, utf8mb64=True)
        except:
            raise

    def update_access_token(self, data, access_token_id):
        try:
            self.db.insert(self.TABLE, data)
            self.db.exact_where(self.FIELD_ID, access_token_id)
            return self.db.execute(commit=False, utf8mb64=True)
        except:
            raise
