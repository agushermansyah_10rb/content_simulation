from core.model import Model


class Applications(Model):
    TABLE = "applications"
    FIELD_ID = "app_id"
    FIELD_NAME = "app_name"
    FIELD_CONSUMER_KEY = "consumer_key"
    FIELD_CONSUMER_SECRET = "consumer_secret"
    FIELD_STATUS = "app_status"
    FIELD_LAST_UPDATE = "last_update"
    FIELD_READ_PERMISSION = "read_permission"
    FIELD_WRITE_PERMISSION = "write_permission"
    FIELD_DM_PERMISSION = "dm_permission"

    def __init__(self, conn, cursor):
        Model.__init__(self, conn, cursor)

    def save_app(self, data):
        try:
            self.db.insert(self.TABLE, data)
            return self.db.execute(commit=False, utf8mb64=True)
        except:
            raise

    def update_app(self, data, app_id):
        try:
            self.db.insert(self.TABLE, data)
            self.db.exact_where(self.FIELD_ID, app_id)
            return self.db.execute(commit=False, utf8mb64=True)
        except:
            raise

    def get_app(self, app_id=None, start=None, rows=None):
        try:
            self.db.select(self.TABLE, "app")
            if app_id:
                self.db.exact_where(self.FIELD_ID, app_id)
            if (start == 0 or start) and rows:
                self.db.limit(start, rows)
            return self.db.execute(commit=False)
        except:
            raise

    def get_app_token(self, twitter_account_id, app_id=None, status="0"):
        try:
            self.db.select(self.TABLE, "app")
            self.db.join("access_token", "at", on="app.app_id = at.app_id", join_type="INNER")
            if app_id:
                self.db.where("app.app_id = '{}'".format(app_id))
            if twitter_account_id:
                self.db.where("twitter_account_id = '{}'".format(twitter_account_id))
            self.db.exact_where(self.FIELD_STATUS, status)
            self.db.order_by("at.app_id ASC")
            return self.db.execute(commit=False)
        except:
            raise
