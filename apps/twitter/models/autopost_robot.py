from core.model import Model


class AutopostRobot(Model):
    TABLE = "autopost_robot"
    FIELD_ROBOT_ID = "robot_id"
    FIELD_AUTOPOST_CATALOG_ID = "autopost_catalog_id"
    FIELD_TWEET_ID = "tweet_id"
    FIELD_DEVICE = "device"
    FIELD_CREATED = "created_at"
    FIELD_STATUS = "status"
    FIELD_MESSAGE = "message"

    def __init__(self, conn, cursor):
        Model.__init__(self, conn, cursor)

    def save_ar(self, data):
        try:
            self.db.insert(self.TABLE, data)
            return self.db.execute(commit=False, utf8mb64=True)
        except:
            raise

    def get_ar(self, sac_id=None, query=None):
        try:
            self.db.select(self.TABLE, "auto")
            self.db.exact_where(self.FIELD_AUTOPOST_CATALOG_ID, sac_id)
            if query:
                self.db.where(query)
            return self.db.execute(commit=False)
        except:
            raise

    def update_ar(self, data, ac_id):
        try:
            self.db.update(self.TABLE, data)
            self.db.exact_where(self.FIELD_AUTOPOST_CATALOG_ID, ac_id)
            return self.db.execute(commit=False, utf8mb64=True)
        except:
            raise

