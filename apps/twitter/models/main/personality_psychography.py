from apps.twitter.models.main.psychography_catalog import PsychographyCatalog
from core.model import Model


class PersonalityPsychography(Model):

    TABLE = "personality_psychography"
    FIELD_PERSONALITY_ID = "personality_id"
    FIELD_PSYCHOGRAPHY_ID = "psychography_id"
    FIELD_SENTIMENT = "sentiment"
    FIELD_EMOTIONS = "emotions"

    def __init__(self, conn, cursor):
        Model.__init__(self, conn, cursor)
        psychography = PsychographyCatalog(conn, cursor)
        self.PSYCHOGRAPHY_RESTRICTION = psychography.PSYCHOGRAPHY_RESTRICTION

    def get_data_personality_psychography(self, pp_id=None, start=None, rows=None, order=None, p_status=None,
                                          column_custom=None, query=None):
        try:
            column = "pp.personality_id, psy.p_id, sentiment, emotions, p_desc, parent_id, psy.p_alias"
            if column_custom:
                column = column_custom

            self.db.select(self.TABLE, 'pp', column)
            self.db.join("psychography_catalog", "psy", on="pp.psychography_id= psy.p_id")
            self.db.where("psy.p_desc NOT IN ({})".format(",".join(self.PSYCHOGRAPHY_RESTRICTION)))
            if pp_id:
                self.db.where("pp.personality_id= {}".format(pp_id))
            if p_status:
                self.db.join("personality", "p", on="pp.personality_id= p.personality_id")
                self.db.where("personality_status = {}".format(p_status))
            if rows and rows != 0:
                self.db.limit(start, rows)
            if order:
                self.db.order_by(order)
            if query:
                self.db.where(query)
            return self.db.execute(commit=True)
        except Exception, e:
            raise

    def save_data_personality_psychography(self, data):
        try:
            self.db.insert(self.TABLE, data, is_ignore=True)
            return self.db.execute(commit=True).lastrowid
        except Exception, e:
            raise

    def delete_data_personality_psychography(self, personality_id, psychography_id=None):
        try:
            self.db.delete(self.TABLE)
            self.db.exact_where(self.FIELD_PERSONALITY_ID, personality_id)
            self.db.exact_where(self.FIELD_PSYCHOGRAPHY_ID, psychography_id)
            return self.db.execute(commit=False)
        except Exception, e:
            raise

    def get_personality_psychography(self, soldier_id, personality_status=1):
        try:
            self.db.select(self.TABLE, "pp")
            self.db.join("soldier_personality", "sp", on="sp.personality_id = pp.personality_id")
            self.db.join("personality", "p", on="pp.personality_id= p.personality_id")
            self.db.join("psychography_catalog", "psy", on="pp.psychography_id = psy.p_id")
            self.db.where("psy.p_desc NOT IN ({})".format(",".join(self.PSYCHOGRAPHY_RESTRICTION)))
            self.db.exact_where("sp.soldier_id", soldier_id)
            self.db.where("p.personality_status = {}".format(personality_status))
            return self.db.execute(commit=False)
        except Exception, e:
            raise
