from apps.twitter.models.main.psychography_catalog import PsychographyCatalog
from core.model import Model

__author__ = 'Windows'


class AdditionalRobotPsychography(Model):

    TABLE = "isr_main.additional_robot_psychography"

    FIELD_ROBOT_ID = "robot_id"
    FIELD_PSYCHOGRAPHY_ID = "psychography_id"
    FIELD_SENTIMENT = "sentiment"
    FIELD_EMOTIONS = "emotions"

    def __init__(self, conn, cursor):
        Model.__init__(self, conn, cursor)
        psychography = PsychographyCatalog(conn, cursor)
        self.PSYCHOGRAPHY_RESTRICTION = psychography.PSYCHOGRAPHY_RESTRICTION

    def save(self, data):
        try:
            self.db.insert(self.TABLE, data, is_update_field_id=[self.FIELD_ROBOT_ID, self.FIELD_PSYCHOGRAPHY_ID])
            self.db.execute(commit=True)
        except Exception, e:
            raise

    def get_additional_psychography(self, robot_id=None, column="*", query=None):
        try:
            self.db.select(self.TABLE, "arp", column)
            self.db.join("psychography_catalog", "pc", on="arp.psychography_id = pc.p_id", join_type="INNER")
            self.db.exact_where(self.FIELD_ROBOT_ID, robot_id)
            self.db.where("pc.p_desc NOT IN ({})".format(",".join(self.PSYCHOGRAPHY_RESTRICTION)))
            if query:
                self.db.where(query)
            return self.db.execute(commit=False).fetchall
        except Exception, e:
            raise

    def delete_additional_psychography(self, robot_id=None, commit=True):
        try:
            self.db.delete(self.TABLE)
            self.db.where("{} = '{}'".format(self.FIELD_ROBOT_ID, robot_id))
            self.db.execute(commit=commit)
        except Exception, e:
            raise

    def get_psychography_statistic(self, c_id=None, parent_id=None, account_type=None):
        try:
            sql = """SELECT pc.p_id, pc.p_desc, pc.p_alias, pc.p_items, pc.parent_id as parent_id,
                         IFNULL(ps.total_robot,0) as used_count
                         from(
                              SELECT psi.psychography_id as psychography_id, count(distinct psi.robot_id) as total_robot
                              from (
                                    SELECT its.robot_id as robot_id, pp.psychography_id as psychography_id FROM soldier_personality sp
                                    INNER JOIN personality p on sp.personality_id = p.personality_id
                                    INNER JOIN personality_psychography pp on pp.personality_id = sp.personality_id
                                    INNER JOIN isr_twitter.soldier its ON its.soldier_id = sp.soldier_id
                                    WHERE its.robot_id IN (SELECT rc.robot_id from robot_cluster rc
                                    JOIN isr_twitter.robot r ON r.robot_id = rc.robot_id where c_id = '{0}' {3})
                                    UNION ALL
                                    SELECT its.robot_id as robot_id, sp.psychography_id as psychography_id FROM soldier_scene ss
                                    INNER JOIN scene s on ss.scene_id = s.scene_id
                                    INNER JOIN scene_psychography sp ON sp.scene_id = s.scene_id
                                    INNER JOIN isr_twitter.soldier its ON its.soldier_id = ss.soldier_id
                                    WHERE its.robot_id IN (SELECT rc.robot_id from robot_cluster rc
                                    JOIN isr_twitter.robot r ON r.robot_id = rc.robot_id where c_id = '{0}' {3})
                                    UNION ALL
                                    SELECT robot_id, psychography_id from additional_robot_psychography Where robot_id
                                    IN (SELECT rc.robot_id from robot_cluster rc
                                    JOIN isr_twitter.robot r ON r.robot_id = rc.robot_id where c_id = '{0}' {3})
                              ) psi
                                group by psychography_id
                         ) ps
                         RIGHT JOIN psychography_catalog pc
                         ON pc.p_id = ps.psychography_id
                         WHERE IFNULL(pc.parent_id,0) = '{1}' AND
                         pc.p_desc NOT IN ({2})
                         ORDER BY pc.p_desc ASC
                    """.format(c_id,  parent_id, ",".join(self.PSYCHOGRAPHY_RESTRICTION), "AND r.robot_type='{}'".format(account_type) if account_type else "")
            self.db.query(sql)
            return self.db.execute(commit=False)
        except Exception, e:
            raise

    def get_psychography_count(self, c_id=None, p_id=None, account_type=None):
        try:
            sql = """SELECT pc.p_id, pc.p_desc, pc.p_alias, pc.p_items, pc.parent_id as parent_id,
                         IFNULL(ps.total_robot,0) as used_count
                         from(
                              SELECT psi.psychography_id as psychography_id, count(distinct psi.robot_id) as total_robot
                              from (
                                    SELECT its.robot_id as robot_id, pp.psychography_id as psychography_id FROM soldier_personality sp
                                    INNER JOIN personality p on sp.personality_id = p.personality_id
                                    INNER JOIN personality_psychography pp on pp.personality_id = sp.personality_id
                                    INNER JOIN isr_twitter.soldier its ON its.soldier_id = sp.soldier_id
                                    WHERE its.robot_id IN (SELECT rc.robot_id from robot_cluster rc
                                    JOIN isr_twitter.robot r ON r.robot_id = rc.robot_id where c_id = '{0}' {3})
                                    UNION ALL
                                    SELECT its.robot_id as robot_id, sp.psychography_id as psychography_id FROM soldier_scene ss
                                    INNER JOIN scene s on ss.scene_id = s.scene_id
                                    INNER JOIN scene_psychography sp ON sp.scene_id = s.scene_id
                                    INNER JOIN isr_twitter.soldier its ON its.soldier_id = ss.soldier_id
                                    WHERE its.robot_id IN (SELECT rc.robot_id from robot_cluster rc
                                    JOIN isr_twitter.robot r ON r.robot_id = rc.robot_id where c_id = '{0}' {3})
                                    UNION ALL
                                    SELECT robot_id, psychography_id from additional_robot_psychography Where robot_id
                                    IN (SELECT rc.robot_id from robot_cluster rc
                                    JOIN isr_twitter.robot r ON r.robot_id = rc.robot_id where c_id = '{0}' {3})
                              ) psi
                                group by psychography_id
                         ) ps
                         RIGHT JOIN psychography_catalog pc
                         ON pc.p_id = ps.psychography_id
                         WHERE pc.p_id = '{1}' AND
                         pc.p_desc NOT IN ({2})
                         ORDER BY pc.p_desc ASC
                    """.format(c_id, p_id, ",".join(self.PSYCHOGRAPHY_RESTRICTION), "AND r.robot_type='{}'".format(account_type) if account_type else "")
            self.db.query(sql)
            return self.db.execute(commit=False)
        except Exception, e:
            raise

    def get_psychography_ids(self, robot_ids=None):
        try:
            self.db.select(self.TABLE, "arp", "arp.robot_id ,pc.p_desc, arp.emotions, arp.sentiment")
            self.db.join(table="psychography_catalog", alias="pc", on="arp.psychography_id = pc.p_id", join_type="INNER")
            self.db.where("arp.robot_id IN ({})".format(robot_ids))
            return self.db.execute(commit=False).fetchall
        except Exception, e:
            raise

    def get_distinct_ids(self, robot_ids=None):
        try:
            self.db.select(self.TABLE, "arp", "distinct arp.robot_id")
            self.db.where("arp.robot_id IN ({})".format(robot_ids))
            return self.db.execute(commit=False).fetchall
        except Exception, e:
            raise

    # def get_target_commander_psychography(self, robot_ids=None, psychography_ids=None, psychography_id=None, sentiment=None, emotions=None):
    #     try:
    #         sql = "SELECT psy.robot_id as robot_id, psy.psychography_id as psychography_id, psy.sentiment as sentiment, psy.emotions as emotions FROM (SELECT arp.robot_id as robot_id, arp.psychography_id as psychography_id, arp.sentiment as sentiment, arp.emotions as emotions FROM additional_robot_psychography arp WHERE arp.robot_id IN ({0}) AND arp.psychography_id IN ({1})) as psy where psy.psychography_id = '{2}' AND psy.sentiment={3} AND psy.emotions like '%{4}%'".format(robot_ids, psychography_ids, psychography_id, sentiment, emotions)
    #         self.db.query(sql)
    #         return self.db.execute(commit=False).fetchall
    #     except Exception, e:
    #         raise

    def get_target_soldier_psychography(self, robot_ids=None, cac_id=None):
        sub_query = "select cac.cac_source_id from isr_twitter.commander_autopost_catalog cac where cac.cac_id = '{}'".format(cac_id)
        try:
            self.db.select(self.TABLE, "arp", "arp.robot_id, arp.psychography_id, arp.emotions, arp.sentiment")
            self.db.join(table="isr_twitter.general_scheduled_post_psychography", alias="gspp", on="gspp.psychography_id = arp.psychography_id", join_type="INNER")
            self.db.where("FIND_IN_SET(gspp.sentiment, arp.sentiment) > 0")
            self.db.where("FIND_IN_SET(gspp.emotions, arp.emotions) > 0")
            if robot_ids:
                self.db.where("{} IN ({})".format(self.FIELD_ROBOT_ID, robot_ids))
            else:
                self.db.where("{} IN ({})".format(self.FIELD_ROBOT_ID, 'NULL'))

            self.db.where("{} IN ({})".format("gspp.post_id", sub_query))
            return self.db.execute(commit=False)
        except Exception, e:
            raise

    def delete_by_robot_ids(self, robot_ids):
        try:
            self.db.delete(self.TABLE)
            self.db.where("{} IN ({})".format(self.FIELD_ROBOT_ID, robot_ids))
            return self.db.execute(False)
        except:
            raise

    def get_arp_by_robot(self, robot_id):
        try:
            self.db.select(self.TABLE, "arp")
            self.db.where("robot_id = '{}'".format(robot_id))
            return self.db.execute(commit=False)
        except Exception, e:
            raise

    def delete_by_rp(self, robot_ids, p_id):
        try:
            self.db.delete(self.TABLE)
            self.db.where("{} IN ({})".format(self.FIELD_ROBOT_ID, robot_ids))
            self.db.where('{} = "{}"'.format(self.FIELD_PSYCHOGRAPHY_ID, p_id))
            return self.db.execute(False)
        except:
            raise

    def delete_by_robot_p_ids(self, p_ids, robot_ids):
        try:
            self.db.delete(self.TABLE)
            self.db.where("{} IN ({})".format(self.FIELD_PSYCHOGRAPHY_ID, p_ids))
            self.db.where("{} IN ({})".format(self.FIELD_ROBOT_ID, robot_ids))
            return self.db.execute(False)
        except:
            raise
