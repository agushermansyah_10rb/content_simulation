from core.model import Model


class TemplateSchedule(Model):

    TABLE = "template_schedule_temp"
    FIELD_ID = "ts_id"
    FIELD_DESC = "ts_desc"
    FIELD_VALUE = "ts_value"

    def __init__(self, conn, cursor):
        Model.__init__(self, conn, cursor)

    def get_template_schedule(self, _id=None, desc=None):
        try:
            self.db.select(self.TABLE, "ts")
            self.db.exact_where(self.FIELD_ID, _id)
            self.db.exact_where(self.FIELD_DESC, desc)
            return self.db.execute()
        except Exception:
            raise

    def save(self, data):
        try:
            self.db.insert(self.TABLE, data)
            return self.db.execute(False)
        except  Exception, e:
            raise