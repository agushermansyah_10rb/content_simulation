import MySQLdb
from core.model import Model


class PsychographyCatalog(Model):

    TABLE = "psychography_catalog"
    FIELD_ID = "p_id"
    FIELD_P_DESC = "p_desc"
    FIELD_ALIAS = "p_alias"
    FIELD_ITEMS = "p_items"
    FIELD_PARENT_ID = "parent_id"
    PSYCHOGRAPHY_RESTRICTION = ["'deaths'", "'disaster and accident'", "'law and crime'",
                                "'ilegal content'", "'non standard content'"]

    def __init__(self, conn, cursor):
        Model.__init__(self, conn, cursor)

    def save(self, p_id=None, p_desc=None, p_alias=None, p_items=None, parent_id=None):
        try:
            data = {"p_id": p_id, "p_desc": p_desc, "p_alias": p_alias if p_alias else None,
                    "p_items": p_items if p_items else None, "parent_id": parent_id}
            self.db.insert(self.TABLE, data, is_ignore=[self.FIELD_P_DESC, self.FIELD_PARENT_ID])
            self.db.execute(commit=True)
        except Exception, e:
            raise

    def update(self, p_id=None, p_desc=None, p_alias=None, p_items=None, parent_id=None):
        try:
            data = {"p_desc": p_desc, "p_alias": '' if p_alias is None else str(p_alias),
                    "p_items": '' if p_items is None else str(p_items), "parent_id": parent_id}
            self.db.update(self.TABLE, data)
            self.db.where("p_id = '{}' ".format(p_id))
            self.db.execute(commit=True)
        except Exception, e:
            raise

    def get_psychographycal_catalog(self, p_id=None, parent_id=None, keyword=None):
        try:
            field = "pc.p_id as p_id, pc.p_desc as p_desc, pc.p_alias as p_alias, pc.p_items as p_items, "\
                    "pc.parent_id as parent_id, pc_parent.p_desc as parent_name"
            self.db.select(self.TABLE, "pc", field)
            self.db.join(table="psychography_catalog", alias="pc_parent", on="pc_parent.p_id=pc.parent_id",
                         join_type='LEFT')
            if keyword:
                self.db.where("pc.{} like '%{}%' ".format(self.FIELD_P_DESC, MySQLdb.escape_string(keyword)))

            if parent_id and parent_id == '0':
                self.db.where("pc.{} is NULL".format(self.FIELD_PARENT_ID))
            elif parent_id:
                self.db.exact_where("pc.{}".format(self.FIELD_PARENT_ID), parent_id)

            self.db.exact_where("pc.{}".format(self.FIELD_ID), p_id)
            self.db.where("pc.{} NOT IN ({})".format(self.FIELD_P_DESC, ",".join(self.PSYCHOGRAPHY_RESTRICTION)))
            self.db.order_by("pc.p_desc asc")
            return self.db.execute(commit=False)
        except Exception, e:
            raise

    def get_psychographycal_parent(self, keyword=None, p_id=None):
        try:
            field = "pc.p_id as p_id, pc.p_desc as p_desc, pc.p_alias as p_alias, pc.p_items as p_items, " \
                    "pc.parent_id as parent_id"
            self.db.select(self.TABLE, "pc", field)
            if keyword:
                self.db.where("{} like '{}%' ".format(self.FIELD_P_DESC, MySQLdb.escape_string(keyword)))
            self.db.where("pc.p_desc NOT IN ({})".format(",".join(self.PSYCHOGRAPHY_RESTRICTION)))
            self.db.order_by("pc.p_desc asc")
            return self.db.execute(commit=False)
        except Exception:
            raise

    def get_psychography(self, p_desc=None, parent_id=None, p_id=None):
        try:
            self.db.select(self.TABLE, "pc")
            self.db.exact_where(self.FIELD_ID, p_id)
            self.db.exact_where(self.FIELD_P_DESC, p_desc)
            self.db.exact_where(self.FIELD_PARENT_ID, parent_id)
            self.db.order_by("p_desc asc")
            self.db.where("pc.p_desc NOT IN ({})".format(",".join(self.PSYCHOGRAPHY_RESTRICTION)))
            return self.db.execute(commit=False).fetchone
        except:
            raise

    def delete_psychographycal_catalog(self, p_id):
        try:
            self.db.delete(self.TABLE)
            self.db.where("{} = '{}'".format(self.FIELD_ID, p_id))
            self.db.execute(commit=True)
        except Exception, e:
            raise

    def check_psychography(self, p_desc=None, parent_id=None, p_id=None):
        try:
            self.db.select(self.TABLE, "pc", "p_id")
            self.db.exact_where(self.FIELD_P_DESC, MySQLdb.escape_string("{}".format(p_desc)))
            if parent_id:
                self.db.exact_where(self.FIELD_PARENT_ID, parent_id)
            if p_id:
                self.db.exact_where(self.FIELD_ID, p_id)
            return self.db.execute(commit=False)
        except Exception, e:
            raise

    def get_tree_psychography(self, p_id=None):
        try:
            sql = "SELECT p_id, p_desc, parent_id, level FROM (SELECT _id AS p_id, p_desc, parent_id, @cl := @cl +1 " \
                  "AS level FROM ( SELECT @r AS _id," \
                  " (SELECT p_desc FROM psychography_catalog where p_id = _id) as p_desc," \
                  " (SELECT @r := parent_id FROM psychography_catalog WHERE p_id = _id) AS parent_id,  " \
                  "@l := @l +1 AS level FROM (SELECT @r :='{}', @l :=0, @cl :=0) vars, " \
                  "psychography_catalog ps WHERE @r <> 0 ORDER BY level DESC )qi" \
                  " )qo".format(p_id)
            self.db.query(sql)
            return self.db.execute(commit=False).fetchall
        except Exception, e:
            raise

    def get_child(self, p_id, restriction=True):
        try:
            self.db.select(self.TABLE, "pc")
            self.db.exact_where(self.FIELD_PARENT_ID, p_id)
            if restriction:
                self.db.where("pc.p_desc NOT IN ({})".format(",".join(self.PSYCHOGRAPHY_RESTRICTION)))
            return self.db.execute(False)
        except Exception:
            raise

    def get_psychography_fm_by_desc(self, parent_id, desc):
        try:
            self.db.select(self.TABLE, "pc")
            self.db.where('pc.{} = "{}"'.format(self.FIELD_PARENT_ID, parent_id))
            if desc:
                self.db.where("pc.p_desc IN ({})".format(desc))
            else:
                self.db.where('pc.p_desc IS NULL')
            return self.db.execute(False)
        except Exception:
            raise
