from core.model import Model


class RobotSchedule(Model):

    TABLE = "robot_schedule"
    FIELD_ROBOT_ID = "robot_id"
    FIELD_HOUR = "hour"
    FIELD_TYPE = "type"
    FIELD_COUNT = "count"
    FIELD_STATUS = "status"

    def __init__(self, conn, cursor):
        Model.__init__(self, conn, cursor)

    def save(self, data):
        try:
            self.db.insert(self.TABLE, data)
            self.db.execute(False)
        except Exception:
            raise

    def delete(self, robot_id):
        try:
            self.db.delete(self.TABLE)
            self.db.exact_where(self.FIELD_ROBOT_ID, robot_id)
            self.db.execute(False)
        except Exception:
            raise

    def get_robot_schedule(self, robot_id=None, hour=None, robot_status=None, _type=None, types=None):
        try:
            self.db.select(self.TABLE, "ss")
            if robot_status:
                self.db.join("isr_twitter.robot", 'r', on="ss.robot_id = r.robot_id", join_type="INNER")
                self.db.exact_where("robot_status", robot_status)
            self.db.exact_where(self.FIELD_ROBOT_ID, robot_id)
            self.db.exact_where(self.FIELD_HOUR, hour)
            self.db.exact_where(self.FIELD_TYPE, _type)
            if types:
                self.db.where('{} IN ({})'.format(self.FIELD_TYPE, types))
            return self.db.execute()
        except Exception:
            raise

    def update(self, robot_id, data):
        try:
            self.db.update(self.TABLE, data)
            self.db.exact_where(self.FIELD_ROBOT_ID, robot_id)
            self.db.execute(False)
        except Exception:
            raise

    def decrease_counter(self, robot_id, hour, _type, count):
        try:
            self.db.update(self.TABLE)
            self.db.update_set(self.FIELD_COUNT, count, False)
            self.db.exact_where(self.FIELD_ROBOT_ID, robot_id)
            self.db.exact_where(self.FIELD_HOUR, hour)
            self.db.exact_where(self.FIELD_TYPE, _type)
            self.db.execute(False)
        except Exception:
            raise
