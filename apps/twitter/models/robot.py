from core.model import Model


class Robot(Model):
    TABLE = "robot"
    FIELD_ID = "robot_id"
    FIELD_TWITTER_ACCOUNT_ID = "twitter_account_id"
    FIELD_CLONE_ID = "clone_id"
    FIELD_POST_SCHEDULE = "post_schedule"
    FIELD_TEMPLATE_POST_SCHEDULE = "template_post_schedule"
    FIELD_LAST_TWEET = "last_tweet"
    FIELD_BANNED = "banned"
    FIELD_BLOCK = "block"
    FIELD_MENTIONS = "mentions"
    FIELD_PASSWORD = "password"
    FIELD_STATUS = "robot_status"
    FIELD_ROBOT_TYPE = "robot_type"

    def __init__(self, conn, cursor):
        Model.__init__(self, conn, cursor)

    def save_robot(self, data):
        try:
            self.db.insert(self.TABLE, data)
            return self.db.execute(commit=False, utf8mb64=True)
        except:
            raise

    def get_robot(self, robot_id=None, twitter_account_id=None, status=None, start=None, rows=None, keyword=None,
                  order_by=None):
        try:
            self.db.select(self.TABLE, "rb")
            self.db.join("account", "a", on="a.twitter_account_id = rb.twitter_account_id", join_type="INNER")
            if robot_id:
                self.db.exact_where(self.FIELD_ID, robot_id)
            if twitter_account_id:
                self.db.where("rb.twitter_account_id = '{}'".format(twitter_account_id))
            if status:
                self.db.exact_where(self.FIELD_STATUS, status)
            if keyword:
                self.db.where("(screen_name like '%{0}%' OR name like '%{0}%')".format(keyword))
            if order_by:
                self.db.order_by(order_by)
            if (start == 0 or start) and rows:
                self.db.limit(start, rows)
            return self.db.execute(commit=False)
        except:
            raise

    def update_robot(self, data, robot_id):
        try:
            self.db.update(self.TABLE, data)
            self.db.exact_where(self.FIELD_ID, robot_id)
            return self.db.execute(commit=False)
        except:
            raise
