from core.model import Model


class AutopostCatalog(Model):
    TABLE = "autopost_catalog"
    FIELD_ID = "id"
    FIELD_ROBOT_ID = "robot_id"
    FIELD_TWEET_ID = "tweet_id"
    FIELD_AGE = "age"
    FIELD_GENDER = "gender"
    FIELD_RELIGION = "religion"
    FIELD_CITY = "city"
    FIELD_PROVINCE = "province"
    FIELD_COUNTRY = "country"
    FIELD_TWEET = "tweet"
    FIELD_SENTIMENT = "sentiment"
    FIELD_EMOTIONS = "emotions"
    FIELD_KEYWORDS = "keywords"
    FIELD_CLASS = "class"
    FIELD_STATUS = "status"
    FIELD_STATUS_EXPIRED = "status_expired"
    FIELD_TIME_EXPIRED = "time_expired"
    FIELD_CREATED = "created_at"
    FIELD_PUB_DATE = "pub_date"
    FIELD_PUB_DAY = "pub_day"
    FIELD_PUB_MONTH = "pub_month"
    FIELD_PUB_YEAR = "pub_year"
    FIELD_TYPE = "type"
    FIELD_CAMPAIGN_ID = "campaign_id"
    FIELD_TWEETED_AT = "tweeted_at"

    def __init__(self, conn, cursor):
        Model.__init__(self, conn, cursor)

    def save_ac(self, data):
        try:
            self.db.insert(self.TABLE, data)
            return self.db.execute(commit=False, utf8mb64=True)
        except:
            raise

    def update_ac(self, data, ac_id):
        try:
            self.db.update(self.TABLE, data)
            self.db.exact_where(self.FIELD_ID, ac_id)
            return self.db.execute(commit=False, utf8mb64=True)
        except:
            raise

    def get_ac(self, robot_id=None, catalog_id=None, status=None, s_date=None, e_date=None):
        try:
            self.db.select(self.TABLE, "ac")
            if robot_id:
                self.db.exact_where(self.FIELD_ROBOT_ID, robot_id)
            if catalog_id:
                self.db.exact_where(self.FIELD_ID, catalog_id)
            if status:
                self.db.exact_where(self.FIELD_STATUS, status)
            if s_date and e_date:
                self.db.where("date({}) BETWEEN '{}' AND '{}'".format(self.FIELD_PUB_DAY, s_date, e_date))
            return self.db.execute(commit=False)
        except Exception, e:
            raise