from core.model import Model


class Account(Model):
    TABLE = "account"
    FIELD_ID = "twitter_account_id"
    FIELD_SCREEN_NAME = "screen_name"
    FIELD_NAME = "name"
    FIELD_CREATED_AT = "created_at"
    FIELD_PROFILE_IMAGE = "profile_image_url"
    FIELD_DESCRIPTION = "description"
    FIELD_LOCATION = "location"
    FIELD_STATUSES_COUNT = "statuses_count"
    FIELD_FRIENDS_COUNT = "friends_count"
    FIELD_FOLLOWERS_COUNT = "followers_count"
    FIELD_FAVOURITES_COUNT = "favourites_count"
    FIELD_URL = "url"
    FIELD_VERIFIED = "verified"
    FIELD_TIME_ZONE = "time_zone"
    FIELD_PROTECTED = "protected"
    FIELD_LANGUAGE = "language"
    FIELD_AGE = "age"
    FIELD_GENDER = "gender"
    FIELD_RELIGION = "religion"
    FIELD_COUNTRY = "country"
    FIELD_PROVINCE = "province"
    FIELD_CITY = "city"
    FIELD_SUBDISTRICT = "subdistrict"
    FIELD_VILLAGE = "village"
    FIELD_BANNED = "banned"
    FIELD_BLOCKED = "blocked"
    FIELD_UNFOLLOWED = "unfollowed"
    FIELD_INACTIVE = "inactive"
    FIELD_GET_MENTION = "get_mention"
    FIELD_PSYCHOGRAPHY = "psychography"
    FIELD_EMAIL = "email"
    FIELD_PHONE = "phone_no"

    def __init__(self, conn, cursor):
        Model.__init__(self, conn, cursor)

    def save_twitter_account(self, data):
        try:
            self.db.insert(self.TABLE, data)
            return self.db.execute(commit=False, utf8mb64=True)
        except:
            raise
