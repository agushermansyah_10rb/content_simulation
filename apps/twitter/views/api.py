import re
import argparse

from bottle import run, get, request, route, post
from core import config
from core import logger
from helper.general import build_result, error_response
from apps.twitter.controllers.api.api import API


@get("/get_tweets")
def get_tweets():
    try:
        data = api.get_tweets()
        return build_result(True, data=data, message="success")
    except Exception, e:
        logger.error(str(e))
        return error_response(500, str(e))


if __name__ == '__main__':
    argp = argparse.ArgumentParser(description="API")
    argp.add_argument('-c', '--config', dest='config', help='engine configuration file', default="config.conf")
    argp.add_argument('-p', '--port', dest='port', help='engine port listened', type=int, default=9002)
    argp.add_argument('-w', '--worker', dest='worker', help='number of worker', type=int, default=6)
    argp.add_argument('-to', '--timeout', dest='timeout', help='request timeout', type=int, default=90)

    args = argp.parse_args()
    config.set_config_file(args.config)

    api = API()
    run(host='0.0.0.0', port=args.port, server='gunicorn', workers=args.worker, timeout=args.timeout,
        worker_class='gevent')
