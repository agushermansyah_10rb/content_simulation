import argparse

from core import config
from apps.twitter.controllers.robot.parser import DatabankParser


if __name__ == "__main__":
    mode = ["databank"]
    argp = argparse.ArgumentParser(description="Robot Engine")
    argp.add_argument('-c', dest='config', help='engine configuration file', default="config.conf")
    argp.add_argument('-m', dest='mode', help='engine mode {}'.format(", ".join(mode)), default="config.conf")
    args = argp.parse_args()
    config.set_config_file(args.config)

    if args.mode == "databank":
        parser = DatabankParser()
        parser.databank_parser()
    else:
        argp.print_help()
