import argparse

from apps.twitter.controllers.robot.stream_keywords import DatabankStream
from core import config

if __name__ == "__main__":
    mode = ["databank_stream"]
    argp = argparse.ArgumentParser(description="Twitter Streaming")
    argp.add_argument('-c', '--config', help='engine configuration file', default="config.conf")
    argp.add_argument('-m', '--mode', help='engine mode {}'.format(", ".join(mode)))
    argp.add_argument('-r', '--robot')
    argp.add_argument('-at', '--access_token', help='use access token from config (true/false)', default=False)
    args = argp.parse_args()
    config.set_config_file(args.config)
    robot = args.robot
    mode = args.mode
    access_token = args.access_token

    if access_token:
        access_token = access_token.lower()
        if access_token == "true":
            access_token = True
        else:
            access_token = False

    engine = DatabankStream()
    if mode == 'databank_stream':
        engine.start(access_token=access_token)
    else:
        argp.print_help()
