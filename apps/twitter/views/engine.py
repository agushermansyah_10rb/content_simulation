import argparse

from core import config
from apps.twitter.controllers.robot.extract_autopost_robot import ExtractTweet
from apps.twitter.controllers.robot.engine import Engine

if __name__ == "__main__":
    mode = ["set_robot_schedule", "worker_post_tweet", "pusher_extract_autopost", "worker_extract_autopost",
            "pusher_push_autopost", "worker_push_autopost"]
    argp = argparse.ArgumentParser(description="Robot Engine")
    argp.add_argument('-c', '--config', help='engine configuration file', default="config.conf")
    argp.add_argument('-m', '--mode', help='engine mode {}'.format(", ".join(mode)))
    argp.add_argument('-r', '--robot_id', help='push robot id')
    argp.add_argument('-t', '--types', help='types')
    argp.add_argument('-i', '--net_interface', help='network interface')
    argp.add_argument('-ip', '--ip_address', help='IP address')
    args = argp.parse_args()
    config.set_config_file(args.config)
    mode = args.mode
    net_intf = args.net_interface

    if args.mode == "set_robot_schedule":
        engine = Engine()
        engine.assign_robot_schedule()
    elif args.mode == "worker_post_tweet":
        engine = Engine()
        engine.send_twitter_tweet(net_intf)
    elif args.mode == "worker_extract_autopost":
        engine = ExtractTweet()
        engine.worker_extract_autopost(ip_address=args.ip_address)
    elif args.mode == "pusher_extract_autopost":
        engine = ExtractTweet()
        engine.pusher_extract_autopost()
    elif args.mode == "worker_push_autopost":
        engine = ExtractTweet()
        engine.worker_push_autopost(ip_address=args.ip_address)
    elif args.mode == "pusher_push_autopost":
        engine = ExtractTweet()
        engine.pusher_push_autopost()
    else:
        argp.print_help()
