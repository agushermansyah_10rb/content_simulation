from core import config
from datetime import datetime
import logging
import logging.handlers
import os


__author__ = 'irvan'

class Logger:
    def __init__(self, config_load=None):
        self.path = "/var/log"
        self.max_bytes = 300000
        self.backup_count = 5
        if config_load:
            self.config = config_load
        else:
            self.config = config.load()

        if "log" in self.config.sections():
            config_items = [key[0] for key in self.config.items("log")]
            if "path" in config_items:
                self.path = self.config.get("log", "path")
            if "max_bytes" in config_items:
                self.max_bytes = self.config.get("log", "max_bytes")
            if "backup_count" in config_items:
                self.backup_count = self.config.get("log", "backup_count")

        self.path = "{}/{}/{}".format(self.path, os.environ.get("APPS_NAME"), os.environ.get("VIEW_NAME"))
        if not os.path.exists(self.path) and not os.environ.get("LOG"):
            os.makedirs(self.path)

    def print_log_to_file(self, log_type, message, print_log=False, print_to_file=True):
        log_file = "{}_{}".format(os.environ.get("LOG"), log_type) if os.environ.get(
            "LOG") else self.path + '/{0}{1}'.format(os.environ.get("MODE_NAME"), log_type)

        logger = logging.getLogger('{0}{1}_logger'.format(os.environ.get("MODE_NAME"), log_type))
        logger.setLevel(logging.DEBUG)
        handler = logging.handlers.RotatingFileHandler(log_file, maxBytes=self.max_bytes,
                                                       backupCount=self.backup_count)
        logger.addHandler(handler)

        if print_to_file:
            if log_type == 'error':
                logger.error("[{0}] -- {1}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), message))
            elif log_type == 'info':
                logger.info("[{0}] -- {1}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), message))
            elif log_type == 'warning':
                logger.warning("[{0}] -- {1}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), message))

        logger.removeHandler(handler)

        if print_log:
            print "[{0}] [{1}] {2}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), log_type.upper(), message)

    def log_prefix(self):
        return "{}".format(os.environ.get("LOG")) if os.environ.get("LOG") else self.path + '/{0}'.format(
            os.environ.get("VIEW_NAME"))


def info(message, print_log=False, config_load=None):
    logger = Logger(config_load)
    logger.print_log_to_file(log_type="info", message=message, print_log=print_log)


def warning(message, print_log=False, config_load=None):
    logger = Logger(config_load)
    logger.print_log_to_file(log_type="warning", message=message, print_log=print_log)


def error(message, print_log=True, sentry=None, config_load=None):
    logger = Logger(config_load)
    logger.print_log_to_file(log_type="error", message=message, print_log=print_log)
    if sentry:
        sentry.captureException()


def log_prefix():
    logger = Logger()
    return logger.log_prefix()
