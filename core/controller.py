import base64
import MySQLdb
import json
import os
import re
import random
import socket
import fcntl
import struct
from ssdb import SSDB
from DBUtils.PersistentDB import PersistentDB
from raven import Client
from apps.twitter.models.main.psychography_catalog import PsychographyCatalog
from content_management.utils import Utils
from core import config, logger
from helper.general import to_dict, carrot_xml_parser_v2, init_redis, init_pusher_beanstalk


class Controller:
    time_range = {"y": 365, "q": 90, "m": 30, "w": 7, "t": 1}

    def __init__(self):
        self.config = config.load()
        self.sentry = Client(self.config.get("sentry", "dsn_server"))
        self.pool_content_management = PersistentDB(
            MySQLdb, host=self.config.get('db_content_simulation', 'dbhost'),
            user=self.config.get('db_content_simulation', 'dbuser'),
            passwd=self.config.get('db_content_simulation', 'dbpwd'),
            db=self.config.get('db_content_simulation', 'dbname'), charset='utf8')
        self.redis_pointer = init_redis()
        self.redis_pointer_ipa = init_redis(db="2")
        self.ssdb = SSDB(host=self.config.get("ssdb", "server"), port=int(self.config.get("ssdb", "port")))

    def get_ip_address(self, custom_socket=None):
        list_socket = ["enp3s0f0", "wlp9s0", "eth0", "eth1", "eth2", "wlan0",
                       "eno2", "eno1", "em1", "em2", "em3", "em4", "ens160",
                       "enp0s20u2c2", "ens32"]

        if custom_socket:
            list_socket = custom_socket

        ip_address = None
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        for net_inf in list_socket:
            try:
                ip_address = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', net_inf[:15]))[20:24])
            except Exception, e:
                # print e
                pass
        return ip_address

    def get_utils_object(self, socmed='twitter'):
        conn = self.pool_content_management.connection()
        cursor = conn.cursor(MySQLdb.cursors.DictCursor)
        psycho_catalog_model = PsychographyCatalog(conn, cursor)
        try:
            psychography_catalog = psycho_catalog_model.get_psychographycal_catalog().fetchall
            utilobj = Utils(dbname="mongo_db", configObj=self.config, get_ugc=False,
                            psychography_catalog=psychography_catalog, disable_logger=True, socmed=socmed)
            return utilobj
        except Exception:
            raise
        finally:
            conn.close()
            cursor.close()

    def error_logging(self, message, config_load=None):
        logger.error(message, sentry=self.sentry, config_load=config_load)

    def tweet_management(self, text):
        try:
            # """ 23 => total count link character at twitter """
            total_text = len(text)
            list_link = []
            list_word = []
            for dt in text.split(" "):
                if re.search("^http(s)?://[^ ]+\.[A-Za-z]+(\.[A-Za-z]+)?", str(dt)):
                    total_text = total_text - len(dt) + 23
                    list_link.append(dt)
                else:
                    list_word.append(dt)

            sentence = " ".join("{}".format(x) for x in list_word)
            links = " ".join("{}".format(x) for x in list_link)

            total_space = len(list_link) if list_link else 0
            total_link = len(list_link) * 23
            available_text = 280
            use_elipsis = False

            if links:
                if total_text > 280:
                    use_elipsis = True

                available_text = available_text - total_link - total_space
                sentence = "{}".format(sentence[:available_text])
            else:
                if total_text > 280:
                    use_elipsis = True

            return sentence, links, available_text, use_elipsis
        except Exception, e:
            raise e

    def text_management(self, text, mentions=None, hashtags=None, random_mention=False):
        try:
            sentence, links, total_text, use_elipsis = self.tweet_management(text=text)

            mention_space = 0
            hashtag_space = 0
            elipsis = 0
            next_trim = True
            trim_tweet = None

            if mentions:
                mention_space = 1
            if hashtags:
                hashtag_space = 1
            if use_elipsis:
                elipsis = 3

            # Default text tweet
            text_tweet = "{}...".format(sentence[:total_text - elipsis]) if use_elipsis else sentence
            text_tweet = "{} {}".format(text_tweet, links) if links else text_tweet

            # Compare mention and hashtag
            if mentions and hashtags:
                sentence_mention = "{} {}".format(mentions, sentence)
                trim_by_elipsis = False
                if len(sentence_mention) > total_text:
                    total_trim = total_text - (len(mentions) + mention_space + 3)  # 3 => elipsis
                    if total_trim > 0:
                        trim_tweet = sentence[:total_trim]
                        total_text = total_trim
                        trim_by_elipsis = True

                        if random_mention:
                            # Mention can be anywhere
                            text_and_mention = self.random_mention(mentions, "{}...".format(trim_tweet))
                        else:
                            text_and_mention = "{} {}...".format(mentions, trim_tweet)

                        if len(text_and_mention) <= 280:
                            text_tweet = text_and_mention
                        else:
                            next_trim = False
                else:
                    if random_mention:
                        # Mention can be anywhere
                        text_tweet = self.random_mention(mentions, sentence)
                    else:
                        text_tweet = sentence_mention

                    trim_tweet = sentence
                    total_text = total_text - (len(mentions) + mention_space)

                if next_trim:
                    sentence_hashtag = "{} {}".format(trim_tweet, hashtags)
                    if len(sentence_hashtag) > total_text:
                        elipsis = 0 if trim_by_elipsis else 3
                        total_trim = total_text - (len(hashtags) + hashtag_space + elipsis)
                        if total_trim > 0:
                            trim_tweet = trim_tweet[:total_trim]
                            tweet_hashtag = "{} {}... {}".format(mentions, trim_tweet, hashtags)

                            if len(tweet_hashtag) <= 280:
                                if links:
                                    if random_mention:
                                        # Mention can be anywhere
                                        text_tweet = self.random_mention(mentions, "{}...".format(trim_tweet))
                                        text_tweet = "{} {} {}".format(text_tweet, links, hashtags)
                                    else:
                                        text_tweet = "{} {}... {} {}".format(mentions, trim_tweet, links, hashtags)
                                else:
                                    if random_mention:
                                        # Mention can be anywhere
                                        text_tweet = self.random_mention(mentions, "{}...".format(trim_tweet))
                                        text_tweet = "{} {}".format(text_tweet, hashtags)
                                    else:
                                        text_tweet = "{} {}... {}".format(mentions, trim_tweet, hashtags)
                    else:
                        if random_mention:
                            # Mention can be anywhere
                            trim_tweet = "{}...".format(trim_tweet) if trim_by_elipsis else trim_tweet
                            text_tweet = self.random_mention(mentions, trim_tweet)
                            text_tweet = "{} {}".format(text_tweet, hashtags)
                        else:
                            text_tweet = "{} {}".format(sentence_mention, hashtags)

            elif mentions and not hashtags:
                sentence_mention = "{} {}".format(mentions, sentence)
                if len(sentence_mention) > total_text:
                    total_trim = total_text - (len(mentions) + mention_space + 3)  # 3 => elipsis
                    if total_trim > 0:
                        trim_tweet = sentence[:total_trim]
                        text_and_mention = "{} {}...".format(mentions, trim_tweet)
                        if len(text_and_mention) <= 280:
                            if links:
                                if random_mention:
                                    # Mention can be anywhere
                                    text_tweet = self.random_mention(mentions, "{}...".format(trim_tweet))
                                    text_tweet = "{} {}".format(text_tweet, links)
                                else:
                                    text_tweet = "{} {}... {}".format(mentions, trim_tweet, links)
                            else:
                                if random_mention:
                                    # Mention can be anywhere
                                    text_tweet = self.random_mention(mentions, "{}...".format(trim_tweet))
                                else:
                                    text_tweet = "{} {}...".format(mentions, trim_tweet)
                else:
                    if links:
                        if random_mention:
                            # Mention can be anywhere
                            text_tweet = self.random_mention(mentions, sentence)
                        else:
                            text_tweet = "{} {} {}".format(mentions, sentence, links)
                    else:
                        text_tweet = "{} {}".format(mentions, sentence)

            elif hashtags and not mentions:
                sentence_hashtag = "{} {}".format(sentence, hashtags)
                if len(sentence_hashtag) > total_text:
                    total_trim = total_text - (len(hashtags) + hashtag_space + 3)  # 3 => elipsis
                    if total_trim > 0:
                        trim_tweet = sentence[:total_trim]
                        text_and_hashtag = "{}... {}".format(trim_tweet, hashtags)
                        if len(text_and_hashtag) <= 280:
                            if links:
                                text_tweet = "{}... {} {}".format(trim_tweet, links, hashtags)
                            else:
                                text_tweet = "{}... {}".format(trim_tweet, hashtags)
                else:
                    if links:
                        text_tweet = "{} {} {}".format(sentence, links, hashtags)
                    else:
                        text_tweet = "{} {}".format(sentence, hashtags)

            return text_tweet
        except Exception, e:
            raise e

    def random_mention(self, mention, text):
        try:
            list_text = text.split(" ")
            total_index = len(list_text)
            rand_index = random.randint(0, total_index - 1)
            list_text[rand_index] = "{} {}".format(mention, list_text[rand_index])
            text_tweet = " ".join(x for x in list_text)
            return text_tweet
        except Exception, e:
            raise e
