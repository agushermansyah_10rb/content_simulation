from raven import Client
from core import config
from eblib.db import Db
from helper.general import check_solr_doc
from eblib.solr import SolrLib, SolrException


class Model:
    def __init__(self, conn, cursor):
        self.db = Db(conn, cursor)
        # self.config = config.load()
        # self.sentry = Client(self.config.get("sentry", "dsn_server"))

    @staticmethod
    def insert_solr(solr, data):
        data = [check_solr_doc(data)]
        response = solr.update(data, commit=False)
        if response.status != 200:
            raise SolrException(response.raw_content["error"]["msg"])
        else:
            print "SOlr Berhasil"
            return True
