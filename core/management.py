from os import listdir
import os
import sys

__author__ = 'irvan'


def execute_apps(argv):
    apps = argv[1]
    views = argv[2]
    mode = ''
    if '-m' in argv:
        index_mode = argv.index("-m") + 1
        if len(argv) > index_mode:
            mode = "{}_".format(argv[index_mode])

    if apps in listdir("apps"):
        if "{}.py".format(views) in listdir("apps/{}/views".format(apps)):
            # set argument
            sys.argv = argv[2:]

            # set settings of module to environ
            os.environ.setdefault("CONFIG_PATH", "apps/{}".format(apps))
            os.environ.setdefault("CONFIG_FILE", "config.conf".format(apps))
            os.environ.setdefault("APPS_NAME", apps)
            os.environ.setdefault("VIEW_NAME", views)
            os.environ.setdefault("MODE_NAME", mode)

            if '-log' in sys.argv:
                log_path = sys.argv[sys.argv.index("-log") + 1]
                os.environ.setdefault("LOG", log_path)
                sys.argv.pop(sys.argv.index("-log") + 1)
                sys.argv.pop(sys.argv.index("-log"))

            # module execute
            execfile("apps/{}/views/{}.py".format(apps, views), {'__name__': '__main__'})
        else:
            # Views is not available in selected apps
            print "{}.py is not available in {}'s apps".format(views, apps)
    else:
        # apps is not available in project
        print "apps {} is not available".format(apps)