import requests

__author__ = 'GANI'

class SimpleRequests:

    def __init__(self):
        pass

    def get_response(self, url, headers=None, params=None, cookies=None, timeout=60):
        result = None, None
        try:
            if headers:
                if isinstance(headers, dict):
                    if 'User-Agent' in headers or 'user-agent' in headers:
                        headers = headers
                    else:
                        headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36'
                else:
                    headers = None
            else:
                headers = {
                    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36'
                }

            req = requests.get(url, headers=headers, params=params, cookies=cookies, timeout=timeout)
            if req:
                result = req.content, req.status_code
        except Exception as e:
            print '[GET RESPONSE][ERROR] - {}'.format(str(e))
        finally:
            return result

    def set_response(self, url, headers=None, params=None, cookies=None, timeout=60):
        result = None, None
        try:
            if headers:
                if isinstance(headers, dict):
                    if 'User-Agent' in headers or 'user-agent' in headers:
                        headers = headers
                    else:
                        headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36'
                else:
                    headers = None
            else:
                headers = {
                    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36'
                }

            req = requests.post(url, data=params, headers=headers, cookies=cookies, timeout=timeout)
            if req:
                result = req.content, req.status_code
        except Exception as e:
            print '[GET RESPONSE][ERROR] - {}'.format(str(e))
        finally:
            return result