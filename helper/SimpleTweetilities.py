import re
import random
import tweepy
import oauth2
import urlparse
import mechanize
import cookielib

from pyquery import PyQuery as pq

__editor__ = 'Agan Yudistira'

class MyTwitterUtils:
    def __init__(self, consumer_key=None, consumer_key_secret=None, access_token=None, access_token_secret=None, proxy=None):
        self.default_consumer_key = 'NgWdvaOJEnDv3pg1L8WicwCCh'
        self.default_consumer_key_secret = 'JfJM3eKKn3JUEL21tE4tMWusi0xJyajxK6ve9MRYf6SvqsqzaQ'
        self.default_access_token = '730435916825690112-e37bXVJ0MDxcy7EAeszMHipNdUWL6oI'
        self.default_access_token_secret = 'TBkqTDwhpKKTJIP8Mjh2ZobFYVSN3VpgajHy5a19vMR9p'

        self.consumer_key = consumer_key
        self.consumer_key_secret = consumer_key_secret
        self.access_token = access_token
        self.access_token_secret = access_token_secret
        self.proxy = None

    def get_api(self):
        result = None
        try:
            if self.consumer_key and self.consumer_key_secret and self.access_token and self.access_token_secret:
                auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_key_secret)
                auth.set_access_token(self.access_token, self.access_token_secret)
                api = tweepy.API(auth, proxy=self.proxy)
                result = api
            else:
                raise Exception ("consumer or access token is empty")
        except (tweepy.TweepError, BaseException) as e:
            try:
                print '[GET API][ERROR] - {}'.format(str(e[0][0]["message"]))
            except:
                print '[GET API][ERROR] - {}'.format(str(e))
        finally:
            return result

    def get_user_data(self, username):
        result = None
        try:
            if self.consumer_key and self.consumer_key_secret and self.access_token and self.access_token_secret:
                auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_key_secret)
                auth.set_access_token(self.access_token, self.access_token_secret)
            else:
                auth = tweepy.OAuthHandler(self.default_consumer_key, self.default_consumer_key_secret)
                auth.set_access_token(self.default_access_token, self.default_access_token_secret)

            api = tweepy.API(auth)

            raw_data = api.get_user(username)._json
            if raw_data:
                result = raw_data
        except (tweepy.TweepError, BaseException) as e:
            try:
                print '[GET USER DATA][ERROR] - {}'.format(str(e[0][0]["message"]))
            except:
                print '[GET USER DATA][ERROR] - {}'.format(str(e))
        finally:
            return result


class MyTwitterTokenUtils:
    def __init__(self, auth_data):
        self.request_token_url = 'https://api.twitter.com/oauth/request_token'
        self.access_token_url = 'https://api.twitter.com/oauth/access_token'
        self.authorize_url = 'https://api.twitter.com/oauth/authorize'

        self.auth_data = auth_data
        self.official_app_name = self.auth_data['official_app_name']
        self.official_consumer_key = self.auth_data['official_consumer_key']
        self.official_consumer_secret = self.auth_data['official_consumer_secret']
        self.screen_name = self.auth_data['screen_name']
        self.password = self.auth_data['password']
        self.proxy = self.auth_data['proxy'] if 'proxy' in self.auth_data and self.auth_data['proxy'] else None
        self.get_ua = ['Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
                       'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
                       'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36']

    def get_browser(self):
        result = None
        try:
            debug_browser = False
            browser = mechanize.Browser()

            # set cookies
            cookies = cookielib.LWPCookieJar()
            browser.set_cookiejar(cookies)

            u_agent = random.choice(self.get_ua)

            if self.proxy:
                # set proxy
                raw_proxy = {
                    'http': self.proxy
                }
                browser.set_proxies(raw_proxy)

            # browser option
            browser.set_handle_equiv(True)
            browser.set_handle_redirect(True)
            browser.set_handle_referer(True)
            browser.set_handle_robots(False)

            # debugging message
            browser.set_debug_http(debug_browser)
            browser.set_debug_redirects(debug_browser)
            browser.set_debug_responses(debug_browser)

            # refresh avoid hangs
            browser.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)
            # set user agent
            browser.addheaders = [
                ('User-Agent', '{}'.format(u_agent))
            ]

            result = browser
        except Exception as e:
            print '[GET BROWSER][ERROR] - {}'.format(str(e))
        finally:
            return result

    def get_request_url(self):
        result = None
        try:
            consumer = oauth2.Consumer(key=self.official_consumer_key, secret=self.official_consumer_secret)
            client = oauth2.Client(consumer)

            response, content = client.request(self.request_token_url, "GET")
            if response['status'] != '200':
                raise Exception("Invalid response {}.".format(response['status']))

            request_token = dict(urlparse.parse_qsl(content))
            self.auth_data['access_token'] = request_token['oauth_token']
            self.auth_data['access_token_secret'] = request_token['oauth_token_secret']

            result = "{}?oauth_token={}".format(self.authorize_url, request_token['oauth_token'])
        except Exception as e:
            print '[GET REQUEST URL][ERROR] - {}'.format(str(e))
        finally:
            return result

    def get_verifier_code(self, request_url):
        result = None
        try:
            browser = self.get_browser()
            browser.open(request_url)

            # begin automation
            browser.select_form(nr=0)
            browser.form['session[username_or_email]'] = self.screen_name
            browser.form['session[password]'] = self.password
            response = browser.submit()

            # getting result
            html = response.read()
            element = pq(html)
            verifier_code = element("code")
            verifier_code = re.sub(r'<[^>]*>', '', str(verifier_code))

            if verifier_code:
                result = verifier_code
        except Exception as e:
            print '[GET VERIFIER CODE][ERROR] - {}'.format(str(e))
        finally:
            return result

    def generate_token(self):
        result = None
        try:
            request_url = self.get_request_url()
            if request_url:
                verifier_code = self.get_verifier_code(request_url)

                if verifier_code:
                    authorize_consumer = oauth2.Consumer(key=self.official_consumer_key, secret=self.official_consumer_secret)
                    authorize_token = oauth2.Token(key=self.auth_data['access_token'], secret=self.auth_data['access_token_secret'])
                    authorize_token.set_verifier(verifier_code)
                    client = oauth2.Client(authorize_consumer, authorize_token)
                    response, content = client.request(self.access_token_url, "POST")
                    access_token = dict(urlparse.parse_qsl(content))
                    access_token['access_token'] = access_token['oauth_token']
                    access_token['access_token_secret'] = access_token['oauth_token_secret']
                    access_token['consumer_key'] = self.official_consumer_key
                    access_token['consumer_secret'] = self.official_consumer_secret

                    del access_token['oauth_token']
                    del access_token['oauth_token_secret']
                    result = access_token
        except Exception as e:
            print '[GENERATE TOKEN][ERROR] - {}'.format(str(e))
        finally:
            return result
