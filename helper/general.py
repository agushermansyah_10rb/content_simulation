# -*- coding: utf-8 -*-
import hashlib
import json
import random
import re
from datetime import datetime, date, timedelta
from decimal import Decimal
from operator import itemgetter
from xml.etree import ElementTree

import redis
from twitter.api import Twitter
from twitter.oauth import OAuth

from core import config
from eblib.beanstalk import Pusher, Worker
from eblib.solr import SolrException
from PIL import Image

__author__ = 'root'

time_frame = {"y": 365, "s": 60, "q": 90, "m": 30, "w": 7, "t": 0}
mapping_age = {
        "0 - 17": "below_18",
        "18 - 25": "18_25",
        "18 - 30": "above_35",
        "26 - 35": "26_35",
        "30 - unlimited": "above_35",
        "36 - unlimited": "above_35",
        "below 17": "below_18",
        "18-25": "18_25",
        "26-35": "26_35",
        "36-unlimited": "above_35",
        "below_18": "below_18",
        "18_25": "18_25",
        "26_35": "26_35",
        "above_35": "above_35",
        None: None,
        "None": None,
        "unknown": None,
        "": None
    }

age_map_profiling_statistic = {
            "0 - 17": "0_17",
            "18 - 25": "18_25",
            "18 - 30": "above_35",
            "26 - 35": "26_35",
            "30 - unlimited": "36_unlimited",
            "36 - unlimited": "36_unlimited",
            "below 17": "0_17",
            "18-25": "18_25",
            "26-35": "26_35",
            "36-unlimited": "36_unlimited",
            "below_18": "0_17",
            "18_25": "18_25",
            "26_35": "26_35",
            "above_35": "36_unlimited",
            "18_30":"26_35",
            "30_unlimited":"36_unlimited",
            "0_17":"0_17"
    }


def error_response(status_code, message):
    # status_code = 200
    # body = {'status': False,
    #         'message': message}
    # response.body = json.dumps(body)
    # response.status = status_code
    # response.content_type = 'application/json'
    # return response
    return build_result(False, message=message)


def str2bool(v):
    if str(v).lower() in ['t', 'true', 'y', 'yes', '1']:
        return True
    return False


def encryption(string):
    import crypt
    m = hashlib.md5()
    m.update(string)
    return crypt.crypt(m.hexdigest(), m.hexdigest())


def oauth_login(config, section):
    from twitter.oauth import OAuth
    return OAuth(config.get('twitter', section + '_token'), config.get('twitter', section + '_token_secret'),
                 config.get('twitter', 'api_key'), config.get('twitter', 'api_secret'))


def to_dict(obj, class_key=None):
    if isinstance(obj, dict):
        for k in obj.keys():
            obj[k] = to_dict(obj[k], class_key)
        return obj
    elif hasattr(obj, "__iter__"):
        return [to_dict(v, class_key) for v in obj]
    elif hasattr(obj, "__dict__"):
        data = dict([(key, to_dict(value, class_key))
                     for key, value in obj.__dict__.iteritems()
                     if not callable(value) and not key.startswith('_')])
        if class_key is not None and hasattr(obj, "__class__"):
            data[class_key] = obj.__class__.__name__
        return data
    else:
        try:
            return str(obj)
        except UnicodeEncodeError:
            return str(obj.encode('utf-8'))


def init_redis(db=None):
    redis_host = config.load().get('redis', 'server_host')
    redis_port = config.load().get('redis', 'server_port')
    redis_db = db if db is not None else config.load().get('redis', 'server_db')
    redis_pointer = redis.StrictRedis(host=redis_host, port=int(redis_port), db=int(redis_db))
    redis_pointer.config_set("save", "1 1")

    return redis_pointer


def is_topic_pass_date(_date, start_date, end_date):
    try:
        if start_date and int(_date) < int(start_date.strftime("%Y%m%d")):
            return False
        if end_date and int(_date) > int(end_date.strftime("%Y%m%d")):
            return False
    except TypeError:
        return False

    return True


def init_pusher_beanstalk(conf, tube, section='beanstalk'):
    serverhost = conf.get(section, 'serverhost')
    try:
        serverport = int(conf.get(section, 'serverport'))
    except:
        serverport = 11300
    basetube = conf.get(section, 'tubename')
    tubename = "%s_%s" % (basetube, tube)
    return Pusher(tubename, serverhost, serverport)


def init_worker_beanstalk(conf, tube, worker_id='worker', section='beanstalk'):
    serverhost = conf.get(section, 'serverhost')
    basetube = conf.get(section, 'tubename')
    tubename = "%s_%s" % (basetube, tube)
    return Worker(tubename, worker_id, serverhost)


def generate_pub_date(_date):
    pub_sec = _date.strftime("%Y%m%d%H%M%S")
    pub_min = _date.strftime("%Y%m%d%H%M")
    pub_hour = _date.strftime("%Y%m%d%H")
    pub_day = _date.strftime("%Y%m%d")
    pub_month = _date.strftime("%Y%m")
    pub_year = _date.strftime("%Y")

    return pub_sec, pub_min, pub_hour, pub_day, pub_month, pub_year


def generate_topic_pub_day(s_date, e_date):
    try:
        s_date = datetime.strptime(str(s_date), "%Y-%m-%d").strftime("%Y%m%d")
        e_date = datetime.strptime(str(e_date), "%Y-%m-%d").strftime("%Y%m%d")
    except Exception:
        raise

    return s_date, e_date


def not_null(var, _unicode=False):
    if var == "None":
        var = None

    if var == 0:
        var = str(var)

    if var and isinstance(var, basestring) and _unicode:
        var = unicode(var).encode('ascii', 'xmlcharrefreplace')

    return var


def boolint(_bool):
    if _bool is True or _bool == "True":
        return 1

    return 0


def json_build(data):
    if isinstance(data, dict):
        for k, v in data.iteritems():
            data[k] = json_build(v)

    if isinstance(data, list):
        for i, v in enumerate(data):
            data[i] = json_build(v)

    if isinstance(data, tuple):
        data = json_build([item for item in data])

    if isinstance(data, datetime):
        data = str(data)

    if isinstance(data, date):
        data = str(data)

    if isinstance(data, long):
        data = str(data)

    if isinstance(data, Decimal):
        data = str(data)

    return data


def check_solr_doc(data, update=False, key=None):
    new_data = dict()
    for k, v in data.iteritems():
        if not_null(v):
            if update and k is not key:
                v = {"set": v}
            new_data[k] = v

    return new_data


def tweet_date(created_at, time_zone=7):
    try:
        create_at = datetime.strptime(created_at, '%a %b %d %H:%M:%S +0000 %Y')
    except ValueError:
        create_at = datetime.strptime(created_at, "%Y-%m-%d %H:%M:%S")
    create_at = create_at + timedelta(hours=time_zone)
    return create_at


def build_result(status=False, data=None, message=None):
    if data:
        data = json_build(data)
    return {"status": status, "data": data, "message": message}


def twitter_error(error):
    return int(re.search('^Twitter sent status (\d{3})', str(error)).group(1))


def bytimeintervali(seconds, delay=5, _date=None):
    now = datetime.now()
    if _date:
        date_tmp = _date
    else:
        date_tmp = now - timedelta(seconds=delay)
    s = int(date_tmp.strftime("%Y%m%d%H%M%S"))
    return s - (s % seconds)


def carrot_xml_parser(doc):
    ids = []
    root = ElementTree.fromstring(doc)
    n_id = {}
    for doc in root.findall('document'):
        n_id[doc.get('id')] = doc.find('content_id').text

    result = {}
    for group in root.findall('group'):
        data = {}
        issue_total = group.get('size')
        issue_name = group.find('title').find('phrase').text

        if issue_name != "Other Topics":
            data['total'] = int(issue_total)
            data['ids'] = list()
            for doc in group.findall('document'):
                nid = n_id[doc.get('refid')]
                data['ids'].append(nid)

            result[issue_name] = data
        ids = []
        for issue in result:
            for _id in result[issue]['ids']:
                ids.append(_id)
        ids = list(set(ids))
    issues = []
    for issue in result:
        data = {'ids': result[issue]['ids'],
                'total': result[issue]['total'],
                'issue': issue}
        issues.append(data)
    issues = sorted(issues, key=itemgetter('total'), reverse=True)
    return {'issue': issues, 'ids': ids}


def carrot_xml_parser_v2(doc):
    root = ElementTree.fromstring(doc)
    n_id = {}
    for doc in root.findall('document'):
        n_id[doc.get('id')] = doc.find('content_id').text

    result = {}
    for group in root.findall('group'):
        data = {}
        issue_total = group.get('size')
        issue_name = group.find('title').find('phrase').text

        if issue_name != "Other Topics":
            data['total'] = int(issue_total)
            data['ids'] = list()
            for doc in group.findall('document'):
                nid = n_id[doc.get('refid')]
                data['ids'].append(nid)

            issue_name = re.sub(r'\bbr\b', "", issue_name, flags=re.I)
            result[issue_name] = data
    return result


"""
Copyright (c) 2011, Giuseppe Tribulato.
License: MIT (see http://www.opensource.org/licenses/mit-license.php for details)
"""
from bottle import request
import functools
import inspect


def checkParams(**types):
    def decorate(f):
        farg, _, _, def_params = inspect.getargspec(f)
        if def_params is None: def_params = []
        farg = farg[:len(farg) - len(def_params)]

        param_info = [(par, ptype, par in farg) for par, ptype in types.iteritems()]

        @functools.wraps(f)
        def wrapper(*args, **kargs):
            getparam = request.GET.get
            for par, ptype, required in param_info:
                value = getparam(par)
                if not value:  # None or empty str
                    if required:
                        error = "\%s requires the parameter %s with %s type" % (wrapper.__name__, par, ptype.__name__)
                        raise TypeError(error)
                    continue
                try:
                    kargs[par] = ptype(value)
                except:
                    error = "Parameter %s must be %s" % (par, ptype.__name__)
                    raise ValueError(error)

            return f(*args, **kargs)

        return wrapper

    return decorate


def get_time_range():
    class Return:
        def today(self, now):
            return now

        def last_day(self, now):
            return now - timedelta(days=time_frame['t'] - 1)

        def last_week(self, now):
            return now - timedelta(days=time_frame['w'] - 1)

        def last_month(self, now):
            return now - timedelta(days=time_frame['m'] - 1)

        def last_querter(self, now):
            return now - timedelta(days=time_frame['q'] - 1)

        def last_semester(self, now):
            return now - timedelta(days=time_frame['s'] - 1)

        def last_year(self, now):
            return now - timedelta(days=time_frame['y'] - 1)

    return Return()


time_range = get_time_range()


def get_date_by_tf(now, tf):
    if tf == 'w':
        end_date = time_range.last_week(now)
    elif tf == 'm':
        end_date = time_range.last_month(now)
    elif tf == 'q':
        end_date = time_range.last_querter(now)
    elif tf == 's':
        end_date = time_range.last_semester(now)
    elif tf == 'y':
        end_date = time_range.last_year(now)
    else:
        end_date = time_range.today(now)

    return end_date


def bytimeinterval(seconds, delay=5):
    date_tmp = datetime.now()
    s = int(date_tmp.strftime("%S"))
    s_group = (s - (s % seconds) + seconds) - delay

    if s_group == 60:
        s_group = 0
        date_tmp = date_tmp + timedelta(minutes=1)
    return "{0}{1}".format(date_tmp.strftime("%Y%m%d%H%M"), datetime.strptime(str(s_group), "%S").strftime("%S"))


def build_dict(seq, key):
    return dict((d[key], dict(d, index=i)) for (i, d) in enumerate(seq))


def update_account_subscription_cache(redis_pointer, subs_account):
    redis_pointer.delete("general:account_subscription")
    redis_pointer.set("general:account_subscription", json.dumps(subs_account))


def get_account_subscription(redis_pointer):
    account = redis_pointer.get("general:account_subscription")
    if account:
        account = json.loads(account)

    return account


def catch_signal_term(signum, frame):
    raise KeyboardInterrupt


def get_image_from_google(query, image_type="face", rsz=8, image_size="medium"):
    import requests

    # Notice that the start changes for each iteration in order to request a new set of images for each loop
    url = 'https://ajax.googleapis.com/ajax/services/search/images'
    payload = {"v": "1.0", "q": query, "imgtype": image_type, "rsz": rsz, "imgsz": image_size}

    req = requests.get(url, payload)
    # Get results using JSON
    contents = json.loads(req.content)
    req.close()

    results = None
    if "responseData" in contents:
        if "results" in contents["responseData"]:
            results = contents["responseData"]["results"]

    return results


def get_text_group_index(content, group, l_bracket, r_bracket):
    index = 0
    content_x = ""
    # format handler like "{{Football player infobox" or "{{infobox Football player"
    __header__ = re.compile(r'(\{{2}(kotak\s*info|infobox)\s*[^\|]+|\{{2}[^\|]+\s*(kotak\s*info|infobox))',
                            re.IGNORECASE)
    search_header = re.search(__header__, content)
    if search_header:
        header = search_header.group(1)
        index = content.index(header)
        content_x = content[index:]
    # -- original version --
    # index = content.index(l_bracket * 2 + group)
    # content_x = content[index:]
    open_br = 0
    last_index = index
    # start updated version
    # prev_is_lbracket = False
    # prev_is_rbracket = False
    # count_double_bracket = 0
    # end updated version
    for idx, char in enumerate(content_x):
        if char == l_bracket:
            open_br += 1
        elif char == r_bracket:
            open_br -= 1
        last_index += 1
        if open_br == 0:
            break
    # start updated version
    #     if char == l_bracket:
    #         if idx == 0:
    #             prev_is_lbracket = True
    #             continue
    #         else:
    #             if prev_is_lbracket:
    #                 count_double_bracket += 1
    #             else:
    #                 prev_is_lbracket = True
    #     elif char == r_bracket:
    #         if prev_is_rbracket:
    #             count_double_bracket -= 1
    #         else:
    #             prev_is_rbracket = True
    #     else:
    #         prev_is_lbracket = False
    #         prev_is_rbracket = False
    #     last_index += 1
    #     if count_double_bracket == 0:
    #         last_index += 1
    #         break
    # end updated version

    return index, last_index


def get_text_group_index2(content, group, l_bracket, r_bracket):
    index = 0
    content_x = ""
    # format handler like "{{Football player infobox" or "{{infobox Football player"
    __header__ = re.compile(r'(\{{2}(kotak\s*info|infobox)\s*[^\|]+|\{{2}[^\|]+\s*(kotak\s*info|infobox))',
                            re.IGNORECASE)
    search_header = re.search(__header__, content)
    if search_header:
        header = search_header.group(1)
        index = content.index(header)
        content_x = content[index:]
    index = content.index(l_bracket * 2 + group)
    content_x = content[index:]
    open_br = 0
    last_index = index
    for char in content_x:
        if char == l_bracket:
            open_br += 1
        elif char == r_bracket:
            open_br -= 1
        last_index += 1
        if open_br == 0:
            break

    return index, last_index


def insert_solr(solr, data):
    data = [check_solr_doc(data)]
    response = solr.update(data, commit=False)
    if response.status != 200:
        raise SolrException(response.raw_content["error"]["msg"])
    else:
        return True


def update_solr(solr, data, key):
    data = [check_solr_doc(data, True, key)]
    response = solr.update(data, commit=False)
    if response.status != 200:
        raise SolrException(response.raw_content["error"]["msg"])


def delete_solr(solr, query):
    query = query if query else '*:*'
    response = solr.delete_by_query(query)
    if response.status != 200:
        raise SolrException(response.raw_content["error"]["msg"])
    else:
        return True


def get_twitter_user_detail(consumer_key, consumer_secret, access_token, access_token_secret, user_id):
    auth = OAuth(access_token, access_token_secret, consumer_key, consumer_secret)
    twitter_api = Twitter(api_version='1.1', auth=auth)
    try:
        user = twitter_api.users.show(user_id=user_id)
    except Exception:
        raise
    finally:
        del auth
        del twitter_api

    return user


def get_random_minutes():
    minute = str(random.randrange(0, 59))
    if len(minute) == 1:
        minute = "0%s" % minute

    return minute

def convert_age_type(age):
    if isinstance(age, tuple) or isinstance(age, list):
        age = ''.join(age)

    if age not in mapping_age:
        return None

    return age

def get_age_from_mapping(age):
    return mapping_age[age] if age in mapping_age else None

def get_age_profiling_mapping(age):
    return age_map_profiling_statistic[age]

def get_random_time(start_hours=0, end_hours=23, start_minutes=0, end_minutes=59, start_seconds=0, end_seconds=59):
    hours = random.randrange(start=start_hours, stop=end_hours, step=1)
    minutes = random.randrange(start=start_minutes, stop=end_minutes, step=1)
    seconds = random.randrange(start=start_seconds, stop=end_seconds, step=1)
    return str(hours)+":"+str(minutes)+":"+str(seconds)

def resize_image(filename, basewidth):
    import PIL
    from PIL import Image

    img = Image.open(filename)
    wpercent = (basewidth / float(img.size[0]))
    hsize = int((float(img.size[1]) * float(wpercent)))
    img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
    img.save(filename)

def get_number_day():
    number_day = datetime.today().weekday()
    return int(number_day) + 1

def get_default_setting_apps():
    default_setting = {"soldier_schedule_clone_time":0,
                       "soldier_schedule_template": 0,
                       "soldier_manage_post_based_commander": 0,
                       "soldier_manage_post_based_clone": 0,
                       "soldier_auto_relationship_similar_religion": 0,
                       "soldier_auto_relationship_similar_location": 0,
                       "soldier_auto_relationship_similar_gender": 0,
                       "soldier_auto_relationship_similar_age": 0,
                       "soldier_auto_relationship_new_follower": 50,
                       "soldier_auto_relationship_follower": "0-0",
                       "soldier_auto_relationship_country": "Indonesia", "soldier_auto_follow_unfollow": 0,
                       "soldier_auto_follow_rted": 0,
                       "soldier_auto_relationship_psychography":"{}",
                       "commander_schedule_template":0,
                       "commander_auto_follow_unfollow": 0,
                       "commander_auto_follow_rted": 0,
                       "commander_auto_relationship_age": "Below_18",
                       "commander_auto_relationship_gender": "Male",
                       "commander_auto_relationship_religion": "Islam",
                       "commander_auto_relationship_follower": "50-100",
                       "commander_auto_relationship_province": "DKI Jakarta",
                       "commander_auto_relationship_country": "Indonesia",
                       "commander_maximum_post": 50,
                       "commander_schedule_source_time": 0,
                       "commander_auto_reationship_psychography":"{}",
                       "commander_manager_post_psychography": "{}"
                        }
    return default_setting


def generate_pub_day():
    return str(datetime.now().year)+str(datetime.now().month)+str(datetime.now().day)


def chunk_list(l, n):
    return [l[i:i + n] for i in xrange(0, len(l), n)]


def trim_tweet(text, hashtags=None):
    tmp = ''
    hashtag = ''
    hashtag_length = 1
    max_tweet_length = 210
    if text:
        if hashtags:
            hashtag = ' '.join(["#{}".format(h) for h in list(set(hashtags)) if h])
            hashtag = hashtag.strip()
            hashtag_length = len(hashtag)
        for tw in text.split(' '):
            if len(tmp) < (max_tweet_length + hashtag_length):
                tmp += ' '+tw
        text = '{} {}'.format(tmp.strip(), hashtag)
    return text


def parse_proxy(url):
    url = url.strip()
    protocol = re.sub('^(https?).*', '\g<1>', url)
    if re.search('\@', url):
        username = re.sub('^.*\/\/([A-z0-9]+)\:.*', '\g<1>', url)
        password = re.sub('^.*\:([A-z0-9\!]+)\@.*', '\g<1>', url)
    else:
        username = None
        password = None

    ip = re.sub('.*(\@|\/\/)(.*)\:\d+\/?$', '\g<2>', url)
    port = re.sub('.*\:(\d+)\/?$', '\g<1>', url)
    return protocol, username, password, ip, port


def modify_tweet(text, hashtags=None, with_link=True):
    tmp = ""
    link = ""
    hashtag = ""
    tweet = ""
    hashtag_length = 0
    link_length = 0
    space_length = 1
    if text:
        if re.search("(https?://[^ ]+$)", str(text)):
            link = re.search(".*(https?://[^ ]+$)", str(text)).group(1)
            text = re.sub("(https?://[^ ]+$)", "", str(text))
            link_length = len(link)
            space_length = 2
        if hashtags:
            hashtag = " ".join(["#{}".format(h) for h in list(set(hashtags)) if h])
            hashtag = hashtag.strip()
            hashtag_length = len(hashtag)
        for tw in text.split(' '):
            tmp = "{} {}".format(tmp, tw)
            total = int(len(tmp)) + link_length + hashtag_length + space_length
            if total < 280:
                tweet = "{} {}".format(tweet, tw)
        if with_link:
            if link:
                tweet = re.sub("\s$", "", str(tweet))
                tweet += ' ' + link
        if hashtag:
            tweet = re.sub("\s$", "", str(tweet))
            tweet += ' ' + hashtag
        tweet = re.sub("^\s", "", str(tweet))
    return tweet


def specific_date(pubdate, with_timezone=True):
    start_date = datetime.now().strptime(pubdate, "%Y-%m-%d %H:%M:%S")
    if with_timezone:
        delta_time = start_date + timedelta(hours=7)
    else:
        delta_time = start_date
    pub_year = delta_time.strftime("%Y")
    pub_month = delta_time.strftime("%Y%m")
    pub_day = delta_time.strftime("%Y%m%d")
    pub_hour = delta_time.strftime("%Y%m%d%H")
    pub_min = delta_time.strftime("%Y%m%d%H%M")
    pub_second = delta_time.strftime("%Y%m%d%H%M%S")
    pub_date = delta_time.strftime("%Y-%m-%d %H:%M:%S")
    return pub_year, pub_month, pub_day, pub_hour, pub_min, pub_second, pub_date


def set_active_hour(low, up, length):
    result = []
    step = (up - low) / float(length)
    for i in range(length):
        result.append(low)
        low = low + step

    result_dict = {}
    for v in result:
        rounded = str(int(v))
        if len(rounded) == 1:
            rounded = '0{}'.format(rounded)

        if rounded not in result_dict:
            result_dict[rounded] = 1
        else:
            result_dict[rounded] += 1

    return result_dict

def get_default_active_hour():
    dict_hour = dict()
    for i in range(0, 24):
        i = str(i)
        if len(i) < 2:
            i = '0{}'.format(i)

        dict_hour[i] = 0

    return dict_hour


def time_gap_percentage(start_time, end_time):
    p_used = 100
    rest = end_time - datetime.now()
    total = end_time - start_time
    if rest.total_seconds() > 0:
        diff = '{:.2%}'.format(rest.total_seconds() / total.total_seconds())
        p_left = diff.replace('%', '')
        p_used = 100 - float(p_left)
    return p_used


def parse_percentage(total, value):
    return int(round((total * float(value)) / 100))


def generate_facebook_campaign_schedule(compositions):
    schedule = {}

    for v in compositions:
        compositions[v] = parse_percentage(24, compositions[v])

    for v in range(0, 24):
        for vv in list(compositions):
            if not compositions[vv]:
                del compositions[vv]

        if compositions:
            action = random.choice(compositions.keys())
            schedule[v] = action

            compositions[action] -= 1
        else:
            schedule[v] = None

    return schedule


def generate_facebook_fm_schedule(compositions, start_hour, end_hour):
    schedule = {}
    end_hour += 1

    for v in compositions:
        compositions[v] = parse_percentage((end_hour - start_hour), compositions[v])

    for v in range(start_hour, end_hour):
        for vv in list(compositions):
            if not compositions[vv]:
                del compositions[vv]

        if compositions:
            action = random.choice(compositions.keys())
            schedule[v] = action

            compositions[action] -= 1
        else:
            schedule[v] = None

    return schedule


def get_size_picture(img_path):
    result = None, None
    try:
        im = Image.open(img_path)
        width, height = im.size

        if width and height:
            result = width, height
    except Exception as e:
        print e
    finally:
        return result

class TweetGenHelper:

    def __init__(self):
        # HELPER HMTL ENT, HASHTAG, & LINK
        self.numHelper = [
            'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan',
            'sepuluh', 'sebelas', 'duabelas', 'tigabelas', 'empatbelas', 'limabelas',
            'enambelas', 'tujuhbelas', 'delapanbelas', 'sembilanbelas', 'duapuluh',
            'duapuluhsatu', 'duapuluhdua', 'duapuluhtiga', 'duapuluhempat', 'duapuluhlima', 'duapuluhenam',
            'duapuluhtujuh', 'duapuluhdelapan', 'duapuluhsembilan', 'tigapuluh', 'tigapuluhsatu', 'tigapuluhdua',
            'tigapuluhtiga', 'tigapuluhempat', 'tigapuluhlima', 'tigapuluhenam', 'tigapuluhtujuh', 'tigapuluhdelapan',
            'tigapuluhsembilan', 'empatpuluh', 'empatpuluhsatu', 'empatpuluhdua', 'empatpuluhtiga', 'empatpuluhempat',
            'empatpuluhlima', 'empatpuluhenam', 'empatpuluhtujuh', 'empatpuluhdelapan', 'empatpuluhsembilan',
            'limapuluh'
        ]

        self.tagMentions = list()
        self.htmlEnt = list()
        self.hashtags = list()
        self.links = list()

    def encode_text(self, tweet_text):
        result = None
        try:
            tweet_text = re.sub('\?', 'TANDATANYAHELPER', tweet_text)

            self.tagMentions = re.findall('\@[A-z0-9_]*', tweet_text)
            for idx, entities in enumerate(self.tagMentions):
                tweet_text = re.sub(entities, 'ME{}'.format(self.numHelper[idx].upper()), tweet_text)

            self.htmlEnt = re.findall('\&\#[^\;]*\;', tweet_text)
            for idx, entities in enumerate(self.htmlEnt):
                tweet_text = re.sub(entities, 'HE{}'.format(self.numHelper[idx].upper()), tweet_text)

            self.hashtags = re.findall('(^\#[^< ]*|(?<=\s)\#[^< ]*)', tweet_text)
            for idx, hashtag in enumerate(self.hashtags):
                tweet_text = re.sub(hashtag, 'HT{}'.format(self.numHelper[idx].upper()), tweet_text)

            self.links = re.findall('https?[^\s]*', tweet_text)
            for idx, link in enumerate(self.links):
                tweet_text = re.sub(link, 'LN{}'.format(self.numHelper[idx].upper()), tweet_text)

            result = tweet_text
        except Exception as e:
            print '[ENCODE TEXT][ERROR] - {}'.format(str(e))
        finally:
            return result

    def decode_text(self, tweet_text):
        result = None
        try:
            for im, mention in enumerate(self.tagMentions):
                tweet_text = re.sub('ME{}'.format(self.numHelper[im].upper()), mention, tweet_text)

            for ie, entities in enumerate(self.htmlEnt):
                tweet_text = re.sub('HE{}'.format(self.numHelper[ie].upper()), entities, tweet_text)

            for ha, hashtag in enumerate(self.hashtags):
                tweet_text = re.sub('HT{}'.format(self.numHelper[ha].upper()), hashtag, tweet_text)

            for il, link in enumerate(self.links):
                tweet_text = re.sub('LN{}'.format(self.numHelper[il].upper()), link, tweet_text)

            tweet_text = re.sub('\_', '.UNDERSCOREHELPER.', tweet_text)

            tweet_text = re.sub('\&\s?amp\s?\;\s?', '&', tweet_text)
            tweet_text = re.sub('\s+', ' ', tweet_text)
            tweet_text = re.sub('\&(?=[A-z0-9])', '& ', tweet_text)
            tweet_text = re.sub('\#\s+', '#', tweet_text)
            tweet_text = re.sub('\s?\.\s?$', '.', tweet_text)
            tweet_text = re.sub('(?<=[A-z0-9])\s\.', '.', tweet_text)
            tweet_text = re.sub('@\s(?=[A-z0-9])', '@', tweet_text)
            tweet_text = re.sub('(?<=[A-z0-9])\s+\:\s+(?=[A-z0-9])', ': ', tweet_text)
            tweet_text = re.sub('(?<=[A-z0-9])\s+\,\s+(?=[A-z0-9])', ', ', tweet_text)
            tweet_text = re.sub('(?<=[A-z0-9])\s+\.\s+(?=[A-z0-9])', '. ', tweet_text)
            tweet_text = re.sub('(?<=[\d])\;(?=(\&))', '; ', tweet_text)

            tweet_text = re.sub('.UNDERSCOREHELPER.', '_', tweet_text)
            tweet_text = re.sub('TANDATANYAHELPER', '?', tweet_text)

            result = tweet_text
        except Exception as e:
            print '[DECODE TEXT][ERROR] - {}'.format(str(e))
        finally:
            return result
