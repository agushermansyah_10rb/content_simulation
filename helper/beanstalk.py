from datetime import datetime, timedelta
import calendar

__author__ = 'irvan'


def pubhour_job_builder(pub_hour=None, pub_day=None, pub_month=None, pub_year=None, past_day=None):
    job_date = []
    try:
        if pub_hour:
            if datetime.strptime(pub_hour, "%Y%m%d%H"):
                job_date.append(pub_hour)
        elif pub_day:
            if datetime.strptime(pub_day, "%Y%m%d"):
                for pub_hour in range(int("{}00".format(pub_day)), int("{}24".format(pub_day))):
                    job_date.append(pub_hour)
        elif pub_month:
            if datetime.strptime(pub_month, "%Y%m"):
                date = datetime.strptime(pub_month, "%Y%m")
                year = date.strftime("%Y")
                month = date.strftime("%m")
                month_range = calendar.monthrange(int(year), int(month))
                for pub_day in range(int("{}01".format(pub_month)),
                                     int("{}{}".format(pub_month, month_range[1])) + 1):
                    for pub_hour in range(int("{}00".format(pub_day)), int("{}24".format(pub_day))):
                        job_date.append(pub_hour)
        elif pub_year:
            if datetime.strptime(pub_year, "%Y"):
                date = datetime.strptime(pub_year, "%Y")
                year = date.strftime("%Y")
                for month in range(1, 13):
                    if month <= 9:
                        month = "0{}".format(month)
                    else:
                        month = str(month)
                    pub_month = "{}{}".format(year, month)
                    month_range = calendar.monthrange(int(year), int(month))
                    for pub_day in range(int("{}01".format(pub_month)),
                                         int("{}{}".format(pub_month, month_range[1])) + 1):
                        for pub_hour in range(int("{}00".format(pub_day)), int("{}24".format(pub_day))):
                            job_date.append(pub_hour)
        elif past_day:
            print past_day
            for i in range(0, int(past_day)):
                pub_day = (datetime.now() - timedelta(days=i+1)).strftime("%Y%m%d")
                for pub_hour in range(int("{}00".format(pub_day)), int("{}24".format(pub_day))):
                    job_date.append(pub_hour)
        else:
            pub_hour = datetime.now().strftime("%Y%m%d%H")
            job_date.append(pub_hour)
    except:
        raise

    return job_date

def pubday_job_builder(pub_day=None, pub_month=None, pub_year=None, past_day=None):
    job_date = []
    try:
        if pub_day:
            if datetime.strptime(pub_day, "%Y%m%d"):
                job_date.append(pub_day)
        elif pub_month:
            if datetime.strptime(pub_month, "%Y%m"):
                date = datetime.strptime(pub_month, "%Y%m")
                year = date.strftime("%Y")
                month = date.strftime("%m")
                month_range = calendar.monthrange(int(year), int(month))
                for pub_day in range(int("{}01".format(pub_month)), int("{}{}".format(pub_month, month_range[1])) + 1):
                    job_date.append(str(pub_day))
        elif pub_year:
            if datetime.strptime(pub_year, "%Y"):
                date = datetime.strptime(pub_year, "%Y")
                year = date.strftime("%Y")
                for month in range(1, 13):
                    if month <= 9:
                        month = "0{}".format(month)
                    else:
                        month = str(month)
                    p_month = "{}{}".format(year, month)
                    month_range = calendar.monthrange(int(year), int(month))
                    for pub_day in range(int("{}01".format(p_month)), int("{}{}".format(p_month, month_range[1])) + 1):
                        job_date.append(str(pub_day))
        elif past_day:
            for i in range(0, int(past_day)):
                pub_day = datetime.now() - timedelta(days=i+1)
                job_date.append(pub_day.strftime("%Y%m%d"))
        else:
            job_date.append(datetime.now().strftime("%Y%m%d"))
    except:
        raise

    return job_date

def set_priority(pub_day=None, pub_hour=None):
    if pub_hour:
        pub_day = datetime.strptime(str(pub_hour), "%Y%m%d%H").strftime("%Y%m%d")

    priority = 10000
    if pub_day:
        now = datetime.now()
        job_date = datetime.strptime(str(pub_day), "%Y%m%d")
        delta = now - job_date
        if delta.days == 0:
            priority = 10
        elif delta.days > 0:
            priority = delta.days * 100
    return priority


if __name__ == "__main__":
    pub_day = None
    pub_month = None
    pub_year = None
    past_day = None

    print pubday_job_builder(pub_day, pub_month, pub_year, past_day)
