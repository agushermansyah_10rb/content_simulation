from kafka import KafkaClient, SimpleConsumer, SimpleProducer


class KafkaConsumer(SimpleConsumer):
    def __init__(self, hosts, group, topic, partitions=None):
        client = KafkaClient(hosts)
        super(KafkaConsumer, self).__init__(client, group, topic, partitions=partitions)


class KafkaProducer(SimpleProducer):
    def __init__(self, hosts, topic):
        client = KafkaClient(hosts)
        super(KafkaProducer, self).__init__(client)
        self.topic = topic

    def set_message(self, message):
        self.send_messages(self.topic, message)