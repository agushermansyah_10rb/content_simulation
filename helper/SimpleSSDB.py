from ssdb import SSDB

__author__ = 'GANI'


class SimpleSSDB:

    def __init__(self, host, port):
        self.ssdb = SSDB(host=host, port=port)

    # MANUAL USE
    def get_ssdb(self):
        result = None
        try:
            if self.ssdb:
                result = self.ssdb
        except Exception as e:
            print '[GET SSDB][ERROR] - {}'.format(str(e))
        finally:
            return result

    # COMMANDS
    def check_exist(self, hash_name, key):
        result = False
        try:
            exist = self.ssdb.hexists(hash_name, key)
            if exist:
                result = True
        except Exception as e:
            print '[CHECK EXIST][ERROR] - {}'.format(str(e))
        finally:
            return result

    # QUEUE SECTION
    def qpush_data(self, queue_name, data):
        result = False
        try:
            qpush = self.ssdb.qpush(queue_name, data)
            if qpush:
                result = True
        except Exception as e:
            print '[QPUSH DATA][ERROR] - {}'.format(str(e))
        finally:
            return result

    def front_qpush_data(self, queue_name, data):
        result = False
        try:
            qpush = self.ssdb.qpush_front(queue_name, data)
            if qpush:
                result = True
        except Exception as e:
            print '[FRONT QPUSH DATA][ERROR] - {}'.format(str(e))
        finally:
            return result

    # HASH SECTION
    def hset_data(self, hash_name, key, value):
        result = False
        try:
            hset = self.ssdb.hset(hash_name, key, value)
            if hset:
                result = True
        except Exception as e:
            print '[HSET DATA][ERROR] - {}'.format(str(e))
        finally:
            return result

    def multi_hset_data(self, hash_name, key_value):
        result = False
        try:
            if isinstance(key_value, dict):
                hset = self.ssdb.multi_hset(hash_name, **key_value)
                if hset:
                    result = True
            else:
                raise Exception('please input dictionary of keys and value')
        except Exception as e:
            print '[MULTI HSET DATA][ERROR] - {}'.format(str(e))
        finally:
            return result

    def hget_data(self, hash_name, key):
        result = None
        try:
            hget = self.ssdb.hget(hash_name, key)
            if hget:
                result = hget
        except Exception as e:
            print '[HGET DATA][ERROR] - {}'.format(str(e))
        finally:
            return result

    def multi_hget_data(self, hash_name, keys):
        result = None
        try:
            if isinstance(keys, list):
                multi_hget = self.ssdb.multi_hget(hash_name, *keys)
                if multi_hget:
                    result = multi_hget
            else:
                raise Exception('please input list of keys')
        except Exception as e:
            print '[MULTI HGET DATA][ERROR] - {}'.format(str(e))
        finally:
            return result

    def hdel_data(self, hash_name, key):
        result = False
        try:
            hdel = self.ssdb.hdel(hash_name, key)
            if hdel:
                result = True
        except Exception as e:
            print '[HDEL DATA][ERROR] - {}'.format(str(e))
        finally:
            return result

    def multi_hdel_data(self, hash_name, keys):
        result = False
        try:
            if isinstance(keys, list):
                multi_hdel = self.ssdb.multi_hdel(hash_name, *keys)
                if multi_hdel:
                    result = True
            else:
                raise Exception('please input list of keys')
        except Exception as e:
            print '[MULTI HDEL DATA][ERROR] - {}'.format(str(e))
        finally:
            return result

    def hscan_data(self, hash_name, key_start='', key_end='', limit=None, reverse=False):
        result = None
        try:
            if limit:
                if not reverse:
                    hscan = self.ssdb.hscan(hash_name, key_start=key_start, key_end=key_end, limit=limit)
                else:
                    hscan = self.ssdb.hrscan(hash_name, key_start=key_start, key_end=key_end, limit=limit)
            else:
                if not reverse:
                    hscan = self.ssdb.hscan(hash_name, key_start=key_start, key_end=key_end)
                else:
                    hscan = self.ssdb.hrscan(hash_name, key_start=key_start, key_end=key_end)

            if hscan:
                result = hscan
        except Exception as e:
            print '[HSCAN DATA][ERROR] - {}'.format(str(e))
        finally:
            return result

    def hlist_data(self, hash_name_start='', hash_name_end='', limit=None, reverse=False):
        result = None
        try:
            if limit:
                if not reverse:
                    hlist = self.ssdb.hlist(name_start=hash_name_start, name_end=hash_name_end, limit=limit)
                else:
                    hlist = self.ssdb.hrlist(name_start=hash_name_start, name_end=hash_name_end, limit=limit)
            else:
                if not reverse:
                    hlist = self.ssdb.hlist(name_start=hash_name_start, name_end=hash_name_end)
                else:
                    hlist = self.ssdb.hrlist(name_start=hash_name_start, name_end=hash_name_end)

            if hlist:
                result = hlist
        except Exception as e:
            print '[HLIST DATA][ERROR] - {}'.format(str(e))
        finally:
            return result

    def hkeys_data(self, hash_name, key_start='', key_end='', limit=None):
        result = None
        try:
            if limit:
                hkeys = self.ssdb.hkeys(hash_name, key_start=key_start, key_end=key_end, limit=limit)
            else:
                hkeys = self.ssdb.hkeys(hash_name, key_start=key_start, key_end=key_end)

            if hkeys:
                result = hkeys
        except Exception as e:
            print '[HKEYS DATA][ERROR] - {}'.format(str(e))
        finally:
            return result

    def hgetall_data(self, hash_name):
        result = None
        try:
            hgetall = self.ssdb.hgetall(hash_name)
            if hgetall:
                result = hgetall
        except Exception as e:
            print '[HGETALL DATA][ERROR] - {}'.format(str(e))
        finally:
            return result

    def get_data(self, key):
        result = None
        try:
            get = self.ssdb.get(key)
            if get:
                result = get
        except Exception as e:
            print '[GET DATA][ERROR] - {}'.format(str(e))
        finally:
            return result

    def set_data(self, key, value):
        result = None
        try:
            set = self.ssdb.set(key, value)
            if set:
                result = True
        except Exception as e:
            print '[SET DATA][ERROR] - {}'.format(str(e))
        finally:
            return result
